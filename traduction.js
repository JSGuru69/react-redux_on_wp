import React from 'react';
import _ from 'lodash';

window.notfound = []

window.getNotfound = function(){
  notfound.forEach(function(n){
    console.log('"'+n+'" : {\n\t"fr" : "'+n+'",\n\t"nl" : "",\n\t"en" : ""\n},')
  })
}


let langue = 'fr';

const Data = {
  "codepromo-ask" : {
    "fr" : "Vous avez un code promo?",
    "nl" : "Heeft u een kortingscode?",
    "en" : "Do you have a coupon"
  },
  "codepromo-add" : {
    "fr" : "rajoutez-le ici",
    "nl" : "Vul deze hier in",
    "en" : "Add your code here"
  },
  "codepromo-add-it" : {
    "fr" : "Ajouter code promo",
    "nl" : "Vul promotiecode in",
    "en" : "Enter your promotioncode here"
  },

  "commission-agence" : {
    "fr" : "Commission agence",
    "nl" : "Commissie kantoor",
    "en" : ""
  },
  "commission-invoice-proprio" :{
    "fr" : "Facturé au propriétaire",
    "nl" : "Stuur factuur naar eigenaar",
    "en" : "Send bill to owner"
  },

  "commission-total-tvac" : {
    "fr" : "Total TVAC",
    "nl" : "Totale prijs BTW inbegrepen",
    "en" : "Price tax included"
  },

  "calculez-meilleur-prix-a" : {
    "fr" : "Calculez ici votre meilleur prix pour vos:",
    "nl" : "Bereken hier de beste prijs voor uw elektrische keuring en uw EPC-certificaat. ",
    "en" : ""
  },
  "calculez-meilleur-prix-b" : {
    "fr" : "contrôles électriques, certificats PEB et contrôles citerne.",
    "nl" : "Combineer de keuringen en bespaar!",
    "en" : ""
  },
  "page-devis" : {
    "fr" : "Calcul du prix",
    "nl" : "Bereken uw prijs",
    "en" : ""
  },
  "page-rdv" : {
    "fr" : "Rendez-vous",
    "nl" : "Afspraak",
    "en" : "Appointment"
  },
  "page-coordonnees" : {
    "fr" : "Vos coordonnées",
    "nl" : "Uw gegevens",
    "en" : "Your information"
  },
  "page-documents" : {
    "fr" : "Facturation et envoi",
    "nl" : "Bestellen",
    "en" : "Documents"
  },
  "page-validation" : {
    "fr" : "Validation",
    "nl" : "Bevestigen",
    "en" : "Validation"
  },

  "page-contact" : {
    "fr" : "Personne sur place pour accueillir l'expert",
    "nl" : "Persoon ter plaatse die expert ontvangt",
    "en" : ""
  },

  "dvp-moment-0" : {
    "fr" : "Toutes la journée",
    "nl" : "De hele dag",
    "en" : ""
  },
  "dvp-moment-1" : {
    "fr" : "Matin",
    "nl" : "Voormiddag",
    "en" : ""
  },
  "dvp-moment-2" : {
    "fr" : "Après-midi",
    "nl" : "Namiddag",
    "en" : "Afternoon"
  },

  //step1
  "region-select.titre" : {
    "fr" : "Où se situe le bien ?",
    "nl" : "Regio van de woning ?",
    "en" : "Where is the estate ?"
  },
  "region-select.region.0" : {
    "fr" : "Région wallonne",
    "nl" : "Wallonië",
    "en" : "Wallonie"
  },
  "region-select.region.1" : {
    "fr" : "Région bruxelloise",
    "nl" : "Brussel",
    "en" : "Brussels"
  },
  "region-select.region.2" : {
    "fr" : "Région flamande",
    "nl" : "Vlaanderen",
    "en" : "Flanders"
  },

  "bien-select.titre" : {
    "fr" : "Quel type de bien ?",
    "nl" : "Wat voor soort woning is het ?",
    "en" : "Which estate type ?"
  },
  "bien-select.bien.m" : {
    "fr" : "Maison",
    "nl" : "Huis",
    "en" : "House"
  },
  "bien-select.bien.a" : {
    "fr" : "Appartement",
    "nl" : "Appartement",
    "en" : "Appartment"
  },
  "bien-select.bien.i" : {
    "fr" : "Immeuble d'appartements",
    "nl" : "Appartementen gebouw",
    "en" : "Appartments building"
  },

  "bien-select.superficie-a.titre" : {
    "fr" : "La superficie habitable du bien est",
    "nl" : "De oppervlakte van de woning is",
    "en" : "Surface of the estate is"
  },
  "bien-select.superficie-m.titre" : {
    "fr" : "Combien y a-t-il de chambres et/ou bureaux?",
    "nl" : "Hoeveel kamers of bureaus zijn er in het huis?",
    "en" : "How many rooms or desks are there in the house?"
  },
  "bien-select.superficie-m.subtitre" : {
    "fr" : "La supérficie du bien est de",
    "nl" : "De bewoonbare oppervlakte bedraagt",
    "en" : ""
  },

  "bien-select.count.titre" : {
    "fr" : "Combien d'appartements avez-vous dans l'immeuble ?",
    "nl" : "Hoeveel appartementen zijn er in het gebouw? (appartementen zonder hoofdletter)",
    "en" : "How many appartements are there in the building"
  },

  "bien-appart-Studio (<50m2)" : {
    "fr" : "Studio (<50m2)",
    "nl" : "Studio (<50m2)",
    "en" : "Studio (<50m2)"
  },
  "bien-appart-50-75m2" : {
    "fr" : "50-75m2",
    "nl" : "50-75m2",
    "en" : "50-75m2"
  },
  "bien-appart-75-125m2" : {
    "fr" : "75-125m2",
    "nl" : "75-125m2",
    "en" : "75-125m2"
  },
  "bien-appart-125-200m2" : {
    "fr" : "125-200m2/Duplex",
    "nl" : "125-200m2",
    "en" : "125-200m2"
  },
  "bien-appart-200-300m2" : {
    "fr" : "200-300m2/Triplex",
    "nl" : "200-300m2",
    "en" : "200-300m2"
  },
  "bien-appart->300m2" : {
    "fr" : "+300m2",
    "nl" : "+300m2",
    "en" : "+300m2"
  },

  "bien-maison-1 chambres" : {
    "fr" : "1",
    "nl" : "1",
    "en" : "1"
  },
  "bien-maison-2 chambres" : {
    "fr" : "2",
    "nl" : "2",
    "en" : "2"
  },
  "bien-maison-3 chambres" : {
    "fr" : "3",
    "nl" : "3",
    "en" : "3"
  },
  "bien-maison-4 chambres" : {
    "fr" : "4",
    "nl" : "4",
    "en" : "4"
  },
  "bien-maison-5 chambres" : {
    "fr" : "5",
    "nl" : "5",
    "en" : "5"
  },
  "bien-maison-6 chambres" : {
    "fr" : "+",
    "nl" : "+",
    "en" : "+"
  },
  "immeuble-resume":{
    "fr" : " ",
    "nl" : " ",
    "en" : " "
  },
  "bien-appart-Studio (<50m2)-resume" : {
    "fr" : "Studio",
    "nl" : "Studio",
    "en" : "Studio"
  },
  "bien-appart-50-75m2-resume" : {
    "fr" : "50-70m2",
    "nl" : "50-70m2",
    "en" : "50-70m2"
  },
  "bien-appart-75-125m2-resume" : {
    "fr" : "75-125m2",
    "nl" : "75-125m2",
    "en" : "75-125m2"
  },
  "bien-appart-125-200m2-resume" : {
    "fr" : "125-200m2",
    "nl" : "125-200m2",
    "en" : "125-200m2"
  },
  "bien-appart-200-300m2-resume" : {
    "fr" : "200-300m2",
    "nl" : "200-300m2",
    "en" : "200-300m2"
  },
  "bien-appart->300m2-resume" : {
    "fr" : ">300m2",
    "nl" : ">300m2",
    "en" : ">300m2"
  },
  "bien-maison-1 chambres-resume" : {
    "fr" : "1 chambre",
    "nl" : "1 kamer",
    "en" : "1 room"
  },
  "bien-maison-2 chambres-resume" : {
    "fr" : "2 chambres",
    "nl" : "2 kamers",
    "en" : "2 rooms"
  },
  "bien-maison-3 chambres-resume" : {
    "fr" : "3 chambres",
    "nl" : "3 kamers",
    "en" : "3 rooms"
  },
  "bien-maison-4 chambres-resume" : {
    "fr" : "4 chambres",
    "nl" : "4 kamers",
    "en" : "4 rooms"
  },
  "bien-maison-5 chambres-resume" : {
    "fr" : "5 chambres",
    "nl" : "5 kamers",
    "en" : "5 rooms"
  },
  "bien-maison-6 chambres-resume" : {
    "fr" : "+",
    "nl" : "+",
    "en" : "+"
  },
  "bien-maison-Villa-resume": {
    "fr" : "- de 350m2",
    "nl" : "-350m2",
    "en" : "-350m2"
  },
  "bien-maison-Villa" : {
    "fr" : "- de 350m2",
    "nl" : "-350m2",
    "en" : "-350m2"
  },
  "bien-maison-GrandeVilla-resume" : {
    "fr" : "+ de 350m2",
    "nl" : "+350m2",
    "en" : "+350m2"
  },
  "bien-maison-GrandeVilla" : {
    "fr" : "+ de 350m2",
    "nl" : "+350m2",
    "en" : "+350m2"
  },
  "bien-maison-needtocall-resume" : {
    "fr" : "+ de 500m2",
    "nl" : "+500m2",
    "en" : "+500m2"
  },
  "bien-maison-needtocall" : {
    "fr" : "+ de 500m2",
    "nl" : "+500m2",
    "en" : "+500m2"
  },

  "bottom-image.titre" : {
    "fr" : "Les avantages de Certinergie",
    "nl" : "De voordelen van Certinergie",
    "en" : "The advantages of Certinergie"
  },

  "service-select.titre" : {
    "fr" : "De quel(s) service(s) avez-vous besoin ?",
    "nl" : "Welke keuringen wenst u te bestellen?",
    "en" : ""
  },
  "service-select.count.titre" : {
    "fr" : "Combien avez-vous de compteur(s) électrique(s) ?",
    "nl" : "Hoeveel elektriciteitsmeters zijn er?",
    "en" : "How many compteur elec ?"
  },
  "service-select.plan.count.titre" : {
    "fr" : "Combien de niveaux? (pour l'établissement du plan)",
    "nl" : "Hoeveel verdiepingen zijn er?",
    "en" : "How many floors in the building?"
  },
  "plan-2d-info-text" : {
    "fr" : "Nouveau ! : Commandez dès maintenant un plan 2 D de votre habitation. L’ajout d’un plan 2D à vos annonces vous aide à vous démarquer  et augmente le nombre de prospects générés de 30 %. Attention les plans 2D sont une représentation de votre habitation. A ne pas confondre avec les schémas unifilaires et de position.",
    "nl" : "Nieuw! Bestel nu een grondplan bij Certinergie. Wij maken uw 2D plattegrond op. Verkopers die een 2D grondplan toevoegen aan hun advertentie vergroten hun aantal kandidaat-kopers met 30%!",
    "en" : "New! Order a x with Certinergie. We make your 2D x. People who add a X to their ads wil increase the amount of candidate buyers with 30%"
  },
  "peb-description-info-text" : {
    "fr" : "Le prix du certificat PEB varie en fonction de la surface et de la complexité du bâtiment à expertiser. Il dépend également du nombre d'unités d'habitations (1 unité = cuisine, WC, SDB séparées) et ce indépendamment que ces unités soient reconnues par l'urbanisme. A défaut d'une description correspondant à la réalité, nous nous réservons le droit de revoir le prix annoncé",
    "nl" : "De prijs van uw PEB-certificaat varieert naargelang de oppervlakte en de complexiteit van het gebouw, maar hangt ook af van het aantal woningen. (1 eenheid bestaat uit een keuken, een toilet en een aparte badkamer) Wanneer de beschrijving van het pand niet overeenkomt met de werkelijkheid, behouden wij het recht om de aangekondigde prijzen te herzien.",
    "en" : "The price of your certificate is influenced by the surface and the complexity of the building, but also by the amount of homes (1 unity = kitchen, toilet, bathroom). When the description of the estate does not match the reality, we reserve the right to adjust the price."
  },
  "peb-partiel-info-text" : {
    "fr" : "Le rapport partiel PEB est nécessaire uniquement en Wallonie lorsqu’il existe des installations communes aux différents logements (par ex: chaudière commune, production d’eau chaude sanitaire, système de ventilation). Dès lors, un certificat PEB en Wallonie d’un appartement avec des installations communes ne peut être réalisé sans le rapport partiel car celui-ci fait partie intégrante de l’évaluation de la performance de votre appartement. A contrario, un appartement équipé d’une installation individuelle n’est pas visé par l’obligation de présenter un rapport partiel PEB.",
    "nl" : "Het EPC-deelrapport is enkel in Wallonië vereist bij gedeelde voorzieningen zoals ventilatiesystemen, warmtepomp, ... Een EPC-certificaat voor een appartementsgebouw zonder deelrapport is in Wallonië niet geldig. Omgekeerd is het voor een appartement dat is uitgerust met een individuele installatie niet nodig om een deelrapport in te dienen. ",
    "en" : ""
  },
  'contact.copie' : {
    'fr' : 'Même personne que ci-dessus',
    'nl' : 'Zelfde persoon als hierboven',
    'en' : 'Same person as above'
  },

  "champ-requis" : {
    "fr" : "Ce champ est requis pour continuer",
    "nl" : "Dit veld is verplicht om door te gaan",
    "en" : "This field is required to continue"
  },

  'login-continue-without.titre' : {
    'fr' : 'Nouveau ?',
    'nl' : 'Nieuw hier?',
    'en' : 'New here ?'
  },
  'login-continue-without-explication' : {
    'fr' : 'Vous pouvez continuer votre commande sans vous connecter. Votre identifiant Certinergie sera créé une fois votre commande passée.',
    'nl' : 'U kunt doorgaan met uw bestelling zonder in te loggen. Uw Certinergie account wordt pas aangemaakt na uw bestelling.',
    'en' : 'You can continue without a log in. Your Certinergie account will only be created after your command.'
  },
  'login-continue-without.firstvisitesup': {
      'fr' : 'ère',
      'nl' : 'ste',
      'en' : ''
  },
  'login-continue-without.firstvisite': {
      'fr' : ' visite sur certinergie.be',
      'nl' : ' bezoek aan certinergie.be',
      'en' : 'visit certinergie.be'
  },
  'login-continue-without.phrase1' : {
    'fr' : 'Je n’ai pas introduit mon adresse e-mail',
    'nl' : 'Ik heb mijn e-mailadres nog niet ingevoerd',
    'en' : 'I haven\'t entered my e-mailadress yet'
  },
  'login-continue-without.phrase2' : {
    'fr' : 'Je n’ai pas encore demandé une offre via le site web',
    'nl' : 'Ik heb nog geen bestelling geplaatst',
    'en' : 'I\'m not registered'
  },
  'login-continue-without.btn' : {
    'fr' : 'Continuer sans se connecter',
    'nl' : 'Doorgaan zonder account',
    'en' : 'Continue without account'
  },
  'password-forgotten' : {
    'fr' : 'Mot de passe oublié',
    'nl' : 'Wachtwoord vergeten',
    'en' : 'Forgot my password'
  },
  'password' : {
    'fr' : 'Mot de passe',
    'nl' : 'Wachtwoord',
    'en' : 'Password'
  },
  'login-button' : {
    'fr' : 'Se connecter',
    'nl' : 'Meld mij aan!',
    'en' : 'Login'
  },
  'login-already-client':{
    'fr' : 'Déjà client ?',
    'nl' : 'Bent u al klant ?',
    'en' : 'Already a custommer ?'
  },

  "commission.titre" : {
    "fr" : "Commission agence",
    "nl" : "Commisie kantoor",
    'en' : ""
  },

  "commission.explication" : {
    "fr" : "Faites glisser pour changer le montant",
    "nl" : "Schuif om het bedrag aan te passen",
    'en' : ""
  },

  "status-before": {
    "fr" : "Plan tarifaire ",
    "nl" : "Speciale condities",
    "en" : ""
  },
  //step2
  "status-gold" : {
    "fr" : "Gold",
    "nl" : "Goud",
    "en" : "Gold"
  },
  "status-silver" : {
    "fr" : "Silver",
    "nl" : "Zilver",
    "en" : "Silver"
  },
  "status-bronze" : {
    "fr" : "Bronze",
    "nl" : "Brons",
    "en" : "Bronze"
  },
    "status-goldNl" : {
    "fr" : "Gold Flandre",
    "nl" : "Goud Vlaanderen",
    "en" : "Gold"
  },
  "status-silverNl" : {
    "fr" : "Silver Flandre",
    "nl" : "Zilver Vlaanderen",
    "en" : "Silver"
  },
  "status-bronzeNl" : {
    "fr" : "Bronze Flandre",
    "nl" : "Brons Vlaanderen",
    "en" : "Bronze"
  },
    "status-platinum" : {
    "fr" : "Platinum",
    "nl" : "Platinum",
    "en" : "Platinum"
  },

  "info-coordonnee-voscoordonnee.title" : {
    "fr" : "Vos coordonnées",
    "nl" : "Uw gegevens",
    'en' : 'Your information'
  },
    "info-coordonnee-voscoordonnee.logout" : {
    "fr" : "Se déconnecter",
    "nl" : "Uitloggen",
    'en' : 'Logout'
  },
  'info-coordonnee.youare':{
    'fr' : 'Vous êtes connecté en tant que ',
    'nl' : 'U bent ingelogd als ',
    'en' : 'You are logged in as '
  },
  'info-coordonnee.withemail':{
    'fr' : " via l'email ",
    'nl' : " via het e-mailadres ",
    'en' : " with email "
  },
  "clientTypePicker-normal" : {
    "fr" : "Particulier",
    "nl" : "Eigenaar",
    'en' : 'Owner'
  },
  "clientTypePicker-agence" : {
    "fr" : "Agence",
    "nl" : "Agentschap",
    'en' : 'Agency'
  },
  "clientTypePicker-notaire" : {
    "fr" : "Notaire",
    "nl" : "Notaris",
    'en' : 'Notaire'
  },
  "clientTypePicker-installateur" : {
    "fr" : "Installateur",
    "nl" : "Zelfstandige",//Elektricien
    "en" : "Electrician",
  },
  "info-address.tva":{
    "fr" : "N° Tva",
    "nl" : "btw-nummer",
    "en" : "tax number"
  },
  "info-address.titre" : {
    "fr" : "Adresse du bien à expertiser",
    "nl" : "Adres van het pand",
    'en' : 'Estate address'
  },
  "info-address.street" : {
    "fr" : "Rue",
    "nl" : "Straatnaam",
    'en' : 'Street'
  },
  "info-address.numbox" : {
    "fr" : "Num",
    "nl" : "Huisnummer",
    'en' : 'Num'
  },
  "info-address.zipcity" : {
    "fr" : "Code postal",
    "nl" : "PostCode",
    'en' : 'Postal Code'
  },

  "info-owner.name" : {
    "fr" : "Coordonnée",
    "nl" : "Gegevens",
    "en" : ""
  },

  "info-owner.firstname" : {
    "fr" : "Prénom",
    "nl" : "Voornaam",
    'en' : 'Firstname'
  },
  "info-owner.lastname" : {
    "fr" : "Nom",
    "nl" : "Naam",
    'en' : 'Lastname'
  },
  "info-owner.phone" : {
    "fr" : "Téléphone",
    "nl" : "Telefoonnummer",
    'en' : 'Phone'
  },
  "info-owner.email" : {
    "fr" : "Email (optionnel)",
    "nl" : "E-mailadres",
    'en' : 'Email'
  },

  "info-contact.titre" : {
    "fr" : "Personne sur place pour accueillir l'expert",
    "nl" : "Persoon ter plaatse om de deskundige te ontvangen",
    'en' : ''
  },
  "info-contact.explication" : {
    "fr" : "Coordonnées de la personne  qui permettra à l'expert d'accéder à l'habitation",
    "nl" : "Gegevens van de persoon ter plaatse die de deskundige toegang tot de woning verleent.",
    'en' : ""
  },
  "info-contact.prenom" : {
    "fr" : "Prénom",
    "nl" : "Voornaam",
    'en' : 'Firstname'
  },
  "info-contact.nom" : {
    "fr" : "Nom",
    "nl" : "Naam",
    'en' : 'Lastname'
  },
  "info-contact.phone" : {
    "fr" : "Téléphone",
    "nl" : "Telefoonnummer",
    'en' : 'Phone'
  },
  "info-contact.dates" : {
    "fr" : "Disponibilités",
    "nl" : "Beschikbaarheid",
    'en' : 'Prefered visit dates'
  },
  "info-contact.date-picker-placeholder" : {
    "fr" : "Choisir une date",
    "nl" : "Kies een datum",
    'en' : 'Pick a date'
  },
  "info-contact.message-max-date" : {
    "fr" : "Vous ne pouvez pas choisir plus de {maxDate} dates",
    "nl" : "U kunt niet meer dan {maxDate} data kiezen",
    'en' : 'You can\'t choose more than {maxDate} Dates'
  },

  "info-coordonnee.titre.proprio" : {
    "fr" : "Coordonnées du propriétaire",
    "nl" : "Gegevens van de eigenaar",
    'en' : 'Information about the owner'
  },

  "service-select.item-1" : {
    "fr" : "Certificat PEB",
    "nl" : "Energieprestatiecertificaat (EPC)",
    'en' : ''
  },
  "service-select.item-11" : {
    "fr" : "PEB Partiel",
    "nl" : "EPC-deelrapport",
    'en' : ''
  },
  "service-select.item-4" : {
    "fr" : "Contrôle électrique",
    "nl" : "Elektrische keuring",
    'en' : ''
  },

  "service-select.item-69" : {
    "fr" : "Contrôle Citerne",
    "nl" : "Stookolietank keuring",
    'en' : ''
  },
  "service-select.item-14" : {
    "fr" : "Plan 2D",
    "nl" : "Grondplan (2D plan)",
    "en" : ""
  },

  "service-select.citerne.titre" : {
    "fr" : "Quel type de citerne?",
    "nl" : "Soort stookolietank",
    'en' : 'Kind of Fuel oil tank'
  },
  "service-select-citerne.item-9" : {
    "fr" : "Enterrée",
    "nl" : "Ondergrondse",
    'en' : 'Underground'
  },
  "service-select-citerne.item-6" : {
    "fr" : "Aérienne",
    "nl" : "Bovengrondse",
    'en' : 'Surface'
  },
  "service-select-peb-partiel-form-existing-num":{
    "fr":"Numéro du PEB partiel existant",
    "nl":"Nummer van het EPC-deelrapport",
    "en":""
  },

  //Step-Four
  "info-document-titre":{
    "fr" : "Coordonnée de la facturation",
    "nl" : "Versturen van de documenten (certificaten, factuur)",
    "en" : ""
  },

  "info-document-question-text" : {
    "fr" : "Merci de nous faire savoir à quelle adresse voulez-vous réceptionner les documents originaux et recevoir la facture. L’adresse de facturation et d’envoi des documents est indissociable. Merci de nous contacter au 0800/82171 pour toute demande spécifique à ce sujet,",
    "nl" : "Bedankt om ons te laten weten op welke manier u uw originele documenten en uw factuur graag wilt ontvangen. Beide worden altijd naar hetzelfde factuuradres verzonden. Indien u hier vragen over heeft, contacteer ons via 0800/82171.",
    "en" : "Thank you for letting us know, how you want to recieve your orginal documents and your bill. Both will be send to the same adress. If you have any questions about that, please do not hesitate to contact us about this via 0800/82171."
  },
  'info-document-check-a' : {
    'fr' : ' de réduction pour envoyer uniquement la facture et rapport(s) par email' ,
    'nl' : 'korting indien u de factuur en het certificaat enkel via e-mail wenst te ontvangen.' ,
    'en' : 'discount if you wish to recieve your bill and certificates only through e-mail.'
  },
  'info-document.titre.qui' : {
    'fr' : 'Facture et rapport(s) à envoyer à' ,
    'nl' : 'Factuur en certificaten worden verstuurd naar:' ,
    'en' : 'Invoice and report(s) are going to be send to'
  },

  'info-document.titre.ou' : {
    'fr' : 'A l\'adresse' ,
    'nl' : 'Op het adres' ,
    'en' : 'at this address'
  },
  'no-date-wanted':{
    "fr" : "Aucune date préférée",
    "nl" : "Geen voorkeursdata",
    "en" : "No prefered visit dates"
  },
  //Step-Five
  "resume-order.titre" : {
    "fr" : "Votre demande",
    "nl" : "Uw aanvraag",
    'en' : 'Your order'
  },
  "resume-order.titre.right" : {
    "fr" : "Adresse du bien à expertiser",
    "nl" : "Adres van de woning",
    'en' : 'Estate address'
  },

  "resume-rdv.titre" : {
    "fr" : "Rendez-vous souhaité",
    "nl" : "Afspraak gewenst",
    'en' : 'Prefered dates'
  },

  "resume-rdv.contact" : {
    "fr" : "Personne sur place",
    "nl" : "Persoon ter plaatse",
    'en' : 'Personne sur place'
  },

  "resume-document.titre" : {
    "fr" : "Documents",
    "nl" : "Documenten",
    'en' : 'Invoice and reports'
  },
  "resume-document.phrase-a-env-courrier" : {
    "fr" : "Facture et rapport(s) à envoyer par courrier à",
    "nl" : "Factuur en keuringsverslag(en) opsturen met de post naar",
    'en' : 'Invoice and report(s) are going to be send to'
  },
  "resume-document.phrase-a-env-mail" : {
    "fr" : "Facture et rapport(s) à envoyer par e-mail à",
    "nl" : "Factuur en keuringsverslag(en) opsturen via e-mail naar",
    'en' : 'Invoice and report(s) are going to be send to'
  },
  'resume-document.en-ligne' : {
    'fr' : 'Documents disponibles en ligne sur votre espace << Client >> ',
    'nl' : 'Uw documenten zijn beschikbaar in uw klantenzone',
    'en' : 'Documents available online in your dashboard'
  },
  'resume-document.reductionprice-private' : {
    'fr' : '2,5 € ',
    'nl' : '2,5 € ',
    'en' : '2,5 €'
  },
  'resume-document.reduction-private' : {
    'fr' : ' de réduction pour recevoir facture et rapport(s) uniquement par email',
    'nl' : ' korting voor ontvang factuur en keuringsverslag(en) uitsluitend via e-mail',
    'en' : ''
  },
  'resume-document.reduction-company' : {
    'fr' : ' Recevez facture et rapport(s) uniquement par email',
    'nl' : ' Ontvang factuur en keuringsverslag(en) uitsluitend via e-mail',
    'en' : ''
  },
  'resume-document.login' : {
    'fr' : 'Login',
    'nl' : 'Login',
    'en' : 'Login'
  },
  'resume-document.password' : {
    'fr' : 'Mot de passe',
    'nl' : 'Wachtwoord',
    'en' : 'Password'
  },
  "resume-remarque.titre" : {
    "fr" : "Remarques éventuelles",
    "nl" : "Eventuele opmerkingen",
    'en' : 'Some more information'
  },
  "resume-remarque.phrase" : {
    "fr" : "Si vous souhaitez nous en dire plus sur l'habitation, le rendez-vous, etc",
    "nl" : "Als u meer informatie wenst te geven over de woonst, de aanvraag, de afspraak etc",
    'en' : 'If you need to send us more information about the estate, the appointment, etc'
  },

  "resume-price.phrase" : {
    "fr" : "au lieu de {total}€ si vous commandez en ligne maintenant",
    "nl" : "in plaats van {total}€ als u nu online bestelt",
    'en' : 'instead of {total}€ if you order now'
  },

  "resume-price.phrase-a" : {
    "fr" : "au lieu de",
    "nl" : "in plaats van",
    'en' : 'instead of'
  },

  "resume-price.phrase-b" : {
    "fr" : "si vous commandez en ligne maintenant",
    "nl" : "als u nu online bestelt",
    'en' : 'if you order now'
  },

  "resume-price.phrase-with-com" : {
    "fr" : "dont une commission pour {agence} est ",
    "nl" : "met een commissie voor {agence} van ",
    'en' : 'with a commission for {agence} of '
  },
  "resume-price.reduction-phrase-a" : {
    "fr" : "Soit une réduction de ",
    "nl" : "En dus een korting van",
    'en' : 'And a reduction of'
  },
  "resume-price.reduction-phrase-b" : {
    "fr" : " pour commande en ligne",
    "nl" : " om online te bestellen",
    'en' : ' to order online'
  },

  "resume-price.formailonly" : {
    "fr" : " pour envoi des documents par e-mail",
    "nl" : " om de documenten te verzenden via e-mail",
    'en' : ' to send the documents via e-mail'
  },
  "resume-conditiongenerale.phrase" : {
    "fr" : "Je déclare avoir lu et accepté les {link} (obligatoire)",
    "nl" : "Ik verklaar de {link} gelezen en goedgekeurd te hebben (verplicht)",
    'en' : 'I declare to have read and accepted the {link} (obligated)'
  },
  "resume-conditiongenerale.linktext" : {
    "fr" : "conditions générales",
    "nl" : "Algemene Voorwaarden",
    'en' : 'conditions générales'
  },
  "resume-conditiongenerale.linkurl" : {
    "fr" : "/fr/nos-conditions-generales/",
    "nl" : "/nl/onze-algemene-voorwaarden/",
    'en' : '/fr/nos-conditions-generales/'
  },

  "Info-Document-Address-mail":{
    "fr" : "Par voie électronique",
    "nl" : "via e-mail",
    "en" : ""
  },

  "Info-Document-Address-mail-subtext":{
    "fr" : "Je souhaite recevoir les documents originaux signés par voie électronique.",
    "nl" : "Ik wens de orginele documenten te ontvangen via e-mail.",
    "en" : "I wish to recieve the orginal documents by e-mail."
  },
  "Info-Document-Address-mail-subtext-b":{
    "fr" : "Je bénéficie d’une réduction supplémentaire de",
    "nl" : "Ik geniet van een bijkomende korting van",
    "en" : "I benefit from an extra reduction of"
  },

  "Info-Document-Address-proprio" : {
    "fr" : "Chez le propriétaire",
    "nl" : "Bij de eigenaar",
    "en" : "With the owner"
  },
  "Info-Document-Address-proprio-bien" : {
    "fr" : "Au nom du propriétaire à l'adresse du bien",
    "nl" : "Op naam van de eigenaar, op het adres van de te controleren woning.",
    "en" : ""
  },
  "Info-Document-Address-proprio-bien-himself" : {
    "fr" : "Au nom du demandeur à l'adresse du bien",
    "nl" : "Op naam van de aanvrager, op het adres van de te controleren woning.",
    "en" : ""
  },
  "Info-Document-Address-notaire" : {
    "fr" : "En l'étude",
    "nl" : "Op het notariaat",
    "en" : ""
  },
  "Info-Document-Address-agence" : {
    "fr" : "A l'agence",
    "nl" : "Op het immobiliënkantoor",
    "en" : ""
  },
  "Info-Document-Address-proprioCoNotaire" : {
    "fr" : "Au nom du propriétaire en l'étude",
    "nl" : "Op naam van de eigenaar, op het notariaat",
    "en" : ""
  },
  "Info-Document-Address-proprioCoAgence" : {
    "fr" : "Au nom du propriétaire à l'agence",
    "nl" : "Op naam van de eigenaar, op het immobiliënkantoor",
    "en" : ""
  },
  "Info-Document-Address-other" : {
    "fr" : "A une autre adresse",
    "nl" : "Op een ander adres",
    "en" : ""
  },
  //all
  "Address" : {
    "fr" : "Adresse",
    "nl" : "Adres",
    "en" : ""
  },
  "product-4" : {
    "fr" : "Contrôle électrique",
    "nl" : "Elektrische keuring",
    'en' : 'Electricity inspection'
  },
  "product-1" : {
    "fr" : "Certificat PEB",
    "nl" : "EPC-certificaat",
    'en' : 'EPC certificate'
  },
  "product-11" : {
    "fr" : "Certificat partiel",
    "nl" : "EPC-deelrapport",
    'en' : ''
  },
  "product-14" : {
    "fr" : "Plan 2d",
    "nl" : "2D Grondplan",
    'en' : ''
  },
  "product-9" : {
    "fr" : "Contrôle citerne Enterée",
    "nl" : "Ondergrondse stookolietank keuring",
    'en' : 'Underground fuel oil tank inspection'
  },
  "product-6" : {
    "fr" : "Contrôle citerne Aérienne",
    "nl" : "Bovengrondse stookolietank keuring",
    "en" : "Surface fuel oil tank inspection",
  },
  "product-8" : {
    "fr" : "Compteur supplémentaire",
    "nl" : "Extra meter",
    "en" : ""
  },
  "product-reduction" : {
    "fr" : "Réduction (commande en ligne)",
    "nl" : "Korting (online bestellen)",
    'en' : 'Reduction'
  },
  "product-commission" : {
    "fr" : "Commission",
    "nl" : "Commissie",
    'en' : 'Commission'
  },

  "nextStep" : {
    "fr" : "Continuer",
    "nl" : "volgende",
    'en' : 'Continue'
  },

  'commission-titre' : {
    'fr' : 'Commission agence',
    'nl' : 'Commissie Agentschap',
    'en' : 'Agency commission'
  },
  'commission-explication' : {
    'fr' : 'Faites glisser pour changer le montant de votre commission',
    'nl' : 'Schuif om het bedrag van uw commissie aan te passen.',
    'en' : 'Slide to change the value'
  },
  'date-visite-explication-1':{
    'fr' : 'Veuillez choisir une ou plusieurs dates. (veuillez cliquer sur le bouton "plus" pour finaliser votre choix)',
    'nl' : 'Selecteer een of meerdere data.',
    'en' : ''
  },
  "date-butoir.titre" : {
    "fr" : "Date butoir",
    "nl" : "Uiterste datum",
    "en" : ""
  },
  'date-visite-explication-2':{
    'fr' : 'Certinergie vous recontactera pour vous fixer la date de rendez-vous',
    'nl' : 'Certinergie zal contact met u opnemen om de afspraak in te plannen.',
    'en' : ''
  },
  'date-butoir-explication':{
    'fr' : 'Date limite souhaitée pour réalisation des contrôles',
    'nl' : 'Geef hier een uiterste datum aan voor het uitvoeren van de keuringen.',
    'en' : ' '
  },

  'Error-no-email' : {
    'fr' : 'Une adresse email est nécéssaire pour poursuire votre commande',
    'nl' : 'Dit veld is verplicht.',
    'en' : 'An email address is required'
  },
  'Error-no-address' : {
    'fr' : 'Une adresse est nécéssaire pour poursuire votre commande',
    'nl' : 'Dit veld is verplicht.',
    'en' : 'An address is required'
  },
  'Error-no-contact' : {
    'fr' : 'le contact est nécéssaire pour poursuire votre commande',
    'nl' : 'Dit veld is verplicht.',
    'en' : 'error contact'
  },
  'resume-document.en-ligne':{
    'fr' : 'Documents disponibles en ligne sur le compte',
    'nl' : 'Beschikbare documenten in uw klantenzone',
    'en' : 'Documents available online in your dashboard'
  },

  "resume-bdc.titre" : {
    "fr" : "Bon de commande",
    "nl" : "Bestelbon",
    "en" : ""
  },

  "resume-bdc.phrase1" : {
    "fr" : "Vous nous confirmez avoir été mandaté par votre client et avoir prévenu celui-ci de notre passage.",
    "nl" : "U bevestigt een verkoopsmandaat te hebben van uw klant en hem op de hoogte te hebben gebracht van ons bezoek.",
    "en" : ""
  },
  "resume-bdc.phrase2" : {
    "fr" : "A cet effet un bon de commande à faire signer est disponible sur votre espace client / tableau de bord.",
    "nl" : "De bestelbon die u kunt laten ondertekenen is vanaf nu beschikbaar in uw klantenzone.",
    "en" : ""
  },
  "resume-bdc.phrase3" : {
    "fr" : "  A défaut de mandat de vente confirmant notre mission ou d’un bon de commande signé, le demandeur est considéré avoir commandé pour son propre compte.",
    "nl" : "In geval van ingebrekestelling is de aanvrager geacht de bestelling te hebben gedaan in eigen naam en voor eigen rekening.",
    "en" : ""
  },
  "resume-bdc.clause" : {
    "fr" : "Afin d'éviter toute contestation ultérieure d'un client, voici un exemple d'une clause type à insérer dans votre mandat de vente. ",
    "nl" : "Om alle betwisting van een klant te vermijden, vindt u hier een voorbeeld van een clausule die u aan uw verkoopmandaat kunt toevoegen.",
    "en" : ""
  },
  "resume-acte.titre" : {
    "fr" : "Mode de paiement",
    "nl" : "Betaalmethode",
    "en" : ""
  },
  "resume-acte.titre.particulier" : {
    "fr" : "Paiement",
    "nl" : "Betaling",
    "en" : ""
  },
  "resume-acte.payment.label" : {
    "fr" : "Au choix,",
    "nl" : "Naar keuze",
    "en" : ""
  },
  "resume-acte.payment.espece.part1" : {
    "fr" : "Soit en ",
    "nl" : "Of een ",
    "en" : ""
  },
  "resume-acte.payment.espece.keyword" : {
    "fr" : "espèces",
    "nl" : "Contant",
    "en" : ""
  },
  "resume-acte.payment.espece.part2" : {
    "fr" : ", directement au contrôleur sur place.",
    "nl" : ", contant ter plaatse aan de expert ",
    "en" : ""
  },
  "resume-acte.payment.virment.part1" : {
    "fr" : "Par ",
    "nl" : "Via ",
    "en" : ""
  },
  "resume-acte.payment.virment.keyword" : {
    "fr" : "virement",
    "nl" : "een overschrijving",
    "en" : ""
  },
  "resume-acte.payment.virment.part2" : {
    "fr" : ", dans les 15 jours maximum de notre visite.",
    "nl" : ", binnen 15 dagen (maximum) na ons bezoek.",
    "en" : ""
  },
  "resume-acte.payment.facture.label" : {
    "fr" : "La facture vous est donnée ou envoyée",
    "nl" : "De factuur wordt u overhandigd of opgestuurd",
    "en" : ""
  },
  "resume-acte.payment.facture.data" : {
    "fr" : " le jour de notre passage.",
    "nl" : " de dag van ons bezoek.",
    "en" : ""
  },
  "resume-acte.phrase" : {
    "fr" : "Paiement à l'acte",
    "nl" : "Betalen bij het ondertekenen van de akte",
    "en" : ""
  },
  "acte-form.notaireName": {
    "fr" : "Nom du notaire",
    "nl" : "Naam van de notaris",
    "en" : ""
  },


  "resume-acte-phrase1" : {
    "fr" : "Max 6 mois à dater de la réalisation des contrôles.",
    "nl" : "Maximum 6 maanden na het uitvoeren van de keuringen.",
    "en" : ""
  },
  "resume-acte-phrase2" : {
    "fr" : "En cochant cette option, Certinergie porte exceptionnellement ce délai de paiement à maximum 6 mois et ce dans l’attente de la signature de l’acte.",
    "nl" : "Door deze optie aan te vinken, draagt Certinergie uitzondelijk de kosten van de keuring, maar dit slechts voor maximum 6 maanden in afwachting van het ondertekenen van de akte.",
    "en" : ""
  },
  /*"resume-acte-phrase2" : {
    "fr" : "Le demandeur marque son accord pour que la facture soit établie à son nom exclusivement et s’engage dans tous les cas à la régler au terme du délai de 7 mois.",
    "nl" : "",
    "en" : ""
  },*/

  "resume-acte-phrase-agence" : {
    "fr" : "En contrepartie la facturation est faite au nom exclusif de l’agence et devient exigible après le délai de 6 mois.",
    "nl" : "In ruil wordt de factuur opgesteld op de naam van het agentschap. Het bedrag van de factuur wordt opeisbaar na maximum 6 maanden.",
    "en" : ""
  },
  "resume-acte-phrase-notaire" : {
    "fr" : "Numéro national ou la date de naissance du propriétaire requis ( cadre à compléter )",
    "nl" : "Rijksregisternummer of de geboortedatum van de eigenaar verplicht.",
    "en" : ""
  },

  "resume-acte-question-text" : {
    "fr" : "Pour toute explication ou dérogation éventuelle à ces conditions, merci de nous appeler au 0800/82 171.",
    "nl" : "Voor verdere toelichting belt u ons op via 0800/82 171.",
    "en" : ""
  },

  "acte-form.numNational": {
    "fr" : "N.National ou la date de naissance du propriétaire",
    "nl" : "Rijksregisternummer of de geboortedatum van de eigenaar.",
    "en" : ""
  },

  "acte-form.acteDate":{
    "fr" : "Date de l'acte",
    "nl" : "Datum van het ondertekenen van de akte.",
    "en" : ""
  },
  "acte-form.explication":{
    "fr" : "A défaut de paiement dans les 6 mois à dater de ce jour, Certinergie se réserve le droit de facturer le demandeur.",
    "nl" : "Bij ingebrekestelling na 6 maanden, reserveert Certinergie zich het recht om de aanvrager de factureren.",
    "en" : ""
  },
  "other-address.tva":{
    "fr" : "N° Tva",
    "nl" : "btw-nummer",
    "en" : ""
  },
  "other-address.name":{
    "fr" : "Nom et prénom",
    "nl" : "Naam en Voornaam",
    "en" : ""
  },
  "other-address.phone":{
    "fr" : "Téléphone",
    "nl" : "Telefoonnummer",
    "en" : ""
  },
  "other-address.mail":{
    "fr" : "Email",
    "nl" : "E-mailadres",
    "en" : ""
  },
  "other-address.street":{
    "fr" : "Rue",
    "nl" : "Straatnaam",
    "en" : ""
  },
  "other-address.numbox":{
    "fr" : "Numéro",
    "nl" : "Huisnummer",
    "en" : ""
  },
  "other-address.zipcity":{
    "fr" : "Code postal",
    "nl" : "Postcode",
    "en" : ""
  },
  "other-address.country":{
    "fr" : "Pays",
    "nl" : "Land",
    "en" : ""
  },
  "other-address.company" : {
    "fr" : "Entreprise",
    "nl" : "Onderneming",
    "en" : ""
  },


  "why-owner-info-titre" : {
    "fr" : "Je suis titre",
    "nl" : "",
    "en" : ""
  },
  /*"why-owner-info-text" : {
    "fr" : "Je suis du texte explicatif uriozeaurpioze aioru zeiouriozeauro zeuioruzeio ruiozau rizeauioru zeiuriozeaurioz uorizue ruezoi uriozeau oiruezu riozea uriozeu oiruzeioru zeaiouroizeu p",
    "nl" : "",
    "en" : ""
  },*/
  "why-newuser-phone-text" : {
    "fr" : "Nous avons besoin de votre numéro de téléphone afin de vous contacter pour le RDV ou pour vous demander plus de précision si nécessaire",
    "nl" : "Uw telefoonnummer is noodzakelijk indien we u om meer informatie moeten vragen of zodat onze deskundige ter plaatse contact met u kan opnemen indien nodig.",
    "en" : ""
  },
  "why-newuser-email-text" : {
    "fr" : "Votre e-mail est utilisé pour vous confirmer le RDV, pour vous envoyer les rapports et pour créer un compte Certinergie sur lequel vous pourrez accéder aux documents réalisés",
    "nl" : "We hebben uw e-mailadres nodig om uw afspraak te bevestigen en uw bestelling uit te voeren. Met uw e-mailadres creëren wij ook uw klantenzone waar u achteraf uw persoonlijke documenten terugvindt.",
    "en" : ""
  },
  "why-owner-phone-text" : {
    "fr" : "Nous avons besoin du numéro de téléphone de votre client afin de le prévenir en cas de contretemps",
    "nl" : "We hebben het telefoonnummer van uw klant nodig om het te kunnen verwittigen in het geval van vertraging/vervroeging.",
    "en" : ""
  },
  "why-owner-email-text" : {
    "fr" : "Nous avons besoin de l'adresse mail de votre client afin de lui confirmer le RDV, de lui envoyer les rapports ainsi que la facture.",
    "nl" : "We hebben het e-mailadres van uw klant nodig om zijn bezoek te kunnen bevestigen en hem de keuringsverslagen en de factuur te bezorgen.",
    "en" : ""
  },
  "why-contact-title-text" : {
    "fr" : "Coordonnées de la personne qui permettra à l'expert d'accéder à l'habitation",
    "nl" : "Hier vult u de gegevens in van de persoon die de deskundige toegang tot de woning zal verlenen",
    "en" : ""
  },
  "why-contact-phone-text" : {
    "fr" : "Nous avons besoin de votre numéro de téléphone afin de vous contacter pour le RDV",
    "nl" : "Dit telefoonnummer is noodzakelijk voor onze deskundige ter plaatse om contact met u op te nemen indien nodig.",
    "en" : ""
  },
  "why-date-butoir-text" : {
    "fr" : "Date limite souhaitée pour réalisation des contrôles",
    "nl" : "Uiterste data om de keuringen uit te voeren.",
    "en" : ""
  },
  "why-address-info-titre" : {
    "fr" : "Je suis titre",
    "nl" : "",
    "en" : ""
  },
  "why-address-info-text" : {
    "fr" : "Ce champs est obligatoire.",
    "nl" : "Dit veld is verplicht.",
    "en" : ""
  },

  "Info-Document-Address-otherNotaire" : {
  	"fr" : "Chez un Notaire",
  	"nl" : "Bij een notaris",
  	"en" : ""
  },
  "otherNotaire-address.name" : {
  	"fr" : "Nom du Notaire",
  	"nl" : "Naam van de notaris",
  	"en" : ""
  },

  "call-us-popup.titre" : {
    "fr" : "Demande de Devis",
    "nl" : "Vraag uw bestelling aan",
    "en" : ""
  },
  "call-us-popup.text.a" : {
    "fr" : "Au vu de la spécificité de votre bien, celui ci dois faire l’objet d’un devis personnalisé",
    "nl" : "Gezien de bijzonderheid van uw woning, is hier een gepersonaliseerde offerte van kracht.",
    "en" : ""
  },
  "call-us-popup.text.b" : {
    "fr" : "Nous vous invitons à prendre contact avec nous au ",
    "nl" : "We nodigen u uit om direct contact met ons op te nemen of",
    "en" : ""
  },
  "call-us-popup.text.c" : {
    "fr" : " ou via le formulaire de contact ci-dessous.",
    "nl" : " ons te contacteren via het onderstaande contactformulier.",
    "en" : ""
  },
  "call-us-popup.text.d" : {
    "fr" : "Nous vous répondrons dans les plus bref delais",
    "nl" : "We antwoorden u zo snel als mogelijk.",
    "en" : ""
  },

  "elec-schema-explication-1" : {
    "fr" : "Aucun supplément ",
    "nl" : "",
    "en" : ""
  },
  "elec-schema-explication-2" : {
    "fr" : "ne vous sera réclamé pour la réalisation des croquis électriques obligatoires pour les installations datant d’avant 1981 ( à ne pas confondre avec les schémas unifilaires )",
    "nl" : "Er wordt geen supplement aangerekend voor de verplichte schetsen van elektrische installaties die nog dateren van voor 1981.",
    "en" : ""
  },
  "success-popup.title" : {
    "fr" : "Votre commande a bien été enregistrée",
    "nl" : "Uw bestelling is goed geregistreerd.",
    "en" : "Votre commande à bien été enregistré",
  },
  "success-popup.paragraphe-1" : {
    "fr" : "Un e-mail reprenant les détails de votre commande vient de vous être envoyé.",
    "nl" : "U ontvangt zodadelijk een e-mail met alle informatie betreffende uw bestelling.",
    "en" : "Un e-mail reprenant les détails de votre commande vient de vous être envoyé."
  },
  "success-popup.paragraphe-2" : {
    "fr" : "Nous reprendrons contact avec vous dans les meilleurs délais afin de vous confirmer l'heure et la date du rendez-vous.",
    "nl" : "Wij nemen zo snel mogelijk contact met u op om u het uur en de datum van het bezoek te bevestigen.",
    "en" : ""
  },
  "success-popup.paragraphe-3" : {
    "fr" : "Merci pour votre commande.",
    "nl" : "Bedankt voor uw bestelling.",
    "en" : "Merci pour votre commande."
  },
  "success-popup.paragraphe-4" : {
    "fr" : "Vous pouvez à tout moment consulter l'état de",
    "nl" : "U kunt te allen tijde de status van uw ",
    "en" : "Vous pouvez à tout moment consulter l'état de"
  },
  "success-popup.dossierEnLigne" : {
    "fr" : "votre dossier en ligne",
    "nl" : "bestelling online volgen.",
    "en" : "votre dossier en ligne"
  },
  "success-popup.btn" : {
    "fr" : "Terminer",
    "nl" : "Afsluiten",
    "en" : "Terminer"
  },
  "cle-agence-info-text":{
    "fr" : "GRATUIT en deçà de 15 minutes/kilomètres entre le bien à expertiser et l'agence. Au-delà, un forfait de 60 € HTVA sera appliqué.",
    "nl" : "GRATIS indien het pand zich op 15 min/kilometer van het kantoor bevindt. Is het pand verder gelegen, rekenen we een forfaitaire kost aan van 60 € exclusief BTW.",
    "en" : ""
  },
  "editorder":{
    "fr" : "Modifier d'adresse d'expertise",
    "nl" : "Adres van de woning aanpassen",
    "en" : ""
  },
  "editcontact":{
    "fr" : "Modifier de contact",
    "nl" : "Gegevens contactpersoon aanpassen",
    "en" : ""
  },
  "popup-btn":{
    "fr" : "Enregistrer",
    "nl" : "Bevestigen",
    "en" : "Save"
  },


  "client-type-normal" :{
    "fr" : "",
    "nl" : "",
    "en" : ""
  },
  "client-type-notaire" :{
    "fr" : "notaire",
    "nl" : "notaris",
    "en" : ""
  },
  "client-type-agence" :{
    "fr" : "agence",
    "nl" : "agentschap",
    "en" : ""
  },
  "client-type-installateur" :{
    "fr" : "installateur",
    "nl" : "zelfstandige",
    "en" : ""
  },

  "form-info-btn" : {
  	"fr" : "Envoyer",
  	"nl" : "Verzenden",
  	"en" : ""
  },
  "form-info-nom" : {
  	"fr" : "Nom et prénom",
  	"nl" : "Naam en Voornaam",
  	"en" : ""
  },
  "form-info-mail" : {
  	"fr" : "Email",
  	"nl" : "E-mailadres",
  	"en" : ""
  },
  "form-info-phone" : {
  	"fr" : "Téléphone",
  	"nl" : "Telefoonnummer",
  	"en" : ""
  },
  "form-bien-street" : {
  	"fr" : "Rue",
  	"nl" : "Straatnaam",
  	"en" : ""
  },
  "form-bien-numbox" : {
  	"fr" : "N°",
  	"nl" : "Huisnummer",
  	"en" : ""
  },
  "form-bien-zipcity" : {
  	"fr" : "Code postal",
  	"nl" : "Postcode",
  	"en" : ""
  },
  "form-message" : {
  	"fr" : "Commentaire / Question",
  	"nl" : "Commentaar / Vraag",
  	"en" : ""
  },

  "finaliser-commande" : {
    "fr" : "Valider la commande",
    "nl" : "Bevestig de bestelling",
    "en" : ""
  },
  "ResumeClauseMandat.titre" :{
    "fr" : "Cliquer ici" ,
    "nl" : "Klik hier",
    "en" : ""
  },
  "ResumeClauseMandat.phrase" :{
    "fr" : "Le propriétaire charge expressément l’agence {name} de faire procéder à l’établissement des contrôles obligatoires ({product}) dans le cadre de la mise en vente du bien prédécrit via l’intermédiaire de la société CERTINERGIE et s’engage à en assumer le coût." ,
    "nl" : "De eigenaar stelt uitsluitend het agentschap {name} verantwoordelijk om de verplichte keuringen ({product}) aan te vragen in het kader van het verkopen van de woning via de onderneming CERTINGERGIE, en engageert zich om de kosten te dragen. ",
    "en" : ""
  },
  "resume-acte.delai.normal.label" :{
    "fr" : "Paiement normal :" ,
    "nl" : "Betaling zonder uitstel",
    "en" : ""
  },
  "resume-acte.delai.normal.data" :{
    "fr" : "En espèces, directement au contrôleur sur place ou par virement bancaire dans les 15 jours maximum de notre visite." ,
    "nl" : "Contant, onmiddellijk ter plaatse aan de expert of via overschrijving binnen maximum 15 dagen na ons bezoek.",
    "en" : ""
  },
  "resume-acte-normal.phrase" :{
    "fr" : "Dans les 15 jours de la réalisation des contrôles" ,
    "nl" : "Binnen maximum 15 dagen na ons bezoek. ",
    "en" : ""
  },
  "resume-acte.delai.acte.label" :{
    "fr" : "Paiement à l’acte : " ,
    "nl" : "Betalen met uitstel, bij het ondertekenen van de akte.",
    "en" : ""
  },
  "resume-acte.delai.acte.data" :{
    "fr" : "Maximum dans les 6 mois à dater de la réalisation des contrôles" ,
    "nl" : "Binnen maximum 6 maanden na het uitvoeren van de keuringen.",
    "en" : ""
  }

}


const DataEmpty = {}

const Traduction = function(name,replacement){
  //let Data = DataEmpty
  let text = (Data && Data[name] && Data[name][langue])?Data[name][langue]:null;
  if(!text){

    if(notfound.indexOf(name)==-1){
      notfound.push(name)
    }
    text = name
  }
  if(replacement){
    _.forIn(replacement, function(value, key) {
      if(value && value.text && value.url){
        let splitted = text.split('{'+key+'}')
        text = splitted.reduce((tab,current,i)=>{
          tab.push(<span key={i}>{current}</span>)
          if(splitted.length-1 != i){
            tab.push(<a key={'url'+i} target={value.target} href={value.url}>{value.text}</a>)
          }
          return tab
        },[]);
      }else{
        text = text.split('{'+key+'}').join(''+value);
      }

    });
  }
  return text;
}

export default Traduction;
export function SetLangue(ln){
  console.log('change langue to '+ln)
  langue = ln;
}

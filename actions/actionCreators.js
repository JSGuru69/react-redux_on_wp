export function setAddress(street,numbox,zip,city) {
  return {
    type: 'ORDER_CHANGE_ADDRESS',
    street,numbox,zip,city
  }
}

export function setPopupCoordonneeToClose(){
  return {
    type : 'ORDER_POPUP_COORDONNEE_CLOSE'
  }
}

export function setPartielNumber(num){
  return {
    type : 'ORDER_CHANGE_PARTIEL_NUMBER',
    num
  }
}

export function setBdc(bdcSigned){
  return {
    type: 'ORDER_CHANGE_BDC_SIGNED',
    bdcSigned
  }
}

export function changeOwner(isOwner,owner){
  return {
    type: 'ORDER_CHANGE_OWNER_EDIT',
    isOwner,owner
  }
}

export function setActe(isActe,acteinfo){
  return {
    type: 'ORDER_CHANGE_IS_ACTE',
    isActe,acteinfo
  }
}

export function addPromo(code,promo) {
  return {
    type: 'ORDER_ADD_PROMO',
    code,promo
  }
}
export function setDateButoir(dateButoir) {
  return {
    type: 'ORDER_ADD_DATEBUTOIR',
    dateButoir
  }
}
export function setOwner(id,firstname,lastname,phone,email) {
  return {
    type: 'ORDER_CHANGE_OWNER',
    id,firstname,lastname,phone,email
  }
}
export function toggleSendByMail() {
  return {
    type: 'ORDER_CHANGE_SENDBYMAIL'
  }
}
export function setCommission(commission) {
  return {
    type: 'ORDER_CHANGE_COMMISSION',
    commission
  }
}
export function setAddressDocumentAutre(street,numbox,zip,city){
  return {
    type: 'ORDER_CHANGE_ADDRESS_DOCUMENT_AUTRE',
    street,numbox,zip,city
  }
}
export function setContact(firstname,lastname,phone,dates) {
  return {
    type: 'ORDER_CHANGE_CONTACT',
    firstname,lastname,phone,dates
  }
}

export function setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is){
  return {
    type: 'ORDER_CHANGE_DOCUMENT',
    street,numbox,zip,city,name,company,country,tva,phone,email,is
  }
}

export function setOtherDocument(other){
  return {
    type : 'ORDER_CHANGE_OTHER_DOCUMENT',
    other
  }
}
export function setOtherNotaireDocument(otherNotaire){
  return {
    type: 'ORDER_CHANGE_OTHER_NOTAIRE_DOCUMENT',
    otherNotaire
  }
}

export function setRemarque(remarque){
  return {
    type: 'ORDER_CHANGE_REMARQUE',
    remarque
  }
}

export function setRegion(region){
  return {
    type: 'ORDER_CHANGE_REGION',
    region
  }
}

export function setBien(buildingType,count,size,estateType){
  return {
    type: 'ORDER_CHANGE_BIEN',
    buildingType,count,size,estateType
  }
}

export function setElecCount(count){
  return {
    type: 'ORDER_CHANGE_ELEC_COUNT',
    count
  }
}
export function setPlanCount(count){
  return {
    type: 'ORDER_CHANGE_PLAN_COUNT',
    count
  }
}

export function setCiterneChoice(num){
  return {
    type: 'ORDER_CHANGE_CITERNE_CHOICE',
    num
  }
}


export function setExpertises(expertises){
  return {
    type: 'ORDER_CHANGE_EXPERTISE',
    expertises
  }
}

export function setMaxStep(maxStep){
  return {
    type: 'ORDER_CHANGE_MAXSTEP',
    maxStep
  }
}

export function setCurrentStep(currentStep){
  return {
    type: 'ORDER_CHANGE_CURRENTSTEP',
    currentStep
  }
}

export function setNewUser(newUserType,FirstName,Name,PhoneNumber,Email){
  return {
    type: 'ORDER_CHANGE_NEWUSER',
    newUserType,FirstName,Name,PhoneNumber,Email
  }
}
export function setNewUserAddress(address){
  return {
    type: 'ORDER_CHANGE_NEWUSERADDRESS',
    address
  }
}
export function setUser(user){
  return {
    type: 'ORDER_CHANGE_USER',
    user
  }
}


export function setProductsAndTotal(products,total,commission){
  return {
    type: 'ORDER_CHANGE_PRODUCTS_TOTAL',
    products,total,commission
  }
}
export function toggleConditionGenerale(checked){
  return {
    type: 'ORDER_CHANGE_CONDITION',
    checked
  }
}

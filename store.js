import { createStore, compose } from 'redux';
import { syncHistoryWithStore} from 'react-router-redux';
//import { browserHistory } from 'react-router';

import { useRouterHistory } from 'react-router'
import { createHashHistory } from 'history'
const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })


// import the root reducer
import rootReducer from './reducers/index';

//import comments from './data/comments';
//import posts from './data/posts';
//import test from './data/posts';
import order from './data/order';
// create an object for the default data


const defaultState = {
  //posts,
  //comments
  order
};

//the store
//const store = createStore(rootReducer, defaultState);
//the store with devtool
const store = /*(window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore)*/createStore(rootReducer, defaultState);

export const history = syncHistoryWithStore(appHistory, store);
/*
if(module.hot) {
  module.hot.accept('./reducers/',() => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
}*/


//return store;
export default store;

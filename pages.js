const pages = [
  { id: 1, url : '/', text : 'devis' },
  { id: 2, url : '/rendez-vous', text : 'coordonnees' },
  { id: 3, url : '/documents', text : 'documents' },
  { id: 4, url : '/validation', text : 'validation' }
]

export default pages

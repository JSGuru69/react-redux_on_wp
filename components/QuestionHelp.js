//<i class="fa fa-question-circle" aria-hidden="true"></i>
import React from 'react';
import T from '../traduction';


class QHelp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      isOpen : false
    }
  }

  toggle(event){
    event.preventDefault();
    event.stopPropagation();
    let state = {...this.state}
    state.isOpen = !state.isOpen
    this.setState(state)
  }

  render(){
    let imgurl = '/wp-content/themes/Certinergie/theme-js/commande/images/'+this.props.img
    let open = function(url){
      //window.open(url,'_blank');
    }
    let Img = (this.props.img)?<img src={imgurl} className="QuestionHelper-image" onClick={open.bind(null,imgurl)} />:null;

    let classHelpText = (Img)?'Questionhelptext Questionhelptext-with-img':'Questionhelptext'
    let classQuestionHelperHover = (Img)?'QuestionHelper-hover QuestionHelper-hover-img':'QuestionHelper-hover';
    let text = this.props.text && this.state.isOpen ? (
      <div className={classQuestionHelperHover}>
        {Img}
        <p className={classHelpText}>{T(this.props.text+'-text')}</p>
      </div>
    ):null

/*

     onClick={this.toggle.bind(this)}
onMouseEnter={this.toggle.bind(this)}
onMouseLeave={this.toggle.bind(this)}

.AppCommande p.Questionhelptext.Questionhelptext-with-img {
    margin-left: 60px!important;
}

.AppCommande .QuestionHelper-hover img.QuestionHelper-image {
    position: absolute;
    top: 24px;
    max-width: 60px;
    left: 7px;
    min-width: 60px;
    height: 60px;
}

*/
    return (
      <div className="QuestionHelper"
      onMouseEnter={this.toggle.bind(this)}
      onMouseLeave={this.toggle.bind(this)}
      >
        <i className="fa fa-question-circle"></i>
        {text}
      </div>
    )
  }
}

export default QHelp;

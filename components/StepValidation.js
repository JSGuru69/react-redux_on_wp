import React from 'react';
import {Link} from 'react-router';
import { history } from '../store';
import TransformToApi from '../data/transformToApi';

import R from 'browser-request';
import T from '../traduction';

//import subcomponents
import ResumeOrder from './ResumeOrder';
import ResumeRdv from './ResumeRdv';
import ResumeDocument from './ResumeDocument';
import ResumeRemarque from './ResumeRemarque';
import ResumeConditionGenerale from './ResumeConditionGenerale';
import ResumePrice from './ResumePrice';

import ResumeBDC from './ResumeBDC';
import ResumeActe from './ResumeActe';

import Pages from '../pages';
const pageId = 4;

let WAITING = false;
let NEED_TO_GO_UP_ON_ERROR = false;

class StepFive extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      waiting :false,
      errorMessage : null,
      emptyFields : {},
      isSuccess: false,
      needBdc:false,
      afterSuccessBody:null
    }
  }

  componentWillMount(){
    if(window.Certinergie && window.Certinergie.removeDevisBar && window.Certinergie.removeFooter){
      window.Certinergie.removeDevisBar()
      window.Certinergie.removeFooter()
    }
    if(window.Certinergie && window.Certinergie.goTop){
      window.Certinergie.goTop()
    }

    let {maxStep,currentStep} = this.props.order;

    this.props.setCurrentStep(pageId);
    if(maxStep < pageId){
      let path = Pages
                  .filter((item)=>item.id==maxStep)
                  .map((item)=>item.url)[0]

      history.push(path);
    }

  }

  render() {


    let {waiting,isSuccess} = this.state;
    let WaitingDiv =(waiting)? (<div className="waiting-save"><div className="waiting-spinner"><i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div></div>): null;


    let {address,contact,remarque,document,owner,bien,expertisesPrices,user,sendByMail,bdcSigned,isActe,acteinfo,buildings} = this.props.order;
    let {toggleSendByMail,setBdc,setActe,changeOwner,setAddress,setDocument} = this.props;

    let isConditionChecked = this.props.order.conditionGeneralChecked;

    let SaveBtnClass = "btn-next"+(isConditionChecked?' active':'');

    let Error = (this.state.errorMessage)?(<div className="Error" ref="Error">{T('Error-'+this.state.errorMessage)}</div>):null

    let ErrorTest = (this.state.errorMessage == 'test-error')?Error:null;
    let ErrorCondition = (this.state.errorMessage == 'error-condition-general')?Error:null;

    let successDiv = (isSuccess)?(<div className="success-save">
      {this.getSuccessContent()}
    </div>):null




    return (<div>
      <div className="Steps Step-Four">
        {ErrorTest}
        <ResumeOrder setAddress={setAddress} changeOwner={changeOwner} address={address} owner={owner} user={user} bien={bien} expertisesPrices={expertisesPrices} buildings={buildings} />
        <ResumeRdv setContact={this.props.setContact} contact={contact}/>
        <ResumeDocument isActe={isActe} acteinfo={acteinfo} document={document} user={user} owner={owner} sendByMail={sendByMail} toggleSendByMail={toggleSendByMail} setDocument={setDocument}/>
        <ResumeBDC document={document} ref="bdc" products={expertisesPrices.products} user={user} bdc={bdcSigned} needBdc={this.state.needBdc} setBdc={setBdc}/>
        <ResumeActe ref="acte" emptyFields={this.state.emptyFields} user={user} acte={isActe} acteinfo={acteinfo} setActe={setActe} document={document} />

        <ResumeRemarque remarque={remarque} setRemarque={this.props.setRemarque}/>
        <ResumePrice order={this.props.order} expertisesPrices={expertisesPrices} document={document} user={user}/>
        <ResumeConditionGenerale checked={isConditionChecked} toggleConditionGenerale={this.props.toggleConditionGenerale} error={ErrorCondition}/>
        {WaitingDiv}
        {successDiv}
      </div>

      <div id="stepvalidationbtn" className={SaveBtnClass}><a onClick={this.finaliser.bind(this)}> {T('finaliser-commande')}</a></div>
    </div>)
  }

  getSuccessContent(){
    return (<div className="success-popup Card Card-shadow">
      <h3>{T('success-popup.title')}</h3>
      <p>{T('success-popup.paragraphe-1')}</p>
      <p>{T('success-popup.paragraphe-2')}</p>
      <p>{T('success-popup.paragraphe-3')}</p>
      <p>{T('success-popup.paragraphe-4')} <span className="success-espaceclient" onClick={this.gotoEspaceClient.bind(this)}>{T('success-popup.dossierEnLigne')}</span></p>
      <span className="success-popup-btn" onClick={this.endAfterSuccess.bind(this)}>{T('success-popup.btn')}</span>
    </div>)
  }

  gotoEspaceClient(){
    window.location = this.state.afterSuccessBody.espaceClientUrl
  }

  endAfterSuccess(){

    localStorage.removeItem('Certinergie-Order-State');

    //localStorage.setItem('user',this.state.afterSuccessBody);
    //localStorage.setItem('logged',JSON.stringify(true));

    //alert('dois env un mail au client, me retourné l utilisateur qui a pris la commande (pour le connecter automatinquement) et le lien de son espace client pour le lien du popup')

    window.location = '/'
  }

  componentDidUpdate(){
    let nbOfEmpty = Object.keys(this.state.emptyFields).length;
    let higher = Infinity;
    let scroll = $(window).scrollTop()
    if(nbOfEmpty>0 && NEED_TO_GO_UP_ON_ERROR){
      for(var i in this.state.emptyFields){
        let field = this.state.emptyFields[i]
        let top = scroll+(field && field.getBoundingClientRect)?field.getBoundingClientRect().top:Infinity
        if(top < higher){
          higher = top
        }
      }

      if(window.Certinergie && window.Certinergie.goTop && higher!= Infinity &&  !isNaN(higher)){
        window.Certinergie.goTop(higher-250)
      }
    }else{
      var Error = this.refs.Error;
      if(Error && NEED_TO_GO_UP_ON_ERROR){
        let y = Error.offsetTop

        let scroll = $(window).scrollTop()
        let h = $(window).height();

        if(y < scroll || (scroll+h) < y){

          if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
            window.Certinergie.goTop(y-250)
          }
        }
      }
    }


  }

  finaliser(event){
    ga("send", "event", "bouton", "clic", "étape 4 valider");
    let emptyFields = null
    for(var i in this.refs){
      let current = this.refs[i]
      for(var j in current.refs){
        let currentB = current.refs[j];
        if(currentB.value.replace(/ /g,'') == ''){
          emptyFields = emptyFields || {}
          emptyFields[j] = currentB
        }
      }
    }

    let errorMessage = [];
    let needBdc = false;

    let {user} = this.props.order;

    //si agence
    let needBdcAgence = (user.Consumer.ConsumerType == 1689 && (
      document.is == 'other' ||
      document.is == 'proprio-bien' ||
      document.is == 'proprio' ||
      document.is == 'notaire' ||
      document.is == 'proprioCoNotaire'
    ))?true:false;

    //si agence
    let needBdcNotaire = (user.Consumer.ConsumerType == 8122 && (
      document.is == 'other' ||
      document.is == 'proprio-bien' ||
      document.is == 'proprio'
    ))?true:false;

    //if(needBdcNotaire || needBdcAgence){
      if(this.refs.bdc && !this.props.order.bdcSigned){
        //needBdc = true;
        errorMessage.push('error-bdc-not-signed')
      }
    //}

    if(!this.props.order.conditionGeneralChecked){
      errorMessage.push('error-condition-general')
    }

    if((errorMessage && errorMessage.length>0) || emptyFields || needBdc){
      event.preventDefault();
      let state = {...this.state}
      state.errorMessage = errorMessage.length>0?errorMessage[0]:null;
      state.emptyFields=emptyFields||{}
      NEED_TO_GO_UP_ON_ERROR = true;
      state.needBdc = needBdc;
      this.setState(state);
      return false;
    }else{
      if(!this.state.waiting){
        let newState = {...this.state}
        newState.waiting = true;
        this.setState(newState)

        console.clear()
        //console.log("this.props",this.props)
        //console.log(this.props)
        let transformedToApi = TransformToApi(this.props.order);

        let postBody = JSON.stringify(transformedToApi);
        //console.log("postBody",postBody)
        window._commande = {
          raw : transformedToApi,
          str : postBody
        }

        R({
          method:'POST',
          url:'/wp-content/themes/Certinergie/api/newOrderProxy.php',
          body:postBody
        }, function(err, response, body){

          if(!err){
            if(body){
              body = JSON.parse(body)
              if(body.Data){

                let user;
                if(body.Data.UserID){

                  user = {...body.Data};
                  user.Addresses = (user.Addresses)?[user.Addresses]:[]
                  user.Consumer = user.Consumers[0]
                  delete user.Consumers

                  localStorage.setItem('user',JSON.stringify(user));
                  localStorage.setItem('logged',JSON.stringify(true));

                  Certinergie.initLoginSystem()
                }else{
                  user = localStorage.getItem('user')
                  if(user){
                    user = JSON.parse(user);
                  }
                }

                localStorage.removeItem('Certinergie-Order-State');


                let newState = {...this.state}
                newState.waiting = false;
                newState.isSuccess = true;

                user.espaceClientUrl = "http://client.certinergie.be/fr/Session/LogInUser?userID="+user.UserID;

                newState.afterSuccessBody = user
                this.setState(newState)
              }else{
                alert('an error as occured')
              }

            }
          }
        }.bind(this))
      }
    }
  }

};

export default StepFive;

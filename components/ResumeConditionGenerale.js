import React from 'react';

import T from '../traduction';

class ResumeConditionGenerale extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      isChecked : (props && props.checked)?true:false
    }
  }
  onClick(e){
    let newCheck = !this.state.isChecked
    this.setState({
      isChecked:newCheck
    });
    this.props.toggleConditionGenerale(newCheck)
  }
  render() {

    let link = {
      url : T('resume-conditiongenerale.linkurl'),
      text : T('resume-conditiongenerale.linktext'),
      target:'_blank'
    }
    //      <div className="Card Card-shadow Resume-ConditionGenerale" onClick={this.onClick.bind(this)}>
    let classCG = 'Resume-ConditionGenerale '+(!!this.props.error&&!this.state.isChecked?' red-border':'');

    return (
      <div className={classCG} >
        <input type="checkbox" className="regular-checkbox" checked={this.state.isChecked} onClick={this.onClick.bind(this)} />
        <span className="marginleft">{T('resume-conditiongenerale.phrase',{link})}</span>
      </div>
    )
  }
};

export default ResumeConditionGenerale;

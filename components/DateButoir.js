import React from 'react';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import QH from './QuestionHelp';
import T from '../traduction';


class DateButoir extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      date : props.date,
      momentDate: (props.date)?moment(props.date):null
    }
  }

  render(){

    return (
      <div className="DateButoir">
        <h4>{T('date-butoir.titre')}<QH text="why-date-butoir" /></h4>
        <div className="DateButoir-DatePicker">
        {this.displayDatePicker()}
        </div>
      </div>
    )
  }

  handleChange(momentDate){
    let newState = {...this.state}

    if(momentDate && momentDate._d){
      let date = momentDate._d;
      newState.date = date;
      newState.momentDate = momentDate
    }

    this.setState(newState);
    if(this.props.onChange){
      this.props.onChange(newState.date);
    }
  }

  onClickAddDate(e){
    e.preventDefault();


  }


  displayDatePicker(){

    let locale = Certinergie.lang() == 'nl'? 'nl-be':
                 Certinergie.lang() == 'en'? 'en-gb':
                 'fr-be';

    return (<div className="DB-Select">
      <DatePicker
        dateFormat="DD/MM/YYYY"
        locale={locale}
        placeholderText={T('info-contact.date-picker-placeholder')}
        className="DB-Picker"

        onChange={this.handleChange.bind(this)}

        selected={this.state.momentDate}

        filterDate={ this.isWeekday }
        minDate={moment().add(2,'days')}
      />
    </div>)
  }

  isWeekday(momentDate){
    return (momentDate.isoWeekday()<6)
  }



}

export default DateButoir;

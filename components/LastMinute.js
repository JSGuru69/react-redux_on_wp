import React from 'react';
import R from 'browser-request';


import T from '../traduction';

class LastMinute extends React.Component{

  constructor(props){
    super(props);
    let {region,expertises,bien} = props;
    this.state = {lastminutes: []}
    this.getLastMinute(region,expertises,bien);
  }

  componentWillReceiveProps(newProps){
    let {region,expertises,bien} = newProps;

    this.getLastMinute(region,expertises,bien);
  }

  render() {
    let {lastminutes} = this.state;

    let lastMinutesDiv = lastminutes.map((lm)=>{

      let lastminutesClass = "lastMinutesItem"+(lm.LastMinuteID == this.props.lastMinutesSelected?' selected':'');

      return (
        <div clasName={lastminutesClass}>
          {lm.Date}<br/>
          {lm.Price || 'Nothing'}
        </div>
      );
    })
    if(lastMinutesDiv.length==0){
      return null
    }
    return (
      <div className="LastMinute">
        <h3>Last minutes</h3>
        <p>Bénéficiez d'un prix réduit en choisissant une de ces dates de visites</p>
        <div>{lastMinutesDiv}</div>
      </div>
    )
  }

  getLastMinute(r,e,b){

    let types = [
      { buildingType : 'a' , size : 1 , typeId : 1 },
      { buildingType : 'a' , size : 0 , typeId : 11 },
      { buildingType : 'a' , size : 2 , typeId : 12 },
      { buildingType : 'a' , size : 3 , typeId : 13 },
      { buildingType : 'm' , size : 0 , typeId : 7 },
      { buildingType : 'm' , size : 1 , typeId : 9 },
      { buildingType : 'm' , size : 2 , typeId : 8 },
      { buildingType : 'm' , size : 3 , typeId : 10 },
      { buildingType : 'm' , size : 4 , typeId : 5 },
      { buildingType : 'm' , size : 5 , typeId : 6 }
    ];

    let bien = -1;
    let filtered = types.filter(t=> t.buildingType==b.buildingType && t.size == b.size)
    if(filtered.length>0){
      bien =  filtered[0].typeId;
    }


    let expertizes = e.map(ex=>ex.id).join('-');

    if(r && bien && expertizes){
      R('http://www.certinergie.be/wp-content/themes/Certinergie/api/lastminutes.php?'+'region='+r+'&estate='+bien+'&type='+expertizes,(err,res,body)=>{

        let newState = {...this.state}
        newState.lastminutes = JSON.parse(body);
        this.setState(newState);
      });
    }

  }


};

export default LastMinute;

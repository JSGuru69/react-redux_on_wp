import React from 'react';
import CI from './CustomInput';

import T from '../traduction';

class ResumeOrder extends React.Component{
  constructor(props){
    super(props);
    let state = { address: props.address,
                   owner : props.owner ,
                   openEditPopup : false,
                   isOwner:true
                 }
   let {user} = props;
   if(!user.UserID && user.newUserType == "1688"){
     state.owner.firstname = user.FirstName;
     state.owner.lastname = user.Name;
     state.owner.phone = user.PhoneNumber;
     state.owner.email = user.Email;
     state.isOwner=false;
   }

   this.state = state;

  }
  render() {

    let {street,numbox,zip,city} = this.state.address;
    let {firstname,lastname,phone} = this.state.owner;
    let {user,bien} = this.props;
    //console.log(this.props)
    //console.log(bien.size)
    //console.log(bien.count)

    let bienPhrase =  "";
    let url= window.location.href;
    if(url.indexOf("/nl/") !== -1){
      bienPhrase = 'Huis'
     if(bien.buildingType == 'a'){
         bienPhrase = 'Appartement';
      }
      else if(bien.buildingType == 'i'){
        bienPhrase = 'Flatgebouw';
      }
     }
     else{
         bienPhrase = 'Maison'
         if(bien.buildingType == 'a'){
             bienPhrase = 'Appartement';
         }  
        else if(bien.buildingType == 'i'){
          bienPhrase = 'Immeuble d\'appartement';
        }
     }
/*
    (
      bien.buildingType == 'a' ? 'Appartement':
      bien.buildingType == 'i' ? 'Immeuble d\'appartement':
      bien.buildingType == 'm' ? 'Maison' : 'Maison'
    )*/
    bienPhrase= bienPhrase+' ' +T(this.props.buildings[bien.buildingType][bien.size!=-1?bien.size:bien.count].name+'-resume')

    let {total,products} = this.props.expertisesPrices;

    let productsLi = products.map((product,i)=>(<li key={i}>
                                                  <span className="resume-tarif-name">{product.name}</span>
                                                  <span className="resume-tarif-price">{product.price}€</span>
                                                </li>))


    let imgSrc = (bien.buildingType == 'm')?'/wp-content/themes/Certinergie/theme-js/commande/Icones/Bleu/maison.png':
                 (bien.buildingType == 'a')?'/wp-content/themes/Certinergie/theme-js/commande/Icones/Bleu/apparement.png':
                 '/wp-content/themes/Certinergie/theme-js/commande/Icones/Bleu/immeuble.png'

    let TARIF = (<ul className="resume-tarif">
      {productsLi}
      <li className="resume-total">
        <span className="resume-tarif-name">Total TVAC</span>
        <span className="resume-tarif-price">{total}€</span>
      </li>
      </ul>)

    let productListLi = products.map((product,i)=>{
      if(product.productId == 5 ||product.productId == -1){
        return null;
      }
      return (<li key={i}>{product.name}</li>)
    })
    let productListUl = (<ul>{productListLi}</ul>)
//      <div className="Card Card-shadow Resume-Order">

    let POPUP = (this.state.openEditPopup)?this.getPopup():null


    return (
      <div className="Resume-Order">
        <div>
          <div className="itable fifty">
            <h3>{T('resume-order.titre')}</h3>
            <span className="resume-order-estate">{bienPhrase}</span>
            {productListUl}
          </div>
          <div className="itable fifty">
            <h3>{T('resume-order.titre.right')}</h3>
            <div>
              <div className="resume-order-picture">
                <img src={imgSrc} />
              </div>
              <div className="resume-order-name-address">
                {lastname}&nbsp;{firstname} <br/>
                {street}&nbsp;{numbox} <br/>
                {zip}&nbsp;{city}
                <div className="edit-btn" onClick={this.openEditPopup.bind(this)}><i className="fa fa-pencil-square-o"></i></div>
              </div>
            </div>

          </div>
        </div>
        {POPUP}
      </div>
    )
  }

  //<div className="edit-btn" onClick={this.openEditPopup.bind(this)}><i className="fa fa-pencil-square-o"></i></div>

  openEditPopup(e){
    let state = {...this.state};
    state.openEditPopup = true;
    this.setState(state);
  }
  closeEditPopup(e){
    let state = {...this.state};
    state.openEditPopup = false;
    this.setState(state);
  }

  onClickPopupBtn(e){
    let state = {...this.state}

    this.props.changeOwner(state.isOwner,state.owner);
    let {street,numbox,zip,city} = state.address;
    //this.props.changeAddress(street,numbox,zip,city)
    this.closeEditPopup();
  }

  onInputPopupOwner(e){
    let name = e.target.name;
    let value = e.target.value;
    let state = {...this.state}
    state.owner[name]=value;
    this.setState(state)
  }
  onInputPopupAddress(e){
    let name = e.target.name;
    let value = e.target.value;
    let state = {...this.state}
    state.address[name]=value;
    this.setState(state)
  }
  onZipCityChange(e){
  }

  getPopup(){
    let {street,numbox,zip,city} = this.state.address;
    let {firstname,lastname,phone} = this.state.owner;
    return (
      <div className="popup-dark">
        <div className="popup popup-order-edit Card Card-shadow">
          <h3>{T('editorder')}</h3>
          <form>
            <CI label="info-owner.lastname" value={lastname} name="lastname" onInput={this.onInputPopupOwner.bind(this)} />
            <CI label="info-owner.firstname" value={firstname} name="firstname" onInput={this.onInputPopupOwner.bind(this)} />
            <CI isStreet={true} label="info-address.street" value={street} name="street" onInput={this.onInputPopupAddress.bind(this)} />
            <CI isNumbox={true} label="info-address.numbox" value={numbox} name="numbox" onInput={this.onInputPopupAddress.bind(this)} />
            <CI isAddress={true} label="info-address.zipcity" name="zipcity" zip={zip} city={city} onChange={this.onZipCityChange.bind(this)} />
          </form>
          <span className="form-send" onClick={this.onClickPopupBtn.bind(this)}>{T('popup-btn')}</span>
          <div className="close-popup" onClick={this.closeEditPopup.bind(this)}><i className="fa fa-times"></i></div>
        </div>
      </div>
    )
  }

};

export default ResumeOrder;

import React from 'react';

import T from '../traduction';

import CI from './CustomInput';
import QH from './QuestionHelp';
import EstateTypes from '../data/EstateTypes';


const APPART = 'a';
const MAISON = 'm';
const IMMEUB = 'i';

class BienSelect extends React.Component{

  constructor(props){
    super(props);
    this.state = props.bien;
    this.state.openPopupCallUs = false
  }

  render() {

    let {buildingType} = this.state;

    let typeBien = (<div className="bien-select-type">{[MAISON,APPART,IMMEUB].map((id)=> this.renderTypeBien(id))}</div>);

    let superficie = (buildingType == MAISON || buildingType == APPART)? this.renderSuperficie():null;

    let count = (buildingType == IMMEUB)? this.renderCount() : null;

    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }

    let popupCallUs = (this.state.openPopupCallUs)?this.getPopupCallUs():null

    return (<div className="Bien-Select">
        <div className="Card Card-shadow">
          <h2>2. {T('bien-select.titre')} <QH text="peb-description-info" /></h2>
          {typeBien}
          <div className={animationClass(superficie)}>{superficie}</div>

          <div className={animationClass(count)}>{count}</div>
        </div>
        {popupCallUs}
      </div>);

  }

  getPopupCallUs(){
    return (<div className="popup-dark">
      <div className="popup  Card Card-shadow">
        <h3>{T('call-us-popup.titre')}</h3>
        <div className="call-up-popup-text">
          <p>{T('call-us-popup.text.a')}<br/>{T('call-us-popup.text.b')}<span className="call-us-popup-phone">0800 82 171</span>{T('call-us-popup.text.c')}<br/>{T('call-us-popup.text.d')}</p>
        </div>
        <div className="call-up-popup-form">
        <form>
          <CI label="form-info-nom" />
          <CI label="form-info-mail" />
          <CI label="form-info-phone" />

          <CI isStreet={true} label="form-bien-street" />
          <CI isNumbox={true} label="form-bien-numbox" />
          <CI label="form-bien-zipcity" />
          <CI isTextArea={true} label="form-message" />

          </form>
          <span className="form-send" onClick={this.clickAndSend.bind(this)} >{T('form-info-btn')}</span>

        </div>
        <div className="close-popup" onClick={this.closePopupCallUs.bind(this)}><i className="fa fa-times"></i></div>

      </div>
    </div>)
  }

  clickAndSend(){}

  closePopupCallUs(){
    let state = {...this.state}
    state.openPopupCallUs = false;
    this.setState(state)
  }

  onClickTypeBien(id){
    let newState = {...this.state}
    newState.buildingType = id;

    if(id==APPART||id==MAISON){
      newState.count = -1;
      newState.size = -1;
    }
    if(id==IMMEUB){
      newState.size = -1;
      newState.count = -1;
    }

    this.setState(newState)
    this.props.setBien(newState.buildingType,newState.count,newState.size,this.getEstateType(newState.buildingType,newState.size));


    //clear product
    this.props.setExpertises([]);

  }

  renderTypeBien(id){

    let className = "bien-select-type-item"+( (id == this.state.buildingType)?' active' : '');

    let color = (id == this.state.buildingType)?'Blanc':'Bleu'
    let url = '/wp-content/themes/Certinergie/theme-js/commande/Icones/'+color+'/'
    let bienImg = (id == 'a')? url+'apparement-cadre.png':
                    (id == 'm')? url+'maison.png':
                    url+'immeuble.png'

    return (<div className={className} key={id} onClick={this.onClickTypeBien.bind(this,id)}>
      <img src={bienImg} className="bien-select-image" />
      <span className="bien-select-name">{T('bien-select.bien.'+id)}</span>
    </div>)

  }

    getEstateType(buildingType,size){
      let types = EstateTypes;
      /*let types = [
        { buildingType : 'a' , size : 1 , typeId : 1 },
        { buildingType : 'a' , size : 0 , typeId : 11 },
        { buildingType : 'a' , size : 2 , typeId : 12 },
        { buildingType : 'a' , size : 3 , typeId : 13 },

        { buildingType : 'm' , size : 0 , typeId : 14 },
        { buildingType : 'm' , size : 1 , typeId : 7 },
        { buildingType : 'm' , size : 2 , typeId : 9 },
        { buildingType : 'm' , size : 3 , typeId : 8 },
        { buildingType : 'm' , size : 4 , typeId : 10 },
        { buildingType : 'm' , size : 5 , typeId : 5 },
        { buildingType : 'm' , size : 6 , typeId : 6 },
        { buildingType : 'm' , size : 7 , typeId : 6 },
        { buildingType : 'm' , size : 8 , typeId : 6 },
        { buildingType : 'm' , size : 9 , typeId : 6 }
*/
        /*
        {id:0,name:'bien-maison-1 chambres'},{id : 1, name:'bien-maison-2 chambres'},{id :2, name:'bien-maison-3 chambres'},
        {id : 3, name:'bien-maison-4 chambres'},{id : 4, name:'bien-maison-5 chambres'},{id : 5, name:'bien-maison-6 chambres'},
        {id : 6, name:'bien-maison-Villa'},{id : 7, name:'bien-maison-GrandeVilla'},{id:100,name:'bien-maison-needtocall'}
        */
      /*];*/

      let filtered =types.filter(t=> t.buildingType==buildingType && t.size == size)
      if(filtered.length>0){
        return filtered[0].typeId;
      }


      return 0;
    }

  //superficie only happend on appartement and maison (a,m)

  renderSuperficie(){

    let moreSuperficie = [];
    let normalSuperficie = this.props.buildings[this.state.buildingType].map((size,i)=>{

      if(this.state.buildingType == 'm' && i > 3){



          if(i>4){

            if((this.state.size >= 6 && this.state.size <8) || this.state.size == 4){
              if(i >=6 && i<8){
                moreSuperficie.push(this.renderSuperficieItem(size));
              }
              if(size.id ==100){
                moreSuperficie.push(this.renderSuperficieItem(size));
              }
            }

            if((this.state.size >= 8 && this.state.size <10)|| this.state.size == 5){
              if(i >=8 && i<10){
                moreSuperficie.push(this.renderSuperficieItem(size));
              }
              if(size.id ==100){
                moreSuperficie.push(this.renderSuperficieItem(size));
              }
            }

        }

        return (i == 4)?this.renderSuperficieItem(size,this.state.size >= 6 && this.state.size < 8):
               (i == 5)?this.renderSuperficieItem(size,this.state.size >= 8 && this.state.size <10):null
      }else{
        return this.renderSuperficieItem(size)
      }
    })

    let more = null;
    if(moreSuperficie && moreSuperficie.length>0){
      more = (<div>
        <h4>{T('bien-select.superficie-'+this.state.buildingType+'.subtitre')}</h4>
        {moreSuperficie}
      </div>)
    }

    return (<div className="bien-select-superficie">
      <h4>{T('bien-select.superficie-'+this.state.buildingType+'.titre')}</h4>
      <div>{normalSuperficie}</div>
      {more}
    </div>)

  }

  onClickSuperficieItem(id){

    let newState = {...this.state}

    if(id==100){
      newState.openPopupCallUs = true
    }else{

      newState.size = id;
      this.props.setBien(newState.buildingType,newState.count,newState.size);

    }

    this.setState(newState);

  }

  renderSuperficieItem(building,actif){
    let className= 'bien-select-superficie-item'+ ((this.state.size == building.id ||actif)?' active':'');

    return (<div className={className} key={building.id} onClick={this.onClickSuperficieItem.bind(this,building.id)}>
      { T(building.name) }
    </div>)

  }


  //count only happend on immeuble appartement (i)

  renderCount(){
    return (<div className="bien-select-count">
      <h4>{T('bien-select.count.titre')}</h4>
      {this.props.buildings[this.state.buildingType].map((num)=>this.renderCountItem(num))}
    </div>)
  }
  onClickCountItem(id){
    let newState = {...this.state}
    newState.count = id
    this.setState(newState)
    this.props.setBien(newState.buildingType,newState.count,newState.size);
  }
  renderCountItem(service){

    let className= 'bien-select-count-item'+ ((this.state.count == service.id)?' active':'')
    return <div className={className} key={service.id} onClick={this.onClickCountItem.bind(this,service.id)}>{service.id}</div>
  }

};

export default BienSelect;

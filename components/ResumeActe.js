import React from 'react';

import T from '../traduction';

import QH from './QuestionHelp';
import CI from './CustomInput';

class ResumeActe extends React.Component{

  constructor(props){
    super(props);
    let acteinfo = this.props.acteinfo

    if(this.props.user.Consumer.ConsumerType == 8122 && acteinfo.name == ''){
      acteinfo.name = this.props.user.FirstName+' '+this.props.user.Name
    }

    this.state = acteinfo

    this.DebounceChangeActeInfo = _.debounce(this.changeActeInfo,1000);

  }

  render() {

    let {user,document} = this.props;
    let acte = this.props.acte;
    let prefill = null;

    //if(user.Consumer.ConsumerType == 1689 && document.is == 'otherNotaire'){
    //  acte = true;
    //  prefill = {...document}
    //}

    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }

    //<div className={animationClass(openForm)}>{openForm}</div>
    /*

    <p className="explication">
      Merci de nous informer de la date de l'acte via l'onglet "message" dans votre espace client <br/>
      A défaut de paiement par le notaire ou de refus de celui-ci, le demandeur est considéré avoir commandé en son nom et pour son compte.
    </p>
    */

    let AGENCE_NOTAIRE = null;
    let CHECK_ACTE = null;
    let Normal = null;
    if(user.Consumer.ConsumerType == 1689 || user.Consumer.ConsumerType == 8122){
      //        <div className="Card Card-shadow Resume-Acte">

      let phrase = user.Consumer.ConsumerType == 1689? 'resume-acte-phrase-agence' :
                   user.Consumer.ConsumerType == 8122? 'resume-acte-phrase-notaire' : ''

      let openForm = (acte && user.Consumer.ConsumerType == 8122)?this.getForm(prefill):null;
      let Phrase = (phrase)?<span>{T(phrase)}</span>:null

      let Sub = acte?(<p className="explication">
        {T('resume-acte-phrase2')}<br/>{Phrase} <QH text="resume-acte-question"/>
      </p>):null

      CHECK_ACTE = (<div className="choosePayment"><input type="checkbox" className="regular-checkbox" checked={acte} onClick={this.handleChange.bind(this,true)} />
                        <span className="modepaymentlabel">{T('resume-acte.delai.acte.label')}</span>
                        <span className="modepaymentdata">{T('resume-acte.delai.acte.data')}</span>
                        {Sub}{openForm}
                        </div>);
      /*AGENCE_NOTAIRE = (<div></div>)*/

      Normal = !acte?(<p className="explication">
        {T('resume-acte-normal.phrase')}
      </p>):null
    }


    let Title = (<h3>{T('resume-acte.titre')}</h3>);
    /*
    let Modenormal = (<div className="paymentnormal">
                          <div className="itable fifty"><input type="checkbox" className="regular-checkbox" checked={!acte}onClick={this.handleChange.bind(this,false)} />{T('resume-acte.delai.normal.label')}</div>
                          <div className="itable fifty">{T('resume-acte.delai.normal.data')}</div>
                    </div>)
  */
    let Modenormal = (<div className="choosePayment">
                        <input type="checkbox" className="regular-checkbox" checked={!acte}onClick={this.handleChange.bind(this,false)} /><span className="modepaymentlabel">{T('resume-acte.delai.normal.label')}</span>
                        <span className="modepaymentdata">{T('resume-acte.delai.normal.data')}</span>
                     </div>);

    let Facture = (<div className="ActeFacture">{T('resume-acte.payment.facture.label')}{T('resume-acte.payment.facture.data')}</div>);
    let Label = null;
    if(AGENCE_NOTAIRE == null && CHECK_ACTE == null){
      Title=(<h3>{T('resume-acte.titre.particulier')}</h3>);
      Label = (<div className="paymentparticulierlabel">{T('resume-acte.payment.label')}</div>)
      Modenormal = (<div className="paymentparticulier">
                      <ul>
                        <li>{T('resume-acte.payment.espece.part1')}<span className="paymentparticulierkeyword">{T('resume-acte.payment.espece.keyword')}</span>{T('resume-acte.payment.espece.part2')}</li>
                        <li>{T('resume-acte.payment.virment.part1')}<span className="paymentparticulierkeyword">{T('resume-acte.payment.virment.keyword')}</span>{T('resume-acte.payment.virment.part2')}</li>
                      </ul>
                    </div>)
                    /*
      Modenormal = (<div className="choosePayment"><span className="paymentparticulierlabel">{T('resume-acte.payment.espece.label')}</span><span className="paymentparticulierdata">{T('resume-acte.payment.espece.data')}</span></div>);
      Virment = (<div className="choosePayment"><span className="paymentparticulierlabel">{T('resume-acte.payment.virment.label')}</span><span className="paymentparticulierdata">{T('resume-acte.payment.virment.data')}</span></div>);
*/
    }

    return (
      <div className="Resume-Acte">
        {Title}
        {Label}
        {Modenormal}

        {CHECK_ACTE}

        {Facture}
      </div>
    )
  }

  handleChange(value,e){
    this.props.setActe(value,this.state)
  }

  isEmptyFieldPutRedClass(name){
    return this.props.emptyFields && this.props.emptyFields[name]?'empty-input-field':''
  }

  getForm(doc){
    let acteinfo = this.state
    acteinfo.name =  (doc && doc.name && acteinfo.name == '')?doc.name:acteinfo.name;

    return (<div className="acte-form">
      <form>
        {/*<CI label='acte-form.notaireName' qh="why-acte-notaireName" name="name"
            ref="required-acte-name" value={acteinfo.name} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-acte-name')}
            onInput={this.onKeyup.bind(this)} onChange={this.DebounceChangeActeInfo.bind(this)} />*/null}

        <CI label='acte-form.numNational'  name="numNational"
            ref="required-acte-numNational" value={acteinfo.numNational} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-acte-numNational')}
            onInput={this.onKeyup.bind(this)} onChange={this.DebounceChangeActeInfo.bind(this)} />

        {/*<CI label='acte-form.acteDate' qh="why-acte-acteDate" name="acteDate"
            ref="required-acte-acteDate" value={acteinfo.acteDate} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-acte-acteDate')}
            onInput={this.onKeyup.bind(this)} onChange={this.DebounceChangeActeInfo.bind(this)} />*/null}

      </form>
      {null/*<p className="explication">{T('acte-form.explication')}</p>*/}
    </div>)
  }

  onKeyup(e){
    let state = {...this.state}
    let {value,name} = e.target

    state[name]=value;
    this.setState(state)
  }
  changeActeInfo(e){
    this.props.setActe(this.props.acte,this.state)
  }

};

export default ResumeActe;
/*
  Nom du notaire
  date naissance ou registre nationnal du proprio
  date de l'acte si possible
*/

import React from 'react';

import T from '../traduction';

class ResumeRemarque extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        remarque : props.remarque
    };

    //create a debounced onChange method
    this.debouncedOnChange = _.debounce(this.onChange,1000);
  }

  onKeyUp(e){
    e.preventDefault();
    let {value,name} = e.target;
    let state = {
      remarque : value
    }
    this.setState(state);
  }

  onChange(e){
    //call the same logic as in onKeyUp method
    this.onKeyUp.bind(this,e);
    let {remarque} = this.state;
    this.props.setRemarque(this.state.remarque);
  }

  render() {
    //      <div className="Card Card-shadow Resume-Remarque">

    return (
      <div className="Resume-Remarque">
        <h3>{T('resume-remarque.titre')}</h3>
        <div>
          <div className="">{T('resume-remarque.phrase')}</div>
          <textarea defaultValue={this.props.remarque} className="remarque-textarea" onInput={this.onKeyUp.bind(this)} onChange={this.debouncedOnChange.bind(this)}></textarea>
        </div>
      </div>
    )
  }
};

export default ResumeRemarque;

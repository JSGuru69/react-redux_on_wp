import React from 'react';

import ResumeClauseMandat from './ResumeClauseMandat';
import T from '../traduction';

class ResumeBDC extends React.Component{

  constructor(props){
    super(props);
    this.state = {}
  }

  render() {

    let {user,document,products} = this.props;


    //si agence
    let needBdcAgence = (user.Consumer.ConsumerType == 1689 && (
      document.is == 'other' ||
      document.is == 'proprio-bien' ||
      document.is == 'proprio' ||
      document.is == 'notaire' ||
      document.is == 'proprioCoNotaire'
    ))?true:false;

    //si agence
    let needBdcNotaire = (user.Consumer.ConsumerType == 8122 && (
      document.is == 'other' ||
      document.is == 'proprio-bien' ||
      document.is == 'proprio'
    ))?true:false;


    if(needBdcNotaire || needBdcAgence){
      //let classBDC = "Card Card-shadow Resume-BDC"+(this.props.needBdc?' needBdc':'');
      let classBDC = "Resume-BDC"+(this.props.needBdc?' needBdc':'')+(!this.props.bdc?' red-border':'');

      let EXPLICATION_NO_CHECK = (!this.props.bdc)?<p className="explication no-check">{T('resume-bdc.phrase3')}</p>:null;

      return (
        <div className={classBDC}>
          <h3>{T('resume-bdc.titre')}</h3>
          <p><input type="checkbox" className="regular-checkbox" checked={this.props.bdc} onChange={this.handleChangeChk.bind(this)} />
          {T('resume-bdc.phrase1')}</p>
          <p className="explication"> {T('resume-bdc.phrase2')}</p>
          {EXPLICATION_NO_CHECK}

          <ResumeClauseMandat user={user} products={products} />

        </div>
      )
    }else{
      return null;
    }
  }

  handleChangeChk(e){
    this.props.setBdc(e.target.checked)
  }
};

export default ResumeBDC;

import React from 'react';
import T from '../traduction';
import QH from './QuestionHelp';
import CI from './CustomInput';

const PEB = 1;
const PEB_Partiel = 11;
const ELEC = 4;
const CITERNE = {
  E : 6,
  A : 9
}
const PLAN = 14

class ServiceSelect extends React.Component{

  constructor(props){
    super(props);

    let needPartiel = ( (props.partielNumber && props.partielNumber != '') || (props.expertises.reduce((index,current,i)=>{
      if(current.id == PEB_Partiel){
        index = i;
      }
      return index;
    },-1) !== -1));

    this.state = {
      expertises : props.expertises || [],
      elecCount : props.elecCount || 0,
      citerneChoice :props.citerneChoice || -1,
      planCount : props.planCount||0,
      partielNumber : props.partielNumber||'',
      needPartiel : needPartiel
    }

    this.DEBOUNCEDchangePartielNumber = _.debounce(this.changePartielNumber,1000);
  }


  render() {
    let services = this.renderServices();

    let hasElec = this.props.expertises.reduce((index,current,i)=>{
      if(current.id == ELEC){
        index = i;
      }
      return index;
    },-1) !== -1;

    let hasCiterne = this.props.expertises.reduce((index,current,i)=>{
      if(current.id == CITERNE.E+''+CITERNE.A){
        index = i;
      }
      return index
    },-1) !==-1;

    let hasPlan = this.props.expertises.reduce((index,current,i)=>{
      if(current.id == PLAN){
        index = i;
      }
      return index;
    },-1) !== -1;

    let hasPeb = this.props.expertises.reduce((index,current,i)=>{
      if(current.id == PEB){
        index = i;
      }
      return index;
    },-1) !== -1;

    let hasPebPartiel = this.props.expertises.reduce((index,current,i)=>{
      if(current.id == PEB_Partiel){
        index = i;
      }
      return index;
    },-1) !== -1;

    let elecCount = hasElec?this.renderElecCount():null;
    let citerneChoice = hasCiterne?this.renderCiterneChoice():null;
    let planCount =  hasPlan?this.renderPlanCount():null;
    let pebPartielChoice = hasPeb?this.renderPebPartielChoice(hasPebPartiel):null;

    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }
    return (
      <div className="Service-Select">
        <div className="Card Card-shadow">
          <h2>3. {T('service-select.titre')}</h2>
          {services}
          <div className={animationClass(pebPartielChoice)}>{pebPartielChoice}</div>
          <div className={animationClass(elecCount)}>{elecCount}</div>
          <div className={animationClass(citerneChoice)}>{citerneChoice}</div>
          <div className={animationClass(planCount)}>{planCount}</div>
        </div>
      </div>
    )
  }

  //when partiel
  renderPebPartielChoice(hasPebPartiel){
    let {region,bien} = this.props;
    region = parseInt(region);

    let getBtnClass = val => ('service-select-peb-partiel-btn'+(val==this.state.needPartiel?' active':''));

    let buttons = (<div>
      <div className={getBtnClass(true)}  onClick={this.onBtnPartielClick.bind(this,true)} >Oui</div>
      <div className={getBtnClass(false)} onClick={this.onBtnPartielClick.bind(this,false)}>Non</div>
    </div>)

    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }

    let pebPartielExpertizeClass = 'service-select-item-peb-partiel'+(hasPebPartiel?' active':'')

    let form = this.state.needPartiel ?(<div className="service-select-peb-partiel-form">
                  <div className="service-select-peb-partiel-form-existing-num">
                    <form>
                      <CI defaultValue={this.state.partielNumber} onInput={this.inputPartielNumber.bind(this)} onChange={this.DEBOUNCEDchangePartielNumber.bind(this)} label="service-select-peb-partiel-form-existing-num" qh="peb-partiel-info"/>
                    </form>
                  </div>
                  <div className={pebPartielExpertizeClass} onClick={this.togglePebPartiel.bind(this)}>Je commande un PEB partiel </div>
                </div>):null


    if(region == 0 && (bien.buildingType == 'a' || bien.buildingType == 'i')){
      return (<div className="service-select-peb-partiel-choice">
        <h4>Y a-t-il une installation de chauffage commune dans l’immeuble ?</h4> {buttons}
        <div className={animationClass(form)}>{form}</div>
      </div>)
    }
    return null

  }
  inputPartielNumber(e){
    let state = {...this.state};
    state.partielNumber = e.target.value;
    this.setState(state)
  }
  changePartielNumber(e){
    this.props.setPartielNumber(this.state.partielNumber)
  }

  togglePebPartiel(force){
    let service = { id: 11, name: 'Peb Partiel', regions: [0], biens : ['a','i'],info:'peb-partiel-info'}
    this.toggleService(service,force)
  }

  onBtnPartielClick(data){

    let state = {...this.state}
    state.needPartiel = data;
    if(!data){
      state.partielNumber = '';
      this.props.setPartielNumber(state.partielNumber);
      state = this.removePebPartiel(state)
    }
    this.props.setExpertises(state.expertises);

    this.setState(state)
  }

  removePebPartiel(state){
    let service = { id: 11, name: 'Peb Partiel', regions: [0], biens : ['a','i'],info:'peb-partiel-info'}
    let indexes = state.expertises.reduce(function(tab,current,i){
      if(current.id === service.id){
        tab.push(i);
      }
      return tab
    },[])

    indexes.forEach(function(current){
      state.expertises = [
        ...state.expertises.slice(0,current),
        ...state.expertises.slice(current+1,state.expertises.length)
      ];
    })

    return state;
  }

  //when citerne
  renderCiterneChoice(){
    let citernes = [9,6].map((num)=>this.renderCiterneChoiceItem(num))
    return (<div className="service-select-citerne-choice">
      <h4>{T('service-select.citerne.titre')}</h4>
      <div>{citernes}</div>
    </div>)
  }
  renderCiterneChoiceItem(num){
    let className= 'service-select-citerne-item'+ ( (num==this.state.citerneChoice)?' active':'' );
    return (<div className={className} key={num} onClick={this.onClickCiterneChoiceItem.bind(this,num)}>
      {T('service-select-citerne.item-'+num)}
    </div>)
  }
  onClickCiterneChoiceItem(num){
    let newState = {...this.state}
    newState.citerneChoice = num;

    this.setState(newState)
    this.props.setCiterneChoice(num)
  }
  renderPlanCount(){
    let counts = [1,2,3,4,5].map((num)=>this.renderPlanCountItem(num));
    let more = this.renderPlanCountItemMore()
    return (<div className="service-select-plan-count">
      <h4>{T('service-select.plan.count.titre')}</h4>
      <div>{counts}{more}</div>
    </div>)
  }

  renderPlanCountItem(num){
    let className= 'service-select-plan-count-item'+ ( (num==this.state.planCount)?' active':'' );

    return (<div className={className} key={num} onClick={this.onClickPlanCountItem.bind(this,num)} >{num}</div>)
  }
  renderPlanCountItemMore(){
    let defaultValue = (this.state.planCount && this.state.planCount>5)?this.state.planCount:6;
    defaultValue = parseInt(defaultValue);
    let className= 'service-select-plan-count-item'+ ( (5<this.state.planCount)?' active':'' );

    return (<div className={className} key={6} onClick={this.onClickPlanCountItem.bind(this,defaultValue)} onChange={this.onCustomPlanCountChange.bind(this)} ><input type="number" min={6} value={defaultValue} /></div>);
  }

  onClickPlanCountItem(num){
    let newState = {...this.state}
    newState.planCount = num;
    this.setState(newState)
    this.props.setPlanCount(num)
  }
  onCustomPlanCountChange(event){
      let num = event.target.value
      let newState = {...this.state}
      newState.planCount = num;
      this.setState(newState)
      this.props.setPlanCount(num)
  }

  //elec count only when an elec product is selected
  renderElecCount(){
    let counts = [1,2,3,4,5].map((num)=>this.renderElecCountItem(num));
    let more = this.renderElecCountItemMore()
    return (<div className="service-select-elec-count">
      <h4>{T('service-select.count.titre')}</h4>
      <div className="numElec">{counts}{more}</div>
      <div className="explication"><span className="aucunsupplement">{T('elec-schema-explication-1')}</span>{T('elec-schema-explication-2')}</div>
    </div>)
  }
  renderElecCountItemMore(){
    let defaultValue = (this.state.elecCount && this.state.elecCount>5)?this.state.elecCount:6;
    defaultValue = parseInt(defaultValue)
    let className= 'service-select-elec-count-item'+ ( (5<this.state.elecCount)?' active':'' );
    return (<div className={className} key={6} onClick={this.onClickElecCountItem.bind(this,defaultValue)} ><input type="number" min={6} defaultValue={defaultValue} onChange={this.onCustomElecCountChange.bind(this)}/></div>);
  }
  onCustomElecCountChange(event){
      let num = event.target.value

      let newState = {...this.state}

      newState.elecCount = num;

      this.setState(newState)
      this.props.setElecCount(num)
  }
  renderElecCountItem(num){
    let className= 'service-select-elec-count-item'+ ( (num==this.state.elecCount)?' active':'' );
    return (<div className={className} key={num} onClick={this.onClickElecCountItem.bind(this,num)}>{num}</div>)
  }

  onClickElecCountItem(num){
    let newState = {...this.state}

    newState.elecCount = num;

    this.setState(newState)
    this.props.setElecCount(num)
  }



  renderServices(){
    let services = this.props.services.map((service)=>this.renderServicesItem(service));

    return (<div className="service-select">
      {services}
    </div>)
  }

  renderServicesItem(service){
    let {region,bien} = this.props;
    region = parseInt(region);
    if((service.regions.indexOf(region)==-1)||(service.biens.indexOf(bien.buildingType)==-1)){
      return null
    }
    let selected = this.state.expertises.filter((exp)=>exp.id==service.id).length > 0;

    let className="service-select-item"+(selected?' active':'');

    let color = (selected)?'Blanc':'Bleu'
    let url = '/wp-content/themes/Certinergie/theme-js/commande/Icones/'+color+'/'
    let serviceImg = (service.id == ELEC)? url+'elec.png':
                    (service.id == PEB)? url+'peb.png':
                    (service.id == PLAN)?url+'Plan.png':
                    (service.id == PEB_Partiel)?url+'peb.png':
                    url+'citerne.png'



    let imgClass = 'service-select-image-'+service.id;

    let info = service.info?(<div className="information"><QH text={service.info} img={service.img} /></div>):null

    if(service.id == PLAN && this.state.expertises.filter(c=>c.id==PEB).length == 0){
      return null
    }
    return (<div className={className} key={service.id} onClick={this.toggleService.bind(this,service)}>
              <img src={serviceImg} className={imgClass} />
              <span className="sevice-select-name">{T('service-select.item-'+service.id)}</span>
              {info}
            </div>);

  }

  toggleService(service,force){
    let newState = {...this.state}
    let {bien} = this.props;


    let indexOfExp = newState.expertises.reduce((expInd, current,i)=>{
      if(current.id == service.id){
        expInd = i;
      }
      return expInd
    },-1);

    let notFound = indexOfExp == -1;

    if(notFound){
      newState.expertises.push(service);
      if(service.id == ELEC){
        newState.elecCount = 1;

        if(bien.buildingType == 'i'){
          newState.elecCount = bien.count
        }
        this.props.setElecCount(newState.elecCount);
      }
      if(service.id == PLAN){
        newState.planCount = 1;
        this.props.setPlanCount(newState.planCount);

      }
      if(service.id == CITERNE.E+''+CITERNE.A){
        //newState.citerneChoice = CITERNE.E;
        this.props.setCiterneChoice(newState.citerneChoice);
      }
    }

    if(!notFound ){
      newState.expertises = [
        ...newState.expertises.slice(0,indexOfExp),
        ...newState.expertises.slice(indexOfExp+1,newState.expertises.length)
      ];
      if(service.id == ELEC){
        newState.elecCount = 0;
        this.props.setElecCount(newState.elecCount);
      }

      if(service.id == PEB){
        newState.needPartiel = false;

        newState.partielNumber = '';
        this.props.setPartielNumber(newState.partielNumber);

        //newState.planCount = 0;
        //this.props.setPlanCount(newState.planCount);

        let indexOfPart = newState.expertises.reduce((expInd, current,i)=>{
          if(current.id == PEB_Partiel || current.id == PLAN){
            return i;
          }
          return expInd
        },-1)
        if(indexOfPart != -1){
          newState.expertises = [
            ...newState.expertises.slice(0,indexOfPart),
            ...newState.expertises.slice(indexOfPart+1,newState.expertises.length)
          ];
        }

      }
      if(service.id == PLAN){
        newState.planCount = 0;
        this.props.setPlanCount(newState.planCount);
      }
    }

    this.setState(newState);
    this.props.setExpertises(newState.expertises);

  }


};

export default ServiceSelect;

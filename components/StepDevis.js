import React from 'react';
import {Link} from 'react-router';
import { history } from '../store';

import T from '../traduction';
import Pages from '../pages';
const pageId = 1;

//import ZipCityPicker from './ZipCityPicker'

import RegionSelect from './RegionSelect';
import BienSelect from './BienSelect';
import ServiceSelect from './ServiceSelect';
import Tarif from './Tarif';


import LastMinute from './LastMinute';
import CodePromo from './CodePromo';


class StepOne extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      errorMessage : null
    }

  }

  componentWillMount(){
    if(window.Certinergie && window.Certinergie.removeDevisBar){
      window.Certinergie.removeDevisBar()
    }
    if(window.Certinergie && window.Certinergie.goTop){
      window.Certinergie.goTop()
    }

    let {maxStep,currentStep} = this.props.order;

    this.props.setCurrentStep(pageId);
    if(maxStep < pageId){
      let path = Pages
                  .filter((item)=>item.id==maxStep)
                  .map((item)=>item.url)[0]

     history.push(path);
    }
  }

  render() {

    let {
      region,
      bien,buildings,
      expertises,services,elecCount,planCount,citerneChoice,
      user, commissionAmount,partielNumber,
      expertisesPrices,promoCodeUsed
    } = this.props.order;

    let {
      setRegion,
      setBien,
      setExpertises, setElecCount, setPlanCount,setCiterneChoice, setPartielNumber,
      setProductsAndTotal,setCommission,addPromo,setEstateType
    } = this.props;

    let total = expertisesPrices.total;

    let error = this.getError();

    let nextPage = Pages.filter((page)=>page.id==pageId+1)[0]
    let next = (!error && total >0)?(<div id="stepdevisebtn" className="btn-next"><Link to={nextPage.url} onClick={this.checkIfCanGoNext.bind(this)}>{T('nextStep')}</Link></div>):null;


    let REGION = <RegionSelect region={region} setRegion={setRegion}/>
    let BIEN = (region!=-1)?(<BienSelect bien={bien} buildings={buildings} setBien={setBien} setExpertises={setExpertises}/>):null;
    let BienClass = BIEN?'closed-animation open-animation':'closed-animation'
    let SERVICE = (bien.buildingType && (bien.size!=-1 || bien.count != -1) && !(bien.buildingType=='m' && (bien.size == 4 || bien.size ==5)))?(<ServiceSelect region={region} bien={bien} expertises={expertises} services={services} elecCount={elecCount} partielNumber={partielNumber} setPartielNumber={setPartielNumber} citerneChoice={citerneChoice} setExpertises={setExpertises} setPlanCount={setPlanCount} setElecCount={setElecCount} setCiterneChoice={setCiterneChoice} planCount={planCount} />):null
    let TARIF = (expertises.length >0)?<Tarif promoCodeUsed={promoCodeUsed} addPromo={addPromo} bien={bien} region={region} commission={commissionAmount} setCommission={setCommission} expertises={expertises} planCount = {planCount} elecCount={elecCount} expertisesPrices={expertisesPrices} citerneChoice={citerneChoice} user={user} setProductsAndTotal={setProductsAndTotal} />:null;

    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }

    let LASTMINUTE = <LastMinute bien={bien} region={region} expertises={expertises} />

    return (<div>
      <h1>{T('calculez-meilleur-prix-a')}<br/>{T('calculez-meilleur-prix-b')}</h1>
      <div className="Steps Step-One" onClick={this.gotoScroll.bind(this)}>


          {REGION}

          <div className={animationClass(BIEN)}>
            {BIEN}
          </div>

          <div className={animationClass(SERVICE)}>
            {SERVICE}
          </div>

          <div className={animationClass(TARIF)}>
            {TARIF}
          </div>


          {next}
      </div>
  </div> )
    /*
    <div className="Card Card-shadow Tarif">
    </div>
    */
  }

  getTarget(target){

        let autorizedClass = ['region-select-item',
                              'bien-select-name',
                              'bien-select-type-item',
                              'service-select-item',
                              'bien-select-count-item',
                              'service-select-elec-count-item',
                              'bien-select-superficie-item'];

        let atLeastOneCommonClass = autorizedClass.filter((className)=>(target && target.classList)?target.classList.contains(className):false).length>0
        if(atLeastOneCommonClass){
          return target;
        }
        if(target && target.parentNode){
          return this.getTarget(target.parentNode);
        }
        return target;
  }

  gotoScroll(event){

    let target = this.getTarget(event.target);

    let y = target.offsetTop;

    if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
      if(y>0){

        window.Certinergie.goTop(y-250)
      }
    }

  }

  checkIfCanGoNext(event){
    ga("send", "event", "bouton", "clic", "étape 1 continuer");
    let errorMessage = this.getError();
    if(errorMessage){
      event.preventDefault();
      this.setState({errorMessage:errorMessage})
    }else{
      if(this.props.order.maxStep <pageId+1){
        this.props.setMaxStep(pageId+1)
      }
    }

  }

  getError(){
    let errorMessages = []

    let {region,bien,expertises} = this.props.order

    //CHECK REGION
    let hasRegion = region > -1;
    if(!hasRegion){ errorMessages.push('ERROR_REGION'); }

    //CHECK BUILDING
    let hasBuilding = bien.buildingType;
    let missBuildingSize = bien.buildingType == 'i' || (bien.buildingType == 'a' || bien.buildingType == 'm') && bien.size!=-1;
    let missBuildingCount = bien.buildingType != 'i' ||(bien.buildingType == 'i' && bien.count != -1)
    if(!hasBuilding){
      errorMessages.push('ERROR_BUILDING');
    }else{
      if(!missBuildingSize){ errorMessages.push('ERROR_BUILDING_SIZE'); }
      if(!missBuildingCount){ errorMessages.push('ERROR_BUILDING_COUNT'); }
    }

    //CHECK PRODUCT
    let hasExpertises = expertises.length>0;
    if(!hasExpertises){
      errorMessages.push('ERROR_NO_EXPERTISES')
    }

    return (errorMessages.length>0)?errorMessages[0]:null
    //this.setState({errorMessage: (errorMessages.length>0)?errorMessages[0]:null })

  }

  componentDidUpdate(){

    var Error = this.refs.Error;
    if(Error){
      let y = Error.offsetTop

      let scroll = $(window).scrollTop()
      let h = $(window).height();

      if(y < scroll || (scroll+h) < y){

        if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
          window.Certinergie.goTop(y-250)
        }
      }
    }
  }
};

export default StepOne;

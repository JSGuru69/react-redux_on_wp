import React from 'react';
import R from 'browser-request';

import T from '../traduction';


class MailInput extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      email : props.mail,
      isFree: null,
      isUsed : null,
      loading: false,
      isValid : true
    };
    this.value=props.mail||''
    //create a debounced onChange method
    this.debouncedOnChange = _.debounce(this.onChange,1000);
  }

  onChange(e){
    let mail = this.state.email;

    let isValidEmailFormat = this.isValid(mail);

    if(isValidEmailFormat && !this.props.noCheckOnline){
      let newState = {...this.state}

      newState.loading = true;
      this.setState(newState);


      let req = R('http://www.certinergie.be/wp-content/themes/Certinergie/api/mailCheck.php?mail='+mail,(error,res,body)=>{
        body = JSON.parse(body);

        let newState = {...this.state}
        newState.isFree = body.isFree;
        newState.isUsed = body.isUsed;
        newState.email = mail;
        newState.isValid = isValidEmailFormat;
        newState.loading = false;
        this.setState(newState);


        this.value = newState.isFree?mail:''

        this.props.onChange(this.state)
      });
    }else{
      let newState = {...this.state}
      newState.isValid = isValidEmailFormat;

      this.value = newState.isFree?mail:''
      this.props.onChange(this.state)
      this.setState(newState)
    }


  }

  isValid(mail){
    if(mail.length==0){return true;}
    let match = mail.match(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
    return !!match;
  }

  onInput(e){
    let newState = {...this.state}
    newState.email = e.target.value;
    this.setState(newState)
  }

  render() {

    let {loading,isUsed,isValid} = this.state;

    let IsValid = (!isValid)?<span>Entrer un email valide</span>:null

    let Loading = (isValid && loading)?<span>Chargement..</span>:null

    let Exist = (isValid && !loading && isUsed)?<span>Cet email existe déjà</span>:null


    let alreadyUsedClass = ((isUsed||!isValid)?' alreadyused':'')
    alreadyUsedClass = this.state.email != ''?alreadyUsedClass:''

    let classNameRequired = this.props.classNameRequired ? this.props.classNameRequired : ''

    let className = 'mail-input '+alreadyUsedClass+' '+classNameRequired

    return (
      <span className={className}>
        <input placeholder={this.props.placeholder} require={this.props.isRequire} onInput={this.onInput.bind(this)} onChange={this.debouncedOnChange.bind(this)} defaultValue={this.state.email} name="email" />
        {IsValid} {Loading} {Exist}
      </span>
    )
  }
}

export default MailInput;

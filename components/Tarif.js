import React from 'react';
import R from 'browser-request';

import EstateTypes from '../data/EstateTypes';

import T from '../traduction';

import Commission from './Commission';

import CodePromo from './CodePromo';

class MailInput extends React.Component {
  constructor(props){
    super(props);

    //let prices = localStorage.getItem('prices');
    //prices = (prices)?JSON.parse(prices):[];

    let prices = []

    if(prices.length == 0){
      R('http://www.certinergie.be/wp-content/themes/Certinergie/api/prices.php',(err,res,body)=>{

        if(err || typeof body == 'string' && body[0]!='['){

        }else{
          let jsonBody = JSON.parse(body);
          let newState = {...this.state}
          newState.prices = jsonBody;
          //localStorage.setItem('prices',body)
          this.setState(newState);
        }
      })
    }

    this.state = {
      products : [],
      prices : prices,

    }
  }

  render() {



    let isLoading = this.state.prices.length == 0;
    if(isLoading){
      return (<div></div>);
    }

    let {expertises,citerneChoice,elecCount,planCount,user,region,bien} = this.props;
    let customPrice = (user && user.Consumer && user.Consumer.ConsumerPrices)?user.Consumer.ConsumerPrices:[];


    let reducs = []
    let commissions = []

    let hasElec = false;
    let hasPeb = false;
    let estateType = this.getEstateType();

    //on enlève les doublons (problème avec peb)
    expertises = expertises.reduce((exps,curr)=>{
      let res = exps.filter((exp)=>exp.id == curr.id)
      if(res.length === 0){
        exps.push(curr);
      }
      return exps
    },[])

    let products = expertises.reduce((prev,curr)=>{


      let {prices} = this.state
      let {region} = this.props

      let id = (curr.id == 69)?citerneChoice : curr.id;
      let regionPrices = prices.filter(p=>p.Region == region);

      prices = prices.filter(p=>p.OrderType == 1 || !p.OrderType)

      //get price by product
      let elecPrices = prices.filter(p=>p.ProductType == 4);
      let elecSupPrices = prices.filter(p=>p.ProductType == 8);
      let pebPrices = prices.filter(p=>p.ProductType == 1);

      let pebPartielPrices = prices.filter(p=>p.ProductType == 11);
      let planPrices = prices.filter(p=>p.ProductType == 14);
      let citernePrices = (id==6||id==9)?prices.filter(p=>p.ProductType == id):[];
      //get price by region
      //fallback for region
      let elecPricesRegionFallback = elecPrices.filter(p=>!p.Region);
      let elecSupPricesRegionFallback = elecSupPrices.filter(p=>!p.Region);
      let pebPricesRegionFallback = pebPrices.filter(p=>!p.Region);

      let pebPartielPricesRegionFallback = pebPartielPrices.filter(p=>!p.Region);
      let planPricesRegionFallback = planPrices.filter(p=>!p.Region);
      let citernePricesRegionFallback = citernePrices.filter(p=>!p.Region);

      let elecPricesRegion = elecPrices.filter(p=>p.Region==region);
      elecPricesRegion = elecPricesRegion.length>0?elecPricesRegion:elecPricesRegionFallback;

      let elecSupPricesRegion = elecSupPrices.filter(p=>p.Region==region);
      elecSupPricesRegion = elecSupPricesRegion.length>0?elecSupPricesRegion:elecSupPricesRegionFallback;

      let pebPricesRegion = pebPrices.filter(p=>p.Region==region);
      pebPricesRegion = pebPricesRegion.length>0?pebPricesRegion:pebPricesRegionFallback;

      let pebPartielPricesRegion = pebPartielPrices.filter(p=>p.Region==region);
      pebPartielPricesRegion = pebPartielPricesRegion.length>0?pebPartielPricesRegion:pebPartielPricesRegionFallback;

      let planPricesRegion = planPrices.filter(p=>p.Region==region);
      planPricesRegion = planPricesRegion.length>0?planPricesRegion:planPricesRegionFallback;

      let citernePricesRegion = citernePrices.filter(p=>p.Region==region);
      citernePricesRegion = citernePricesRegion.length>0?citernePricesRegion:citernePricesRegionFallback;


      //with estate

      let elecPricesEstates = elecPricesRegion.filter(p=>p.EstateType == estateType)
      elecPricesEstates =(elecPricesEstates.length>0)?elecPricesEstates:elecPricesRegion.filter(p=>!p.EstateType)

      let elecSupPricesEstates = elecPricesRegion.filter(p=>p.EstateType == estateType)
      elecSupPricesEstates = (elecSupPricesEstates.length>0)?elecSupPricesEstates:elecSupPricesRegion.filter(p=>!p.EstateType)

      let pebPricesEstates = pebPricesRegion.filter(p=>p.EstateType == estateType)
      pebPricesEstates =(pebPricesEstates.length>0)?pebPricesEstates:pebPricesRegion.filter(p=>!p.EstateType)

      let pebPartielPricesEstates = pebPartielPricesRegion.filter(p=>p.EstateType == estateType)
      pebPartielPricesEstates =(pebPartielPricesEstates.length>0)?pebPartielPricesEstates:pebPartielPricesRegion.filter(p=>!p.EstateType)

      let planPricesEstates = planPricesRegion.filter(p=>p.EstateType == estateType)
      planPricesEstates =(planPricesEstates.length>0)?planPricesEstates:planPricesRegion.filter(p=>!p.EstateType)


      let citernePricesEstates = citernePricesRegion.filter(p=>p.EstateType == estateType)
      citernePricesEstates =(citernePricesEstates.length>0)?citernePricesEstates:citernePricesRegion.filter(p=>!p.EstateType)


      //getCustom
      let regionEstateCustomPrice = customPrice.filter((cp)=>cp.ConsumerRegion == region)
                                               .filter((cp)=>cp.EstateType == estateType);

      let customElecPrice = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == 4)||[null];
      let customElecSupPrice = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == 8)||[null];
      let customPebPrice  = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == 1)||[null];
      let customPebPartielPrice  = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == 11)||[null];
      let customPlanPrice  = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == 14)||[null];
      let customCiternePrice = regionEstateCustomPrice.filter((cp)=>cp.ProductTypeId == id)||[null];


      //trouvé en fonction de l'estate type
      let product = (id == 4)?this.addElec(elecCount,elecPricesEstates[0],customElecPrice[0],elecSupPricesEstates[0],customElecSupPrice[0]):
                    (id == 1)?this.addPeb(this.props.bien,pebPricesEstates[0],customPebPrice[0]):
                    (id == 6 || id == 9)?this.addCiterne(id,citernePricesRegion[0],customCiternePrice[0]):
                    (id == 11)?this.addPebPartiel(1,pebPartielPricesEstates[0],customPebPartielPrice[0]):
                    (id == 14)?this.addPlan(planCount,planPricesEstates[0],customPlanPrice[0])
                    :null

                    //toDo, add partiel and add plan


      if(id == 1){
        hasPeb = true;
      }
      if(id == 4){
        hasElec = true;
      }

      if(product){
        if(product instanceof Array){
          product.forEach(function(p){
            prev.push(p.product)
            reducs.push(p.reduction)
            commissions.push(p.commission)
          })
        }else{
          prev.push(product.product)
          reducs.push(product.reduction)
          commissions.push(product.commission)
        }
      }
      return prev
    },[]);

    let isCombi = hasElec && hasPeb;

    if(isCombi){

      let reducCombi = (function(r,t,u){
        if(r == 2 && u && u.Consumer  && u.Consumer.Status != 11 && u.Consumer.Status != 12 && u.Consumer.Status != 13){
          //flandre normal 20 de réduc en plus (50 + 30== 80)
          return 0
        }
        else if(r == 2 && u&& u.Consumer && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 11){
          return 40
        }
        else if(r == 2 && u&& u.Consumer && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 12){
          return 30
        }
        else if(r == 2 && u&& u.Consumer && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 13){
          return 20
        }
        else if( (r == 0 || r == 1) && u != null && u.Consumer != null && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 2 && t != 2)
        {
          //agence status gold bxl/wallon combi 50 + 25 + 25 = 100
          return 25
        }
        else if( (r == 0 || r == 1) && u != null && u.Consumer != null && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 3 && t != 2)
        {
          //agence status gold bxl/wallon combi 50 + 25 + 40 = 115
          return 40
        }
        else if( r !=2 && u != null && u.Consumer != null && u.Consumer.ConsumerType == 1689 && u.Consumer.Status == 4 && t != 2)
        {
          //agence status platium bxl/wallon combi -125=50 + 55 + 20
          return 20
        }
        else if(t == 2){
          return 0
        }
        //normal combi 0
        return 0
      })(region,estateType,user)
      reducs.push(reducCombi);
      //console.log("user",user);
      //console.log("prod",products);
      //console.log("es",customPrice);

      if(customPrice && customPrice.length > 0){
        
        let thispebpersoprice = customPrice.filter(c=>c.ProductTypeId == 1)
                                                   .filter(c=>c.EstateType == estateType)
                                                   .filter(c=>c.ConsumerRegion == region)|| [null];
        let thiselecpersoprice = customPrice.filter(c=>c.ProductTypeId == 4)
                                                   .filter(c=>c.EstateType == estateType)
                                                   .filter(c=>c.ConsumerRegion == region)|| [null];
        if((thispebpersoprice[0]) || (thiselecpersoprice[0])){
          reducs.pop();
        }
      }
      
    }

    let reducAmount = reducs.reduce((total,reduc)=>{
      reduc = (reduc>0)?-reduc:reduc
      return total+=reduc
    },0)
    let commissionAmount = commissions.reduce((total,reduc)=>{
      return total+=reduc
    },0)


    let com = parseInt( (this.props.commission > 0)?this.props.commission:(commissionAmount>0)?commissionAmount:0 );

    if(this.props.user && this.props.user.Consumer && this.props.user.Consumer.Commission==0 ){
       com = 0
    }
    products.push(this.addReduction(reducAmount+(com),this.props.promoCodeUsed));

    if(com>0){
      products.push(this.addCommission(com))
    }
    //console.log(products)
    //products is now an array of "product object"

    // ici je save la list des produts dans le store
    //pour la réutilisé dans resumePrice!

    let p = products.map(function(product,i){
      if(product.productId == -1 || product.price == 0){
        return null;
      }
      let classNameTarif = "Tarif-item Tarif-item-"+product.productId;
      return (<div key={i} className={classNameTarif}> <div className="Tarif-name">{product.name}</div><div className="Tarif-price">{product.price}€ </div></div>)
    })
    let total = products.reduce((total,product)=>total+=(product.productId<0)?0:product.price,0)

    this.sendToStoreIfNeeded(products,total,com)


    /*let TOTAL = (this.props.user.Consumer.Commission)?
    (<Commission user={this.props.user} min={0} max={reducAmount} value={com} total= {total} setCommission={this.props.setCommission}/>)
    :
    (<div className="Tarif-total"><div className="Tarif-name">Total TVAC</div> <div className="Tarif-price">{total}€</div></div>)*/

    let TOTAL =     (<Commission user={this.props.user} min={0} max={reducAmount} value={com} total= {total} setCommission={this.props.setCommission}/>)

    let CODEPROMO = <CodePromo bien={bien} region={region} expertises={expertises} promoCodeUsed={this.props.promoCodeUsed} user={this.props.user} addPromo={this.props.addPromo}/>


    return (
    <div className="Card Card-shadow Tarif-BG">
      <div className="Tarif-zone">
        <div className="Tarif-list">{p}</div>
        {TOTAL}
      </div>
      {CODEPROMO}
    </div>)
  }

  getEstateType(){
    let {buildingType,size,count} = this.props.bien;

    let types = EstateTypes;


    let filtered =types.filter(t=> t.buildingType==buildingType && (t.size == size||t.count == count))
    if(filtered.length>0){
      return filtered[0].typeId;
    }


    return 0;
  }

  sendToStoreIfNeeded(products,total,commission){
    if(!( this.props.expertisesPrices.products.length == products.length && total == this.props.expertisesPrices.total)){

      this.props.setProductsAndTotal(products,total,commission);

    }
  }

  addReduction(amount,promos){
    let promoTab = []
    for(var i in promos){
      promoTab.push(promos[i])
    }

    let promo =(promoTab.reduce((t,p)=>t+=p.reduc,0))

    return {
      productId : 5,
      name : T('product-reduction'),
      price : amount-promo
    }
  }
  addCommission(amount){
    return {
      productId : -1,
      name : T('product-commission'),
      price : amount
    }

  }

  addElec(count,price,custom,supPrice,supCustom){
    let com = custom && custom.ConsumerCommission?custom.ConsumerCommission:0;
    let supCom = supCustom && supCustom.ConsumerCommission?supCustom.ConsumerCommission:0;

    price = (price)?price:{Price : 0,Reduction:0}
    supPrice = (supPrice)?supPrice:{Price : 0,Reduction:0}


    if(this.props && this.props.region == 2 && this.props.user && this.props.user.Consumer && price.Price != 0){
      if(this.props.user.Consumer.Status == "13" ){
             price.Reduction = "60.00";
      }
      else if(this.props.user.Consumer.Status == "11" ){
             price.Reduction = "40.00";
      }
      else if(this.props.user.Consumer.Status == "12" ){
             price.Reduction = "50.00";
      }
    }
    else if(this.props && this.props.region !=2  && this.props.user && this.props.user.Consumer && this.props.user.Consumer.Status == 4 && price.Price != 0){
       let diff = price.Price - 95;
       price.Reduction = diff;
    }

    if(custom){
      //price.Price = !!custom.ConsumerAmount? custom.ConsumerAmount :price.Price;
      //price.Reduction = (com>0)?-com:0;
      let personprice = 0;
      if(custom.ConsumerAmount && custom.ConsumerAmount != ""){
        personprice = custom.ConsumerAmount;
        price.Reduction = price.Price - personprice;
      }
    }

    if(supCustom){
      supPrice.Price = !!supCustom.ConsumerAmount? supCustom.ConsumerAmount :supPrice.Price;
      price.Reduction = (supCom>0)?-supCom:0;
    }

    let product = {
      productId : 4,
      name : T('product-4'),
      price : (parseInt(price.Price)*1)
    }

    let res = []
    res.push({
      product : product,
      reduction : -(parseFloat(price.Reduction||0)*1),
      commission :  (parseInt(com)*1)
    })


    let cptSup;
    if(count>1){
       cptSup = {
        productId : 8,
        name : T('product-8'),
        price : (parseInt(supPrice.Price)*(count-1))
      }

      res.push({
        product : cptSup,
        reduction : -(parseInt(supPrice.Reduction)*(count-1)),
        commission :  parseInt(supCom)*(count-1)
      })
    }


    return res
  }
  addPlan(nb,price,custom){

    nb = nb || 1
    let com = custom && custom.ConsumerCommission?custom.ConsumerCommission:0;
    price = (price)?price:{Price : 0,Reduction:0}

    if(custom){
      price.Price = !!custom.ConsumerAmount? custom.ConsumerAmount :price.Price;
      price.Reduction = (com>0)?-com:0;
    }


    let p = parseInt(price.Price)*parseInt(nb)

    let product = {
      productId : 14,
      name : T('product-14'),
      price : p
    }

    return {
      product : product,
      reduction : -(parseFloat(price.Reduction||0)*parseInt(nb)),
      commission :  (parseInt(com)*parseInt(nb))
    }
  }
  addPebPartiel(nb,price,custom){
    nb = nb||1
    let com = custom && custom.ConsumerCommission?custom.ConsumerCommission:0;

    price = (price)?price:{Price : 0,Reduction:0}

    if(custom){
      price.Price = !!custom.ConsumerAmount? custom.ConsumerAmount :price.Price;
      price.Reduction = (com>0)?-com:0;
    }

    let product = {
      productId : 11,
      name : T('product-11'),
      price : parseInt(price.Price)*nb
    }


    return {
      product : product,
      reduction : -(parseFloat(price.Reduction||0)*nb),
      commission :  (parseInt(com)*nb)
    }
  }
  addPeb(bien,price,custom){


    let com = custom && custom.ConsumerCommission?custom.ConsumerCommission:0;

    price = (price)?price:{Price : 0,Reduction:0}

    if(this.props && this.props.region == 2 && this.props.user && this.props.user.Consumer && price.Price != 0){
      if(this.props.user.Consumer.Status == "13" && price.Reduction == 50.00){
            price.Reduction = parseFloat(price.Reduction) + parseFloat(15);
      }
      else if(this.props.user.Consumer.Status == "11"&& price.Reduction == 50.00 ){
             price.Reduction = parseFloat(price.Reduction) + parseFloat(5);
      }
      else if(this.props.user.Consumer.Status == "12" && price.Reduction == 50.00){
             price.Reduction = parseFloat(price.Reduction) + parseFloat(10);
      }
    }

    if(custom && price.Price != 0 ){
      //price.Price = !!custom.ConsumerAmount? custom.ConsumerAmount :price.Price;

      //price.Reduction = (com>0)?-com:0;
      let personprice = 0;
      if(custom.ConsumerAmount && custom.ConsumerAmount != ""){
        personprice = custom.ConsumerAmount;
        price.Reduction = price.Price - personprice;
      }
      
      

    }

    let calculedPrice = parseInt(price.Price)
    let calculedReduction =  parseFloat(price.Reduction||0)

    if(bien.buildingType == "i"){
      let others = (bien.count-1)*parseInt(price.Price - 25)
      calculedPrice+=others

      let othersReduc = (bien.count-1)*parseFloat((price.Reduction)||0)
      calculedReduction+=othersReduc
    }


    let product = {
      productId : 1,
      name : T('product-1'),
      price : calculedPrice
    }
    return {
      product : product,
      reduction : -calculedReduction,
      commission :  parseInt(com)
    }
  }
  addCiterne(id,price,custom){
    let com = custom && custom.ConsumerCommission?custom.ConsumerCommission:0;

    price = (price)?price:{Price : 0,Reduction:0}
    if(custom){
      price.Price = !!custom.ConsumerAmount? custom.ConsumerAmount :price.Price;
      price.Reduction = (com>0)?-com:0;
    }
    let product = {
      productId : id,
      name : T('product-'+id),
      price : parseInt(price.Price)
    }
    return {
      product : product,
      reduction :  -(parseFloat(price.Reduction||0)),
      commission :  parseInt(com)
    }
  }

}

export default MailInput;

import React from 'react';
import ReactDOM from 'react-dom';

import DateVisitePicker from './DateVisitePicker';
import DateButoir from './DateButoir';
import moment from 'moment';
import T from '../traduction';
import QH from './QuestionHelp';
import CI from './CustomInput';

class InfoContact extends React.Component{
  constructor(props){
    super(props);
    this.state = {...props.contact};
    this.state.isCleAgence = false;

    //create a debounced onChange method
    this.debouncedOnChange = _.debounce(this.onChange,1000);
  }

  onKeyUp(e){
    e.preventDefault();
    //let {value,name} = e.target;
    let name = e.target.getAttribute("name");
    let value = e.target.value;
    let state = {...this.state};
    state[name] = value;
    this.setState(state);
    
  }

  onChange(e){
    //call the same logic as in onKeyUp method
    //this.onKeyUp.bind(this,e);
    let state = this.state;
    //let {firstname,lastname,phone,dates} = this.state;
    let name = e.target.getAttribute("name");
    let value = e.target.value;
    if(name == "firstname"){
      state["firstname"] = value;
    }
    else if(name == "lastname"){
      state["lastname"] = value;
    }
    else if(name == "phone" ){
      state["phone"] = value;
    }
    //console.log('targeronchange',e.target)
    //let state = {...this.state};
    //state[name] = value;
    this.setState(state);
    //let {firstname,lastname,phone,dates} = this.state;
    this.props.setContact(state["firstname"] ,state["lastname"] ,state["phone"],state["dates"]||[]);
    //console.log("onchange",this.state)
  }

  onDatesChange(dates){
    let {firstname,lastname,phone} = this.state;
    this.props.setContact(firstname,lastname,phone,dates||[]);
  }

  isEmptyFieldPutRedClass(name){
    return this.props.emptyFields && this.props.emptyFields[name]?'empty-input-field':''
  }
  render() {
    let {dates,dateButoir} = this.props.contact;
    let {firstname,lastname,phone}=this.state;

    let cleAgence = null;

    let {user} = this.props
    if(user && user.Consumer && user.Consumer.ConsumerType){
      if(user.Consumer.ConsumerType == 8122){
        cleAgence = {
          text : {
            'fr' : 'Clé en l\'étude',
            'nl' : 'Sleutel bij notaris',
            'en' : 'Key notaris'
          }
        }
      }else if(user.Consumer.ConsumerType == 1689){
        cleAgence = {
          text : {
            'fr' : 'Clé à l\'agence',
            'nl' : 'Sleutel bij agentschap',
            'en' : 'Key agency'
          }
        }
      }
    }

    let cleAgenceDiv = null;

    if(cleAgence){
      let url= window.location.href;
      if(url.indexOf("/nl/") !== -1){
          cleAgenceDiv = <span onClick={this.cleAgence.bind(this,cleAgence.text['nl'])} className="copy-btn cleAgence-btn">
                        {cleAgence.text['nl']}&nbsp;<QH text="cle-agence-info" />
                      </span>
      }
      else{
              cleAgenceDiv = <span onClick={this.cleAgence.bind(this,cleAgence.text['fr'])} className="copy-btn cleAgence-btn">
                        {cleAgence.text['fr']}&nbsp;<QH text="cle-agence-info" />
                      </span>
      }

    }

    let messageCleAgence = (this.state.isCleAgence)?
                            (<div className="cle-agence-phrase">{T('cle-agence-info-text')}
                            </div>)
                            :null

    //value is not working on ie so this is the workaround -> get the dom elem and bind the value

    return (
      <div className="Card Card-shadow Info-Contact">
        <h3>{T('info-contact.titre')}<QH text="why-contact-title" /></h3>

        {this.props.error}
        <form>
          <div className="copy-group">
            {cleAgenceDiv}
            <span className="copy-btn copy" onClick={this.copyContact.bind(this)}>{T('contact.copie')}</span>
          </div>
          <CI label='info-contact.prenom' name="firstname" ref="required-contact-prenom" classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-contact-prenom')}
              value={firstname} onInput={this.onKeyUp.bind(this)} onChange={this.onChange.bind(this)}/>

          <CI label='info-contact.nom'  name="lastname" ref="required-contact-nom" classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-contact-nom')}
              value={lastname} onInput={this.onKeyUp.bind(this)} onChange={this.onChange.bind(this)}/>

          <CI label='info-contact.phone' qh="why-contact-phone" name="phone"
              classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-contact-phone')} ref="required-contact-phone"
              value={phone} onInput={this.onKeyUp.bind(this)} onChange={this.onChange.bind(this)}/>
          {messageCleAgence}
          <DateVisitePicker onChange={this.onDatesChange.bind(this)} dates={dates}/>

          <DateButoir onChange={this.onDateButoirChange.bind(this)} date={dateButoir}/>

        </form>
      </div>
    )
  }

  onDateButoirChange(e){
    this.props.setDateButoir(e);
    //console.log(e)
  }

  cleAgence(text){
    let newState = {...this.state}
    newState.firstname = '/';
    newState.lastname = '/';
    newState.phone = text;
    newState.isCleAgence = true;
    let {firstname,lastname,phone,dates} = newState;

    this.props.setContact(firstname,lastname,phone,dates||[]);

    this.forceUpdateCustomInput('required-contact-prenom',firstname)
    this.forceUpdateCustomInput('required-contact-nom',lastname)
    this.forceUpdateCustomInput('required-contact-phone',phone)

    this.setState(newState);
  }

  copyContact(){

    let newState = {...this.state}

    let owner = this.props.owner;
    let user = this.props.user;

    if(owner.firstname != '' || owner.lastname != ''){
      newState.firstname = (owner.firstname != null && owner.firstname != '')?owner.firstname:'/'
      newState.lastname = (owner.lastname != null && owner.lastname != '' )? owner.lastname:'/'
      newState.phone = owner.phone
    }else{
      newState.firstname = (user.FirstName != null && user.FirstName != ''  )? user.FirstName:'/'
      newState.lastname = (user.Name != null && user.Name != '')? user.Name:'/'
      newState.phone = user.PhoneNumber
    }
    newState.isCleAgence = false;
    let {firstname,lastname,phone,dates} = newState;
    this.props.setContact(firstname,lastname,phone,dates||[]);


    this.forceUpdateCustomInput('required-contact-prenom',firstname)
    this.forceUpdateCustomInput('required-contact-nom',lastname)
    this.forceUpdateCustomInput('required-contact-phone',phone)

    this.setState(newState);
  }

  forceUpdateCustomInput(ref,value){
    let node = ReactDOM.findDOMNode(this.refs[ref]).getElementsByTagName('input');
    if(node){node[0].value=value;}
  }

};

export default InfoContact;

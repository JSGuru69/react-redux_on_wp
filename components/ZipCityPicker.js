import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';

import Cities from '../city';


class ZipCityPicker extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value : ( (props.zip||'')+' '+(props.city||'')==' ' )?
              '':
              ((props.zip||'')+' '+(props.city||'')),
      cities : [],
      hideList : true,
      i: 0,
      selected : {
        zip: null,
        city : null
      },
      zip : props.zip,
      city : props.city
    }
    let {zip,city} = this.state;
    zip=zip||'';
    city=city||'';
    this.value = zip+' '+city;
  }

  search(search){
    let cities = []
    //si ça commence par 4 chiffre
    if(!isNaN(parseInt(search.slice(0,4)))){
      //je prend toute les villes de ce code postal
      let num = parseInt(search.slice(0,4))
      cities = (Cities[num] || []).map((c)=>num+' '+c);

      //si c'est plus long que 5char (c'est qu'il y a une ville en texte)
      let ville = search.slice(5,search.length)
      //si c'est le cas
      if(ville){
        //je ne prend que ceux qui contienne cette ville
        cities = cities.reduce((tab,c)=>{
          if((c.toLowerCase()).indexOf(ville.toLowerCase())!=-1){
            tab.push(c)
          }
          return tab
        },[]);
      }
    }else if(search.length>1){
      //sinon, je parcours tout le tableau et je prend que ce qui contient la recherche
      _.forIn(Cities,(value,key)=>{
          value.reduce((tab,c)=>{
            if((c.toLowerCase()).indexOf(search.toLowerCase())!=-1){
              tab.push(key+' '+c)
            }
            return tab
          },cities);
      })
    }

    //je trie du plus court au plus long
    cities.sort((a,b)=>{
      const getVille = function(ville){
        let splitted = ville.split(' ')
        splitted.shift();
        return splitted.join()
      }
      return getVille(a).length - getVille(b).length;
    })

    this.selectCity(0,(cities.length>0)?cities:null,search);
  }

  selectCity(i,cities,search){

    let newState = {...this.state}
    newState.hideList = !!!search
    cities = (cities||this.state.cities||[]);

    let selected = cities[i];
    if(selected){
      let splitted = selected.split(' ');

      let zip = splitted[0];
      let city= splitted.slice(1 ,splitted.length).join(' ');

      newState.selected = {zip,city}
      newState.cities = cities;
    }
    newState.i = i;

    let {zip,city} = newState.selected;
    zip=zip||'';
    city=city||'';
    this.value = zip+' '+city;
    newState.zip = zip;
    newState.city= city;
    newState.value = search==''?'' : search?search:zip+' '+city;
    this._onChange(newState);
  }

  _onChange(state){
    this.setState(state);
    let {zip,city} = state.selected;
    zip=zip||'';
    city=city||'';
    this.value = zip+' '+city;
    if(this.props.onChange){
      this.props.onChange(state.selected);
    }

  }

  displayList(){
    let {selected,cities,hideList} = this.state;
    let {zip,city} = selected;

    let list = _.map(cities,(city,i)=>{
      return <div className="ZipCity-Row" key={i} value={i} onClick={this.selectCity.bind(this,i,null,null)}>{city}</div>
    })

    let listDiv = (hideList)?null:(<div className="ZipCity-Rows" ref="selectInput">{list}</div>)

    return listDiv
  }

  onInput(e){
    let val = e.target.value
    if(this.props.noCompletion){
      let splitted = val.split(' ')
      let zip = splitted[0]
      let city = splitted.slice(1).join(' ')
      let s = {...this.state}
      s.value = val
      s.selected = {zip,city}
      this._onChange(s)
    }else{
      this.search(val)
    }
  }

  render() {
    let zc = this.state.value
    let node = ReactDOM.findDOMNode(this.refs.zipInput)
    if(node){node.value=zc;}

    if(zc == ' '){
      zc = '';
    }
      let classNamePicker = "ZipCityPicker"+((this.props.classNameRequired)?(' '+this.props.classNameRequired):'');
    return (

      <div className={classNamePicker}>
        <div>
          <input placeholder={this.props.placeholder} type="text" ref="zipInput"
                 placeholderText="Code postal" defaultValue={zc} onInput={this.onInput.bind(this)}  />
        </div>
        {this.displayList()}
      </div>
    );
  }

  changeSelect(e){
    let val = this.refs.selectInput.value;
    val = parseInt(val);
    this.selectCity(val);

    this.forceUpdate()
  }
}

module.exports = ZipCityPicker

import React from 'react';
import QH from './QuestionHelp';
import ZipCityPicker from './ZipCityPicker';
import MailInput from './MailInput';
import T from '../traduction';


class CustomInput extends React.Component {
  constructor(props){
    super(props);
    this.value = props.value;
  }

  getBoundingClientRect(){
    if(this.refs.elem && this.refs.elem.getBoundingClientRect){
      return this.refs.elem.getBoundingClientRect();
    }
  }

  render() {
    let value = this.props.value||'';

    if(this.props.isAddress){
      value = this.props.zip+' '+this.props.city
    }

    if(value == ' '){
      value = '';
    }

    this.value = value;

    let h = this.props.qh?(<QH text={this.props.qh} />):null;
    let v = null;

    let min = (this.props.minChar?parseInt(this.props.minChar):3);

    let cnRequired = this.props.classNameRequired

    if(value.length>=min){
      v = <div className="custom-input-v"><i className="fa fa-check" ></i></div>
      cnRequired = '';
    }
    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }

    let onChange = this.props.onChange;
    let onInput = this.props.onInput;
    let disabledClass = null;
    if(this.props.disabled){
      onChange = function(){}
      onInput = function(){}
      disabledClass = 'disabled'
    }


    let input = this.props.isMail?
          (
            <MailInput placeholder={T(this.props.label)} noCheckOnline={this.props.noCheckOnline}
                      mail={this.props.value} classNameRequired={(cnRequired||'')+' '+disabledClass}
                      ref={this.props.ref} isRequire={this.props.isRequire}
                      onChange={onChange} />
          ):
          this.props.isAddress?
          (
            <ZipCityPicker placeholder={T(this.props.label)}
                           noCompletion={this.props.noCompletion}
                           zip={this.props.zip}
                           city={this.props.city}
                           classNameRequired={(cnRequired||'')+' '+disabledClass}
                           onChange={onChange} />

          ):
          this.props.isTextArea?
          (
            <textarea ref={this.props.ref} onChange={onChange} className={(cnRequired||'')+' '+(this.props.className||'')+' '+(disabledClass||'')}>{this.props.value}</textarea>
          ):
          //value={this.props.value}
          (<input placeholder={T(this.props.label)}
                  ref="customInput"
                  onInput={onInput}
                  onChange={onChange}
                  className={(cnRequired||'')+' '+(this.props.className||'')+' '+disabledClass}
                  ref={this.props.ref}
                  defaultValue={this.props.value||this.props.defaultValue}
                  name={this.props.name} />)

    let classNameSpan = 'custom-input'+(this.props.isMail?' email-input-field':'')
                                      +(this.props.isStreet?' input-street':'')
                                      +(this.props.isNumbox?' input-numbox':'')
                                      +(this.props.isAddress?' input-address':'')
                                      +(this.props.isTextArea?' input-textarea':'')

    let errorText = (cnRequired && cnRequired != '')?
                      (this.props.erroText)?this.props.errorText:
                      (cnRequired == 'empty-input-field')?T('champ-requis'):null
                    :' '


    let errorDiv = <div className="error-text-custom-input">{errorText}&nbsp;</div>


    return (
      <span className={classNameSpan} ref="elem">
        {T(this.props.label)}
        {h}
        {input}
        {errorDiv}
        <div className={animationClass(v)}>{v}</div>
      </span>
    )
  }
}

export default CustomInput;

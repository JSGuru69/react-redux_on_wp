import React from 'react';
import _ from 'lodash';

import ZipCityPicker from './ZipCityPicker';

import T from '../traduction';

import QH from './QuestionHelp';
import CI from './CustomInput';

class InfoDocument extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      document : {...props.document},
      /*otherNotaire: props.otherNotaireDocument || {
        is : 'otherNotaire',
        type: null,
        name: '',
        phone : '',
        email : '',
        address : {
          street : '',
          numbox : '',
          zip : '',
          city : ''
        }
      },*/
      other :  props.otherDocument || (function(props){
        let {owner,user,address,bien} = props;


        let name = owner.firstname+' '+owner.lastname
        name = name!=' '?name:(user.FirstName+' '+user.Name)

        return {
          is : 'other',
          type: null,
          name: name||'',
          phone : owner.phone?owner.phone:user.PhoneNumber?user.PhoneNumber:'',
          email : owner.email?owner.email:user.Email?user.Email:'',
          company : owner.company?owner.company:user.company?user.compay:'',
          address : {
            street : '',
            numbox : '',
            zip : '',
            city : ''
          }
        }
      })(props)
    };

    if(this.state.document.is === "mail"){
      let {user,address} = this.props;
      let userAddres = (user.newaddress && user.newaddress.street !=='')?user.newaddress:
                       (user.Addresses && user.Addresses.length>0 && user.Addresses[0])?user.Addresses[0]:
                       address;

      if(userAddres.Street && userAddres.PostalCode){
        userAddres.street = userAddres.Street;
        userAddres.numbox = userAddres.Number;
        userAddres.zip = userAddres.PostalCode;
        userAddres.city = userAddres.City;
      }
      let tmpfirstname = (user.FirstName)?(user.FirstName +' '):'';
      let tmpname = (user.Name)?user.Name:'';
      let userName = {
        is : null,
        type: user.Consumer.ConsumerType,
        name: tmpfirstname+tmpname,
        phone : user.PhoneNumber,
        email : user.Email,
        address : userAddres,
        company : user.company
      }
      let onlyMail = {...userName}
      onlyMail.is = "mail";

      let {name,company,phone,email,is}=onlyMail;
      let {street,numbox,zip,city,tva,country} = onlyMail.address;


      this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);
    }



    this.DEBOUNCEDonOtherAddressChange = _.debounce(this.onOtherAddressChange,1000);
    this.DEBOUNCEDonOtherNotaireAddressChange = _.debounce(this.onOtherNotaireAddressChange,1000);

  }

  render(){

    let {owner,user,address,bien} = this.props;
    //console.log('prop',this.props)
    let onlyMail = null;
    let listOfName = [];
    let proprioName = null;
    let proprioAtProprio = null;
    let agenceName = null;
    let proprioCoAgence = null;
    let notaireName = null;
    let proprioCoNotaire = null;
    let otherNotaire = null;
    let installateur = null;
    let other = null;



    //proprio
    if(owner && (owner.firstname!='' || owner.lastname!='')){
      onlyMail = {
        is : 'mail',
        type: '1688',
        name: owner.firstname+' '+owner.lastname,
        phone : owner.phone,
        email : owner.email,
        address : address,
        company : owner.company
      }
      proprioName = {
        is : 'proprio-bien',
        type: '1688',
        name: owner.firstname+' '+owner.lastname,
        phone : owner.phone,
        email : owner.email,
        address : address,
        company : owner.company
      }
    }

    if(user && user.Consumer ){
      let userAddres = (user.newaddress && user.newaddress.street !=='')?user.newaddress:
                       (user.Addresses && user.Addresses.length>0 && user.Addresses[0])?user.Addresses[0]:
                       address;

      if(userAddres.Street && userAddres.PostalCode){
        userAddres.street = userAddres.Street;
        userAddres.numbox = userAddres.Number;
        userAddres.zip = userAddres.PostalCode;
        userAddres.city = userAddres.City;
      }
      let tmpfirstname = (user.FirstName)?(user.FirstName +' '):'';
      let tmpname = (user.Name)?user.Name:'';
      let userName = {
        is : null,
        type: user.Consumer.ConsumerType,
        name: tmpfirstname+tmpname,
        phone : user.PhoneNumber,
        email : user.Email,
        address : userAddres,
        company : user.company
      }
      onlyMail = {...userName}
      onlyMail.is = "mail";

      if(user.Consumer.ConsumerType == 1688){

        proprioName = {...userName};
        proprioName.is = 'proprio-bien'
        proprioName.address = address;
        /*proprioAtProprio = {...userName}
        proprioAtProprio.is = 'proprio'*/

      }
      if(user.Consumer.ConsumerType == 1689){
        agenceName = {...userName};
        agenceName.is = "agence";
        proprioCoAgence = {...proprioName};
        //console.log('proprio,agence',proprioCoAgence,agenceName)
        proprioCoAgence.is = 'proprioCoAgence';

        proprioCoAgence.name = proprioCoAgence.name+' C/o '+agenceName.name;
        proprioCoAgence.address = {...agenceName.address}

        //otherNotaire = this.state.otherNotaire

      }
      if(user.Consumer.ConsumerType == 8122){

        notaireName = {...userName};
        notaireName.is = "notaire";

        proprioCoNotaire = {...proprioName};

        //console.log('proprio,notaire',proprioCoNotaire,notaireName)
        proprioCoNotaire.is = 'proprioCoNotaire';
        proprioCoNotaire.name = proprioCoNotaire.name+' C/o '+notaireName.name;
        proprioCoNotaire.address = {...notaireName.address}
      }

      if(user.Consumer.ConsumerType == 1691){

        installateur = {...userName};
        installateur.is = 'installateur'
        //notaireName = {...userName};
        //notaireName.is = "notaire";

        //proprioCoNotaire = {...proprioName};
        //proprioCoNotaire.is = 'proprioCoNotaire';
        //proprioCoNotaire.name = proprioCoNotaire.name+' C/° '+notaireName.name;
        //proprioCoNotaire.address = {...notaireName.address}
      }

    }

    other = this.state.other


    let listOfNameAddress = [null,proprioAtProprio,proprioName,proprioCoAgence,proprioCoNotaire,agenceName,notaireName,installateur,otherNotaire,other];

    let listOfNameAddressDiv = listOfNameAddress.map((na)=>this.getNADiv(na))



    let DispoEnLigne = (<div>
      <h4>Disponnible en ligne</h4>
      <div className="Info-Document-Address active">
        <h5>Par email ainsi que dans votre espace client</h5>
        <span>Je ne souhaite pas recevoir de document par courrier et je bénéficie ainsi d'une réduc de 2,5€</span>
      </div>
    </div>)



    let AddressList = (<div>
      {/*<h4>A envoyer par courrier</h4>*/null}
      {listOfNameAddressDiv}
    </div>)

    let Error = (this.props.error && this.props.error !== "other-document-some-empty")?(<div className="Error" ref="Error">{T('Error-'+this.props.error)}</div>):null


    return (<div className="Card Card-shadow Info-Document">
      <h3>{T('info-document-titre')}<QH text="info-document-question" /></h3>
      {/*DispoEnLigne*/null}
      {Error}
      {AddressList}
    </div>)

  }

  getNADiv(na){
    if(!na){
      return null;
    }
    let active = (this.props.document.is == na.is ||na.is=='mail')
    let classNameAddress = "Info-Document-Address"+(active?' active':'')

    let reducMailOnly = this.props.document.is == 'mail' && (this.props.user && this.props.user.Consumer.ConsumerType == "1688")?(<p className="Info-Document-Mail-content">{T('Info-Document-Address-mail-subtext-b')} <span className="Info-Document-Reduc">2,50€</span></p>):null
    let subtext = na.is != 'mail'?(<div><p>{na.name}<br/>{na.address.street} {na.address.numbox}<br/>{na.address.zip} {na.address.city}<br/>{na.address.tva}</p></div>):
                                   <div><p className="Info-Document-Mail-content">{T('Info-Document-Address-mail-subtext')}</p>{reducMailOnly}</div>


    let icon = (active)?(<div className="selected-icon active"><i className="fa fa-check" ></i></div>):(<div className="selected-icon"><i className="fa fa-close" ></i></div>)

    if(na.is == 'other'){
      return (this.getOtherForm.bind(this))(na)
    }

    //if(na.is == "otherNotaire"){
    //  return (this.getOtherNotaireForm.bind(this))(na)
    //}


    let title = 'Info-Document-Address-'+na.is;

    let {user} = this.props;

    if(na.is == 'proprio-bien' && user.Consumer.ConsumerType == 1688){
      title = 'Info-Document-Address-proprio-bien-himself'
    }


    let owner = (this.props.owner && this.props.owner.email && user.Consumer.ConsumerType !== 1688)?<span className="second-mail">({this.props.owner.email})</span>:null;

    let mail = (na.is=='mail')?<span className="Info-Document-Mail-titre">{this.props.user.Email} {owner}</span>:null
    return (<div className={classNameAddress} onClick={this.onClickAddress.bind(this,na)}>
      <h5>{T(title)}</h5>{mail}
      {subtext}
      {icon}
    </div>)
  }

  getOtherForm(na){
      if(!na){
        return null;
      }

      let {other} = this.state;
      let active = (this.props.document.is == na.is)

      let icon = (active)?(<div className="selected-icon active"><i className="fa fa-check" ></i></div>):(<div className="selected-icon"><i className="fa fa-close" ></i></div>)
      let animationClass = function(something){
        return something?'closed-animation open-animation':'closed-animation'
      }

      let classNameAddress = "Info-Document-Address"+((active)?' active':'')


      //let Error = (this.props.error && this.props.error == "other-document-some-empty")?(<div className="Error" ref="Error">{T('Error-'+this.props.error)}</div>):null

      let Address =(this.props.document.is == na.is)?(<form>

        <CI label="other-address.name" value={other.name} name="name"
            onInput={(this.onOtherAddressKeyup()).bind(this)} onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document-name')} ref="required-document-name"/>

        <CI label="other-address.company" value={other.company} name="company"
            onInput={(this.onOtherAddressKeyup()).bind(this)} onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)} />

        <CI label="other-address.tva" value={other.address.tva} name="tva"
            onInput={(this.onOtherAddressKeyup('address')).bind(this)}
            onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)} />


        <CI isStreet={true} label="other-address.street" value={other.address.street} name="street"
            onInput={(this.onOtherAddressKeyup('address')).bind(this)}  onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.street')} ref="required-document.street"/>

        <CI isNumbox={true} label="other-address.numbox" value={other.address.numbox} name="numbox"
            onInput={(this.onOtherAddressKeyup('address')).bind(this)}  minChar={1}  onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.numbox')} ref="required-document.numbox"/>

        <CI isAddress={true} label="other-address.zipcity" zip={other.address.zip} city={other.address.city}
            onChange={this.onOtherAddressChangeZipCity.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.zipcity')} ref="required-document.zipcity"/>

        <CI label="other-address.country" value={other.address.country} name="country"
            onInput={(this.onOtherAddressKeyup('address')).bind(this)}
            onChange={this.DEBOUNCEDonOtherAddressChange.bind(this)} />


      </form>):null

      return (<div className={classNameAddress} onClick={this.onClickAddress.bind(this,na)}>
        <h5>{T('Info-Document-Address-'+na.is)}</h5>
        <div className={animationClass(Address)}>
          <div className="other-form-address">

          {Address}

          </div>
        </div>

        {icon}
      </div>)
  }




/*
  getOtherNotaireForm(na){
      if(!na){
        return null;
      }

      let {otherNotaire} = this.state;
      let other = otherNotaire
      let active = (this.props.document.is == na.is)

      let icon = (active)?(<div className="selected-icon active"><i className="fa fa-check" ></i></div>):(<div className="selected-icon"><i className="fa fa-close" ></i></div>)
      let animationClass = function(something){
        return something?'closed-animation open-animation':'closed-animation'
      }

      let classNameAddress = "Info-Document-Address"+((active)?' active':'')

      let Address =(this.props.document.is == na.is)?(<form>

        <CI label="otherNotaire-address.name" qh="why-document-notaire-name" value={otherNotaire.name} name="name"
            onInput={(this.onOtherNotaireAddressKeyup()).bind(this)} onChange={this.DEBOUNCEDonOtherNotaireAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document-name')} ref="required-document-name"/>

        <CI label="other-address.phone" qh="why-document-phone" value={otherNotaire.phone} name="phone"
            onInput={(this.onOtherNotaireAddressKeyup()).bind(this)} onChange={this.DEBOUNCEDonOtherNotaireAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document-phone')} ref="required-document-phone"/>

        <CI label="other-address.mail" qh="why-document-mail" value={otherNotaire.email} name="email"
            onInput={(this.onOtherNotaireAddressKeyup()).bind(this)} onChange={this.DEBOUNCEDonOtherNotaireAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document-mail')} ref="required-document-mail"/>

        <CI isStreet={true} label="other-address.street" qh="why-address.street" value={otherNotaire.address.street} name="street"
            onInput={(this.onOtherNotaireAddressKeyup('address')).bind(this)}  onChange={this.DEBOUNCEDonOtherNotaireAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.street')} ref="required-document.street"/>

        <CI isNumbox={true} label="other-address.numbox" qh="why-address.numbox" value={otherNotaire.address.numbox} name="numbox"
            onInput={(this.onOtherNotaireAddressKeyup('address')).bind(this)}  minChar={1}  onChange={this.DEBOUNCEDonOtherNotaireAddressChange.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.numbox')} ref="required-document.numbox"/>

        <CI isAddress={true} label="other-address.zipcity" qh="why-address.zipcity" zip={otherNotaire.address.zip} city={otherNotaire.address.city}
            onChange={this.onOtherNotaireAddressChangeZipCity.bind(this)}
            classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-document.zipcity')} ref="required-document.zipcity"/>

      </form>):null

      return (<div className={classNameAddress} onClick={this.onClickAddress.bind(this,na)}>
        <h5>{T('Info-Document-Address-'+na.is)}</h5>
        <div className={animationClass(Address)}>
          <div className="other-form-address">

          {Address}

          </div>
        </div>

        {icon}
      </div>)
  }
*/



  isEmptyFieldPutRedClass(name){
    return this.props.emptyFields && this.props.emptyFields[name]?'empty-input-field':''
  }
  onOtherAddressKeyup(type){
    return function(e){

      e.preventDefault();
      let {value,name} = e.target;
      let state = {...this.state};
      if(type == 'address'){
        state.other.address[name] = value;
      }else{
        state.other[name] = value;
      }
      this.setState(state);
    }

  }
  onOtherNotaireAddressKeyup(type){
    return function(e){

      e.preventDefault();
      let {value,name} = e.target;
      let state = {...this.state};
      if(type == 'address'){
        state.otherNotaire.address[name] = value;
      }else{
        state.otherNotaire[name] = value;
      }
      this.setState(state);
    }

  }
  onOtherAddressChange(e){

    (this.onOtherAddressKeyup('address')).bind(this,e)
    let {other} = this.state;

    let {address,name,company,phone,email,is}=this.state.other;
    let {street,numbox,zip,city,tva,country} = address;

    this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);

    this.props.setOtherDocument(this.state.other);


  }
  onOtherNotaireAddressChange(e){

    (this.onOtherNotaireAddressKeyup('address')).bind(this,e)
    let {otherNotaire} = this.state;

    let {address,name,company,tva,phone,email,is}=this.state.otherNotaire;
    let {street,numbox,zip,city,country} = address;
    this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);

    this.props.setOtherNotaireDocument(this.state.otherNotaire);


  }
  onOtherAddressChangeZipCity(zc){
    let state = {...this.state};
    state.other.address.zip = zc.zip;
    state.other.address.city = zc.city;
    this.setState(state);

    let {address,name,company,tva,phone,email,is}=this.state.other;
    let {street,numbox,zip,city,country} = address;
    this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);

    this.props.setOtherDocument(this.state.other);


  }/*
  onOtherNotaireAddressChangeZipCity(zc){
      let state = {...this.state};
      state.otherNotaire.address.zip = zc.zip;
      state.otherNotaire.address.city = zc.city;
      this.setState(state);

      let {address,name,company,tva,phone,email,is}=this.state.otherNotaire;
      let {street,numbox,zip,city} = address;
      this.props.setDocument(street,numbox,zip,city,name,company,tva,phone,email,is);

      this.props.setOtherNotaireDocument(this.state.other);


    }*/



  onClickAddress(na){
    let goodNa = {...na}
    let {street,numbox,zip,city,tva,country} = na.address;
    let {name,company,phone,email,is} = na

    country = country||'Belgique'

    this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);
  }

  componentDidUpdate(){

    var Error = this.refs.Error;
    if(Error){
      let y = Error.offsetTop

      let scroll = $(window).scrollTop()
      let h = $(window).height();

      if(y < scroll || (scroll+h) < y){

        if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
          window.Certinergie.goTop(y-250)
        }
      }
    }
  }

};

export default InfoDocument;

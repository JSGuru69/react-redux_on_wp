import React from 'react';
import _ from 'lodash';

import T from '../traduction';
import ZipCityPicker from './ZipCityPicker'
import QH from './QuestionHelp'

import CI from './CustomInput';

class InfoAddress extends React.Component {
  constructor(props){
    super(props);
    this.state = {address : {...props.address},
                  owner : {...props.owner}};

    //create a debounced onChange method
    this.debouncedOnChange = _.debounce(this.onChange,1000);
  }

  onKeyUp(e){
    e.preventDefault();
    let {value,name} = e.target;

    let state = {...this.state};
    state['address'][name] = value;

    this.setState(state);
  }

  onChange(e){
    //call the same logic as in onKeyUp method
    this.onKeyUp.bind(this,e)
    let {street,numbox,zip,city} = this.state.address;
    this.props.setAddress(street,numbox,zip,city);
  }

  onChangeZipCity(zc){
    let newState = {...this.state}
    newState['address'].zip = zc.zip;
    newState['address'].city = zc.city;

    let {street,numbox,zip,city} = newState['address'];
    this.props.setAddress(street,numbox,zip,city);
    this.setState(newState)
  }
  isEmptyFieldPutRedClass(name){
    return this.props.emptyFields && this.props.emptyFields[name]?'empty-input-field':''
  }
  render() {
    let {street,numbox,zip,city} = this.state['address'];
    return (
      <div className="Card Card-shadow Info-Address">
        <h3>{T('info-address.titre')}</h3>
        {this.props.error}
        <form >

          <CI isStreet={true} label='info-address.street'  name="street"
              ref="required-address-street" value={street} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-address-street')}
              onInput={this.onKeyUp.bind(this)} onChange={this.debouncedOnChange.bind(this)} />

          <CI isNumbox={true} label='info-address.numbox'  name="numbox"
              ref="required-address-numbox" minChar={1} value={numbox} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-address-numbox')}
              onInput={this.onKeyUp.bind(this)} onChange={this.debouncedOnChange.bind(this)} />

          <CI isAddress={true} label='info-address.zipcity'
              zip={zip} city={city} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-address-zipcity')} ref="required-address-zipcity"
              onChange={this.onChangeZipCity.bind(this)} />

        </form>
      </div>
    )
  }
};

export default InfoAddress;

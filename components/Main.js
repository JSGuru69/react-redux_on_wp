import React from 'react';
import { Link } from 'react-router';

import BreadCrumb from './BreadCrumb';
import BottomImages from './BottomImages';
class Main extends React.Component{
  render() {
    let propz = {...this.props}

    let children = React.Children.map(propz.children, child => React.cloneElement(child, propz));

    return (
      <div>
        <BreadCrumb maxStep={this.props.order.maxStep}
                    currentStep={this.props.order.currentStep}
                    setCurrentStep={this.props.setCurrentStep}/>
        {children}
        <BottomImages />
      </div>
    )
  }

  redirect(path){
  }
};

export default Main;

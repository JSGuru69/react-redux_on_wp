import React from 'react';
import {Link} from 'react-router';
import { history } from '../store';

import T from '../traduction';
import Pages from '../pages';
const pageId = 2;

import InfoCoordonnee from './InfoCoordonnee';
import InfoAddress from './InfoAddress';
import InfoContact from './InfoContact';

let NEED_TO_GO_UP_ON_ERROR = false;

class StepCoordonnees extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      errorMessage : null,
      emptyFields:{}
    }
  }

  componentWillMount(){
    if(window.Certinergie && window.Certinergie.removeDevisBar && window.Certinergie.removeFooter){
      window.Certinergie.removeDevisBar()
      window.Certinergie.removeFooter()
    }
    if(window.Certinergie && window.Certinergie.goTop){
      window.Certinergie.goTop()
    }

    let {maxStep,currentStep} = this.props.order;

    this.props.setCurrentStep(pageId);
    if(maxStep < pageId){
      let path = Pages
                  .filter((item)=>item.id==maxStep)
                  .map((item)=>item.url)[0]

      history.push(path);
    }
  }

  render() {

    let nextPage = Pages.filter((page)=>page.id==pageId+1)[0]
    let next = (<div id="stepcoordonneesbtn" className="btn-next"><Link to={nextPage.url} onClick={this.checkIfCanGoNext.bind(this)}>{T('nextStep')}</Link></div>)

    let Error = (this.state.errorMessage)?(<div className="Error" ref="Error">{T('Error-'+this.state.errorMessage)}</div>):null

    let ErrorCoordonnee = this.state.errorMessage == 'no-email'?Error:null;
    let ErrorAddress = this.state.errorMessage == 'no-address'?Error:null;
    let ErrorContact = this.state.errorMessage == 'no-contact'?Error:null;

    return (
      <div className="Steps Step-Two">
        <InfoCoordonnee ref="coordonnee"
                        emptyFields={this.state.emptyFields}
                        error={ErrorCoordonnee}
                        user={this.props.order.user}
                        setPopupCoordonneeToClose={this.props.setPopupCoordonneeToClose}
                        popupCoordonneeIsClosed={this.props.order.popupCoordonneeIsClosed}
                        owner={this.props.order.owner}
                        setOwner={this.props.setOwner}
                        setNewUser={this.props.setNewUser}
                        setUser={this.props.setUser}
                        setNewUserAddress={this.props.setNewUserAddress} />


        <InfoAddress ref="address"
                     emptyFields={this.state.emptyFields}
                     error={ErrorAddress}
                     address={this.props.order.address}
                     user = {this.props.order.user}
                     owner={this.props.order.owner}
                     setOwner={this.props.setOwner}
                     setAddress={this.props.setAddress} />

        <InfoContact ref="contact"
                     emptyFields={this.state.emptyFields}
                     error={ErrorContact}
                     contact={this.props.order.contact}
                     setContact={this.props.setContact}
                     setDates={this.props.setDates}
                     user={this.props.order.user}
                     owner={this.props.order.owner}
                     setDateButoir={this.props.setDateButoir}/>


        {next}

      </div>
    )
  }
  componentDidUpdate(){
    let nbOfEmpty = Object.keys(this.state.emptyFields).length;
    let higher = Infinity;
    let scroll = $(window).scrollTop()
    if(nbOfEmpty>0 && NEED_TO_GO_UP_ON_ERROR){
      for(var i in this.state.emptyFields){
        let field = this.state.emptyFields[i]
        let top = scroll+(field && field.getBoundingClientRect)?field.getBoundingClientRect().top:Infinity

        if(top < higher){
          higher = top
        }
      }

      higher = higher<0?-higher:higher;
      if(window.Certinergie && window.Certinergie.goTop && !isNaN(higher)){
        window.Certinergie.goTop(higher-250)
      }
    }



    var Error = this.refs.Error;
    if(Error && NEED_TO_GO_UP_ON_ERROR){
      let y = Error.offsetTop

      let scroll = $(window).scrollTop()
      let h = $(window).height();
      if(y < scroll || (scroll+h) < y){
        if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
          window.Certinergie.goTop(y-250)
        }
      }


    }

    NEED_TO_GO_UP_ON_ERROR = false;
  }

  checkIfCanGoNext(event){
    ga("send", "event" , "bouton", "clic", "étape 2 continuer");
    let emptyFields = null
    for(var i in this.refs){
      let current = this.refs[i]
      for(var j in current.refs){
        let currentB = current.refs[j];
        if(currentB.value.replace(/ /g,'') == '' && (j+'' !== 'LoginMail' && j+'' !== 'LoginPass')){
          emptyFields = emptyFields || {}
          emptyFields[j] = currentB
        }
      }
    }

    //emptyFields = null

    let errorMessage = [];

    /*let order = this.props.order;

    if(!order.user || !order.user.Email){
      errorMessage.push('no-email');
    }

    if(!order.address || order.address.street == "" || order.address.zip == "" || order.address.city == ""){
      errorMessage.push('no-address');
    }
    if(!order.contact || order.contact.phone == ""){
      errorMessage.push('no-contact');
    }*/
/*
    let emptyFieldsArray = Object.keys(emptyFields);

    let firstnameinputindex = emptyFieldsArray.indexOf('firstnameInput')
    if(firstnameinputindex !==-1){
      emptyFieldsArray.splice(firstnameinputindex,firstnameinputindex+1)
      delete emptyFields.firstnameInput
    }
    let lastnameinputindex = emptyFieldsArray.indexOf('firstnameInput')
    if(lastnameinputindex !==-1){
      emptyFieldsArray.splice(lastnameinputindex,lastnameinputindex+1)

      delete emptyFields.lastnameInput
    }

    if(emptyFieldsArray.length>0){
      emptyFields = null
    }
*/
    if((errorMessage && errorMessage.length>0)||!!emptyFields){
      event.preventDefault();
      this.setState({errorMessage:errorMessage[0],emptyFields:emptyFields||{}});
      NEED_TO_GO_UP_ON_ERROR = true;
      return false;
    }else{
      if(this.props.order.maxStep <pageId+1){
        this.props.setMaxStep(pageId+1)
      }
    }

  }
};

export default StepCoordonnees;

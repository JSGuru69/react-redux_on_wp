import React from 'react';
import R from 'browser-request';

import PROMOS from '../data/promo';
import T from '../traduction';

class CodePromo extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      error : null,
      isOpen : false
    }
  }

  componentWillReceiveProps(){
    let newState = {...this.state}
    newState.error = null;
    this.setState(newState)
  }

  render() {

    let {region,expertises,bien,user} = this.props;
    if(region==-1||!bien.buildingType||expertises.length==0){
      return null;
    }

    let error = null;

    if(this.state.error){
      error = <div className="PromoCode-Error">{this.state.error}</div>
    }

    if(this.state.isOpen){
      return (
        <div className="CodePromo">
          {error}
          <form>
            <span>Code<input ref="codepromo" defaultValue="" /></span>
            <div className="AddCodePromo-btn" onClick={this.addPromo.bind(this)}>{T('codepromo-add-it')}</div>
          </form>
        </div>
      )
    }else{
      return (
        <div className="CodePromo">
          <span>{T('codepromo-ask')}</span><span className="rajoutez-promo" onClick={this.openPromo.bind(this)}>{T('codepromo-add')}</span>
        </div>
      )
    }


  }

  openPromo(){
    let newState = {...this.state}
    newState.isOpen = true;
    this.setState(newState)
  }


  addPromo(event){
    let code = this.refs.codepromo.value;
    let ok = this.verify(code);
    if(ok){
      this.refs.codepromo.value = '';
      let newState = {...this.state}
      newState.error = null;
      newState.isOpen = false;
      this.setState(newState)
      this.props.addPromo(code,PROMOS[code])
    }
  }


  verify(code){


    let ln = "fr" ;

    code = code.toLowerCase();

    let promo = PROMOS[code];
    let Error = null;

    if(promo && this.props.user && this.props.user.Consumer && (this.props.user.Consumer.ConsumerType != 1689)){
      let existing = Object.keys(this.props.promoCodeUsed)
      if(existing.indexOf(code)==-1){
        let region = parseInt(this.props.region);
        if(promo.region.indexOf(region)==-1){
          Error = "region not good"
        }else{


            let selectedExp = this.props.expertises.map(e=>e.id);
            let okForExp = promo.expertises.reduce((ok,exp)=>{
              if(selectedExp.indexOf(exp)==-1){
                ok = false;
              }
              return ok;
            },true);

            if(okForExp){
              let state = {...this.state}
              state.isOpen = false;
              this.setState(state)
              return true;
            }else if(okForExp == false){
              Error = promo.errorExpertises[ln]
            }
            /*else if(used>= 1){
                 Error = ({
                'fr' : 'Vous avez déjà utilisé un code promo',
                'nl' : 'U hebt al één promocode gebruikt',
                'en' : 'You have already used one promo code'
              })[ln];
            }*/


        }
      }else{
        Error = ({
          'fr' : 'Ce code promo est déjà utilisé',
          'nl' : 'Deze promotiecode werd al gebruikt',
          'en' : 'This promo already used'
        })[ln];
      }

    }else{
      Error = ({
        'fr' : 'Ce code promo n\'existe pas',
        'nl' : 'Deze promotiecode wordt nog niet herkend door ons systeem. Op dit moment vernieuwt Certinergie haar besturingssysteem om u nog beter van dienst te zijn. Om gebruikt te maken van deze korting, bel ons snel op via 0800 82 171. Dank u voor uw begrip.',
        'en' : 'This promo doesn\'t exist'
      })[ln];
    }

    if(Error){
      let newState = {...this.state}
      newState.error = Error;
      this.setState(newState);
      return false;
    }

  }

};

export default CodePromo;

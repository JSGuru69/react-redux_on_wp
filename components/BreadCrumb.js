import React from 'react';
import {Link,browserHistory} from 'react-router';

import T from '../traduction';
import Pages from '../pages';

class BreadCrumb extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      list : Pages
    }
  }

  render() {
    let links = this.state.list.map((l)=>this.getLink(l))

    return (
      <div className="Bread-Crumb">

      { links }

      </div>
    )
  }

  onLinkChange(id){
    this.props.setCurrentStep(id)
  }

  getLink(l){
    let active = (l.id <= this.props.maxStep)
    let current = (l.id == this.props.currentStep)
    let className = "Bread-Crumb-Item"

    className += ( active?' active':'' )
    className += ( current?' current':'' )

    let text = T('page-'+l.text)
    
    if(active){
      return (<div className={className} key={l.id}>
          <div className="dot"></div>
          <div>
            <Link to={l.url} onClick={this.onLinkChange.bind(this,l.id)}>{text}</Link>
          </div>
      </div>)
    }

    return (<div className={className} key={l.id}>
        <div className="dot"></div>
        <div>
          <span>{text}</span>
        </div>
    </div>)
  }

};

export default BreadCrumb;

import React from 'react';

import T from '../traduction';

class ResumePrice extends React.Component{
  constructor(props){
    super(props)
  }
  render() {

    let {total,products} = this.props.expertisesPrices;
    let {commissionAmount,user,document} = this.props.order;
    let mailOnlyPhrase = null;
    let reducPhrase = null;
    let reduc = (products && products.length > 0)?products.filter(p=>p.productId == 5):[{price:0}];
    reduc = (reduc[0])?reduc[0].price: 0;
    reduc = reduc<0?-reduc:reduc;
    let totalreduc= reduc + ' €';
    if(document.is == 'mail' && (this.props.user && this.props.user.Consumer.ConsumerType == "1688")){
      reduc += 2.5
      total -=2.5
      mailOnlyPhrase = '2,5 €'
    }

    let totalPhrase = total+reduc;

    //let phrase = T('resume-price.phrase',{total : totalPhrase});
    //au lieu de 225 € si vous commandez en ligne maintenant.
    let phrase = <span>{T('resume-price.phrase-a')} <span className="barre-price">{totalPhrase}€</span> {T('resume-price.phrase-b')}. </span>;

    //let phrase = <span>{T('resume-price.phrase-a')} <span className="barre-price">{totalPhrase}€</span> {T('resume-price.phrase-b')} <span class="mail-only-phrase">{mailOnlyPhrase}</span></span>;
    if(user && user.Consumer && user.Consumer.Commission && commissionAmount && commissionAmount>0){
      let comphrase = commissionAmount + " €";
      let tmpfirstname = (user.FirstName)?(user.FirstName +' '):'';
      let tmpname = (user.Name)?user.Name:'';
      phrase = <span>{T('resume-price.phrase-with-com',{agence : tmpfirstname+tmpname})}<span className="commissiontarif">{comphrase}</span></span>
    }

    if(reduc > 0){
      if(document.is == 'mail' && (this.props.user && this.props.user.Consumer.ConsumerType == "1688")){
        //Soit une réduction de (50 € pour commande en ligne + 2,5 € pour envoi des documents par e-mail )
        reducPhrase = <span className="resume-price-reduc-phrase">{T('resume-price.reduction-phrase-a')}<span className="reduction">{totalreduc}</span>{T('resume-price.reduction-phrase-b')} + <span className="mailonlyreduction">{mailOnlyPhrase}</span> {T('resume-price.formailonly')} </span>;
      }else{
        //Soit une réduction de (50 € pour commande en ligne)
        reducPhrase = <span className="resume-price-reduc-phrase">{T('resume-price.reduction-phrase-a')}<span className="reduction">{totalreduc}</span>{T('resume-price.reduction-phrase-b')} </span>;
      }


    }


//      <div className="Card Card-shadow Resume-Price">

    return (
      <div className="Resume-Price">
        <span className="tarif">{total}€</span> {phrase} <br/>{reducPhrase}
      </div>
    )
  }
};

export default ResumePrice;

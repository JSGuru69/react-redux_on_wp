import React from 'react';

import T from '../traduction';

class RegionSelect extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      selectedRegion : props.region || 0
    }
  }

  onClickOnItem(id){
    let newState = {...this.state}
    newState.selectedRegion = id;
    this.props.setRegion(id);
    this.setState(newState)
  }

  displayItem(regionId){

    let className = 'region-select-item'+((this.state.selectedRegion == regionId)?' active':'');
    let color = (this.state.selectedRegion == regionId)?'Blanc':'Bleu'
    let url = '/wp-content/themes/Certinergie/theme-js/commande/Icones/'+color+'/'
    let regionImg = (regionId == 0)? url+'regionwallone.png':
                    (regionId == 1)? url+'regionbruxelloise.png':
                    url+'regionflamande.png'


    return (<div className={className} key={regionId} onClick={this.onClickOnItem.bind(this,regionId)}>
      <img src={regionImg} className="bien-select-image" />
      <span className="bien-select-name">{T('region-select.region.'+regionId)}</span>
    </div>)
  }

  render() {

    let regionsItem = [0,1,2].map((id)=> this.displayItem(id));

    return (
      <div className="Card Card-shadow">
        <div className="Region-Select">
          <h2>1. {T('region-select.titre')}</h2>
          {regionsItem}
        </div>
      </div>
    )
  }
};

export default RegionSelect;

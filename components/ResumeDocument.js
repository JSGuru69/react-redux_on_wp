import React from 'react';

import T from '../traduction';

class ResumeDocument extends React.Component{
  constructor(props){
    super(props);
    this.state = props.document;

  }
  render() {

    let {street,numbox,zip,city,country,name,company,tva,phone,email,is} = this.state;

    let Name = <div><span className="labelleft">{T('other-address.name')}</span><span className="dataright">{name}</span></div>
    let Company = (company)?<div><span className="labelleft">{T('other-address.company')}</span><span className="dataright">{company}</span></div>:null
    let Tva = (tva)?<div><span className="labelleft">{T('other-address.tva')}</span><span className="dataright">{tva}</span></div>:null
    let Street = <div><span className="labelleft">{T('other-address.street')}</span><span className="dataright">{street}&nbsp;{numbox}</span></div>
    let Zipcity = <div><span className="labelleft">{T('other-address.zipcity')}</span><span className="dataright">{zip}&nbsp;{city}</span></div>
    let Country = <div><span className="labelleft">{T('other-address.country')}</span><span className="dataright">{country}</span></div>
    let Red = null;
    let {user,isActe,acteinfo,owner} = this.props;
    if(user.Consumer.ConsumerType == 1688){
      Red = <div className="resume-document-red"><span className="resunereductionprice">{T('resume-document.reductionprice')}</span>{T('resume-document.reduction')}</div>
    }
    /*<span className="resunereductionprice">{T('resume-document.reductionprice')}</span>{T('resume-document.reduction')}*/
    let mails = user.Email;
    if(user.Consumer.ConsumerType != 1688 && owner != null && owner.email != null && owner.email != ""){
      mails += " | "+ owner.email
    }
    let isMail = (<div className="itable fifty">
                                  <div className="byemail">
                                      <span className="labelleft"> {mails}
                                      </span>
                                      </div>
                              </div>);


    let invoiceAddress = (<div className="itable fifty">{Name}
                                                                  {Company}{Tva}
                                                                  {Street}
                                                                  {Zipcity}
                                                                  {Country}
                                </div>)
//      <div className="Card Card-shadow Resume-Document">

    let password = (this.props.user.Password)?this.props.user.Password:"certinergie";
    let boolmail= (is=='mail')?true:false;
    let sendonlybymail = (user.Consumer.ConsumerType === 1688)?(<div className="chooseOnlyByMail">
                        <input type="checkbox" className="regular-checkbox" checked={boolmail} onClick={this.setMailOnly.bind(this,!boolmail)} />
                        <span><span className="resunereductionprice"> {T('resume-document.reductionprice-private')} </span><span>{T('resume-document.reduction-private')}</span></span>
                     </div>):
                     (<div className="chooseOnlyByMail">
                                         <input type="checkbox" className="regular-checkbox" checked={boolmail} onClick={this.setMailOnly.bind(this,!boolmail)} />
                                         <span className="resumedocumentmailonlycompany">{T('resume-document.reduction-company')}</span>
                                      </div>)


    let addressedubien = (!(
      //on cache la div si
        //agence et paiement acte
        (user.Consumer.ConsumerType === 1689 && isActe)
        ||
        //notaire, paiement acte et pas de num national
        (user.Consumer.ConsumerType === 8122 && isActe && !(acteinfo.numNational && acteinfo.numNational !== ""))
        || (boolmail == true)
    ))?(<div>
      <div className="itable fifty">{T('resume-document.phrase-a-env-courrier')} </div>
      {invoiceAddress}

    </div>):null

    let mail = (<div>
      <div className="itable fifty">{T('resume-document.phrase-a-env-mail')} </div>
      {isMail}

    </div>)
    return (

      <div className="Resume-Document">

        <h3>{T('resume-document.titre')}</h3>
        <div>
          <div className="itable fifty">{T('resume-document.en-ligne')} </div>
          <div className="itable fifty">
            <div><span className="labelleft">{T('resume-document.login')}</span><span className="dataright">{this.props.user.Email}</span></div>
            <div><span className="labelleft">{T('resume-document.password')}</span><span className="dataright">{password}</span></div>
          </div>
        </div>
        {addressedubien}
        {mail}
        {sendonlybymail}
      </div>
    )
  }

  changeCourrier(event){
    this.props.toggleSendByMail()
  }
  noop(){}

  setMailOnly(value,e){
    //console.log("value",value)
    //console.log("thisstat",this.state)
    let stat = this.state
    if(value == true){
          stat.is = "mail"
    }else{
      stat.is = "courrier"
    }
    this.setState(stat);
    let {street,numbox,zip,city,country,name,company,tva,phone,email,is} = this.state;
    this.props.setDocument(street,numbox,zip,city,country,name,company,tva,phone,email,is);
    this.props.toggleSendByMail()
  }

};

export default ResumeDocument;

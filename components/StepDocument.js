import React from 'react';
import {Link} from 'react-router';
import { history } from '../store';

import T from '../traduction';
import Pages from '../pages';

let NEED_TO_GO_UP_ON_ERROR = false

const pageId = 3;

import InfoDocument from './InfoDocument';

class StepDocument extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      errorMessage : null,
      emptyFields : {}
    }
  }

  componentWillMount(){
    if(window.Certinergie && window.Certinergie.removeDevisBar && window.Certinergie.removeFooter){
      window.Certinergie.removeDevisBar()
      window.Certinergie.removeFooter()
    }
    if(window.Certinergie && window.Certinergie.goTop){
      window.Certinergie.goTop()
    }

    let {maxStep,currentStep} = this.props.order;

    this.props.setCurrentStep(pageId);
    if(maxStep < pageId){
      let path = Pages
                  .filter((item)=>item.id==maxStep)
                  .map((item)=>item.url)[0]



      history.push(path);
    }
  }


  render() {

    let nextPage = Pages.filter((page)=>page.id==pageId+1)[0]
    let next = (<div id="stepdocumentbtn" className="btn-next"><Link to={nextPage.url} onClick={this.checkIfCanGoNext.bind(this)}>{T('nextStep')}</Link></div>)


    return (
      <div className="Steps Step-Three">

        <InfoDocument document={this.props.order.document}
                      addressDocumentAutre={this.props.order.addressDocumentAutre}
                      user={this.props.order.user}
                      owner={this.props.order.owner}
                      otherDocument={this.props.order.otherDocument}
                      otherNotaireDocument={this.props.order.otherNotaireDocument}
                      address={this.props.order.address}
                      setDocument={this.props.setDocument}
                      setAddressDocumentAutre = {this.props.setAddressDocumentAutre}
                      setOtherDocument =  {this.props.setOtherDocument}
                      setOtherNotaireDocument =  {this.props.setOtherNotaireDocument}

                      error = {this.state.errorMessage}
                      ref="document"
                      emptyFields={this.state.emptyFields}/>

        {next}

      </div>
    )
  }
  checkIfCanGoNext(event){
     ga("send", "event", "bouton", "clic", "étape 3 continuer");
    let emptyFields = null
    for(var i in this.refs){
      let current = this.refs[i]
      for(var j in current.refs){
        let currentB = current.refs[j];
        if(currentB.value.replace(/ /g,'') == ''){
          emptyFields = emptyFields || {}
          emptyFields[j] = currentB
        }
      }
    }

    let errorMessage = null;


    let {document,otherDocument} = this.props.order;

    if(!document || document.street == '' || document.name ==''){
      errorMessage = "no-document-selected"
    }

    if(document.is == 'other' || document.is =='otherNotaire' || document.is =='mail'){
      errorMessage = null;
    }

    if(errorMessage || emptyFields){
      event.preventDefault();
      NEED_TO_GO_UP_ON_ERROR = true
      this.setState({errorMessage:errorMessage,emptyFields:emptyFields||{}})
    }else{
      if(this.props.order.maxStep <pageId+1){
        this.props.setMaxStep(pageId+1)
      }
    }

  }

  componentDidUpdate(){
    let nbOfEmpty = Object.keys(this.state.emptyFields).length;
    let higher = Infinity;
    let scroll = $(window).scrollTop()
    if(nbOfEmpty>0 && NEED_TO_GO_UP_ON_ERROR){
      for(var i in this.state.emptyFields){
        let field = this.state.emptyFields[i];
        let top = scroll+(field && field.getBoundingClientRect)?field.getBoundingClientRect().top:Infinity
        if(top < higher){
          higher = top
        }
      }
      higher = higher<0?-higher:higher;

      NEED_TO_GO_UP_ON_ERROR = false
      if(window.Certinergie && window.Certinergie.goTop && higher!= Infinity &&  !isNaN(higher)){
        //window.Certinergie.goTop(higher-250)
      }
    }else{
      var Error = this.refs.Error;
      if(Error){
        let y = Error.offsetTop

        let scroll = $(window).scrollTop()
        let h = $(window).height();

        if(y < scroll || (scroll+h) < y){

          if(window.Certinergie && window.Certinergie.goTop && !isNaN(y)){
            window.Certinergie.goTop(y-250)
          }
        }
      }
    }
  }
};

export default StepDocument;

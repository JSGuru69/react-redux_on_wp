import React from 'react';

import T from '../traduction';

class ResumeClauseMandat extends React.Component{

  constructor(props){
    super(props)
    this.state = {open : false};
  }

  render(){

    let name = this.props.user.FirstName+' '+this.props.user.Name;

    let hasPeb = this.props.products.filter((p)=>p.productId == 1).length>0
    let hasElec = this.props.products.filter((p)=>p.productId == 4).length>0
    let hasPlan = this.props.products.filter((p)=>p.productId == 14).length>0
    let hasCit6 = this.props.products.filter((p)=>p.productId == 6).length>0
    let hasCit9 = this.props.products.filter((p)=>p.productId == 9).length>0

    let productsPhrase = []
    if(hasPeb){productsPhrase.push('product-1')}
    if(hasElec){productsPhrase.push('product-4')}
    if(hasPlan){productsPhrase.push('product-14')}
    if(hasCit6){productsPhrase.push('product-6')}
    if(hasCit9){productsPhrase.push('product-9')}

    let product = productsPhrase.map(n=>T(n)).join(', ')
    //( Certificat PEB- Contrôle électrique – contrôle citerne à mazout )
    let sub = (this.state.open)?(<div className="clause-mandat-text">
      {T('ResumeClauseMandat.phrase',{name,product})}

      </div>):null

    if(this.props.user && this.props.user.Consumer && this.props.user.Consumer.ConsumerType == 1689){
      return (
        <div className="Resume-clause-mandat">
          <div>{T('resume-bdc.clause')}<a className="Resume-clause-mandat-btn" onClick={this.toggleOpen.bind(this)}>{T('ResumeClauseMandat.titre')}</a></div>
          {sub}
        </div>
      )
    }
    return null;

  }

  toggleOpen(){
    let state = {...this.state}
    state.open = !state.open
    this.setState(state)
  }

}

export default ResumeClauseMandat

import React from 'react';
import R from 'browser-request';

import T from '../traduction';

import Slider from 'rc-slider';
import SliderStyle from '../css/rcslider.css';


class Commission extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      min : 0,
      max : 10
    }
  }


  render() {
    if(!this.props.user || !(this.props.user && this.props.user.Consumer && this.props.user.Consumer.Commission==1 && this.props.user.Consumer.ConsumerType == 1689) || !(-this.props.max > 0)){

/*
  <div className="Tarif-slider">
  </div>
*/
      return (
        <div className="Tarif-total">

          <div className="Tarif-slider"><span className="Tarif-total-tvac">{T('commission-total-tvac')}</span></div>
          <div className="Tarif-price"><div>{this.props.total}€</div></div>
        </div>
      )
    }
    /*
    if(!this.props.user || !(this.props.user && this.props.user.Consumer && this.props.user.Consumer.Commission==1) || !(-this.props.max > 0)){ return (
      <div className="Tarif-total"><div className="Tarif-name">Total TVAC</div> <div className="Tarif-price">{this.props.total}€</div></div>)
    }*/

    let min = this.props.min
    let max = -this.props.max

    let m = max-min;

    let  getSteps = function(amount,nbStep,comm){
      let step = amount/nbStep;
      let steps = []
      for(var i = 0; i<nbStep+1;i++){

        steps.push(step*i)
      }
      return steps.map((s,i)=> {
        let isactive = comm>=s
        let classNameStep = "steps-step steps-"+steps.length+" step-"+i+((isactive)?' active':'');
        return (<div className={classNameStep}>{s}</div>)
      })
    }


    let stepsDiv = getSteps(m,5,this.props.value);



    return     (<div className="Tarif-total-commission">
      <div className="Tarif-slider">
        <div className="steps">{stepsDiv}</div>
        <Slider defaultValue={this.props.value} min={min} max={max} tipTransitionName="rc-slider-tooltip-zoom-down" onChange={this.onSliderChange.bind(this)} />
      </div>
      <div className="Tarif-commission">{T('commission-agence')} <div>{this.props.value}€</div></div>
      <div className="Tarif-price">{T('commission-invoice-proprio')} <div>{this.props.total}€</div></div>
    </div>)


    return (

      <span className='commission-bar'>
        <h4>{T('commission-titre')}</h4>
        <p>{T('commission-explication')}</p>
        <Slider defaultValue={this.props.value} min={this.props.min} max={-this.props.max} tipTransitionName="rc-slider-tooltip-zoom-down" onChange={this.onSliderChange.bind(this)} />
      </span>
    )
  }

  onSliderChange(change){
    this.props.setCommission(change);
  }
}

export default Commission;

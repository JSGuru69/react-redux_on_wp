import React from 'react';
import _ from 'lodash';
import CI from './CustomInput';
import T from '../traduction';

class ResumeRdv extends React.Component{

  constructor(props){
    super(props);
    let {contact} = props;
    contact.openEditPopup = false
    this.state = {contact}
  }

  render() {
    let {contact} = this.state;
    let datesLi = this.getDatesLi(this.state.contact.dates);

    let rdvResume = (this.state.contact.dates.length>0)?(<ul>{datesLi}</ul>):(<span>{T('no-date-wanted')}</span>)
//      <div className="Card Card-shadow Resume-Rdv">

    let POPUP = this.state.openEditPopup?this.getPopup():null;

    return (
      <div className="Resume-Rdv">
        <div>
          <div className="itable fifty">
            <h3>{T('resume-rdv.titre')}</h3>
            {rdvResume}
          </div>
          <div className="itable fifty">
            <h3>{T('resume-rdv.contact')}</h3>
            <div>
              <div className="resume-rdv-picture">
                <img src="/wp-content/themes/Certinergie/theme-js/commande/Icones/Bleu/Proprio.png" />
              </div>
              <div className="resume-rdv-name">
                {contact.firstname}&nbsp;{contact.lastname} <br/>
                {contact.phone}
                <div className="edit-btn" onClick={this.openEditPopup.bind(this)}><i className="fa fa-pencil-square-o"></i></div>
              </div>
            </div>
          </div>
        </div>
        {POPUP}
      </div>
    )
  }

  getDatesLi(dates){
    return _.map(dates,function(dateObj,i){
      let date = dateObj.date
      if(typeof date == 'string'){
        date = new Date(date)
      }
      if(date){
        let d = _.map([date.getDate(), date.getMonth()+1,date.getFullYear()],(num)=>{
          if((''+num).length<2){ return '0'+num; }
          return num;
        }).join('/');
        let dayMomentPhrase = T('dvp-moment-'+dateObj.dayMoment)/*dateObj.dayMoment==0?'Toutes la journée':
                              dateObj.dayMoment==1?'Matin':
                              'Après-midi'*/
        return (<li key={i}>{d} - {dayMomentPhrase}</li>)
      }
      return null;

    });
  }




  openEditPopup(e){
    let state = {...this.state};
    state.openEditPopup = true;
    this.setState(state);
  }
  closeEditPopup(e){
    let state = {...this.state};
    state.openEditPopup = false;
    this.setState(state);
  }

  onClickPopupBtn(e){
    let state = {...this.state}
    let {firstname,lastname,phone,dates} = this.state.contact
    this.props.setContact(firstname,lastname,phone,dates||[])
    this.setState(state);
    this.closeEditPopup();
  }
  onInputPopup(e){
    let name = e.target.name;
    let value = e.target.value;
    let state = {...this.state}
    state.contact[name]=value;
    this.setState(state)
  }
  getPopup(){
    let {firstname,lastname,phone,dates} = this.state.contact

    return (
      <div className="popup-dark">
        <div className="popup popup-contact-edit Card Card-shadow">
          <h3>{T('editcontact')}</h3>
          <form>
            <CI label="info-contact.nom" name="lastname" onInput={this.onInputPopup.bind(this)} value={lastname}/>
            <CI label="info-contact.prenom" name="firstname" onInput={this.onInputPopup.bind(this)} value={firstname}/>
            <CI label="info-contact.phone" name="phone" onInput={this.onInputPopup.bind(this)} value={phone}/>
          </form>
          <span className="form-send" onClick={this.onClickPopupBtn.bind(this)}>{T('popup-btn')}</span>
          <div className="close-popup" onClick={this.closeEditPopup.bind(this)}><i className="fa fa-times"></i></div>
        </div>
      </div>
    )
  }

};

export default ResumeRdv;

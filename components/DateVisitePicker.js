import React from 'react';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import T from '../traduction';

const maxDate = 6;

class DateVisitePicker extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      dates : props.dates || [],
      message : this.getMessage(props.dates.length),
      selected : null
    }
  }

  render(){
    let {dates,message} = this.state;

    return (
      <div className="DateVisitePicker">
        <h4>{T('info-contact.dates')}</h4><br/>
        {this.displayMessage()}
        <div>
        {this.displayDatePicker()}
        {this.displayDates()}
        </div>
      </div>
    )
  }

  handleChange(momentDate){
    let newState = {...this.state}

    if(momentDate && momentDate._d){
      let date = momentDate._d;
      newState.selected = momentDate;
    }else{
      newState.selected = null;
    }

    this.setState(newState);
  }

  onClickAddDate(e){
    e.preventDefault();
    let selectedDate = (this.state.selected && this.state.selected._d)?this.state.selected._d:null;

    let val = this.refs.dayMoment.value

    if(selectedDate){
      let added = this.addDate({
        date : selectedDate,
        dayMoment: val
      });

    }

  }

  displayDates(){
    return (<div className="DVP-Dates">
      { _.map(this.state.dates,(dateObj,i)=>{
        let date = dateObj.date
        if(typeof date=='string'){
          date = new Date(date);
        }
        let dateString = _.map([date.getDate(), date.getMonth()+1,date.getFullYear()],(num)=>{
          if((''+num).length<2){
            return '0'+num;
          }
          return num
        }).join('/')

        let dayMomentPhrase = dateObj.dayMoment==0?'Toutes la journée':
                              dateObj.dayMoment==1?'Matin':
                              'Après-midi'

        return (
          <div className="DVP-Date" key={i}>
            {dateString}<br/>{dayMomentPhrase} <div className="DVP-remove" onClick={this.removeDate.bind(this,i)}>x</div>
          </div>
        )
      }) }
      <p className="info-contact-explication DVP-Dates-Explication">{T('date-visite-explication-1')}<br/>{T('date-visite-explication-2')}</p>

    </div>)
  }

  displayDatePicker(){

    let locale = Certinergie.lang() == 'nl'? 'nl-be':
                 Certinergie.lang() == 'en'? 'en-gb':
                 'fr-be';

    let addBtn = (this.state.message)?
    (<div className="DVP-Add disabled"><i className="fa fa-plus"></i></div>):
    (<div className="DVP-Add" onClick={this.onClickAddDate.bind(this)}><i className="fa fa-plus"></i></div>)


    let addZone =this.state.selected? (<div>
      <div className="DVP-Select-day-moment">
        <select ref="dayMoment">
          <option value="0">{T('dvp-moment-0')}</option>
          <option value="1">{T('dvp-moment-1')}</option>
          <option value="2">{T('dvp-moment-2')}</option>
        </select>
      </div>
      {addBtn}
    </div>):null

    return (<div className="DVP-Select">
      <DatePicker
        dateFormat="DD/MM/YYYY"
        locale={locale}
        placeholderText={T('info-contact.date-picker-placeholder')}
        className="DVP-Picker"

            inline
        onChange={this.handleChange.bind(this)}

        disabled={!!this.state.message}
        selected={this.state.selected}

        excludeDates={this.state.dates.map(d=>d.date)}
        filterDate={ this.isWeekday }
        minDate={moment().add(1,'days')}
      />
      {addZone}
    </div>)
  }

  isWeekday(momentDate){
    return (momentDate.isoWeekday()<6)
  }

  displayMessage(){
    let {message} = this.state;
    return null;
    if(message){
      return (<div className="DVP-Message">{message}</div>)
    }
    return null;
  }

  getMessage(l = 0){
    return (l >= maxDate)?T('info-contact.message-max-date',{maxDate}):null;
  }

  addDate(date){
    let {dates} = this.state;
    let newState = {...this.state}
    if(dates.length < maxDate){
      dates.push(date);
      dates.sort((a,b)=> a.date>b.date ? 1 : a.date<b.date ? -1 : 0);
      newState.dates = dates;
    }
    newState.message = this.getMessage(dates.length);
    if(newState.message){
      newState.selected = null;
    }
    if(this.props.onChange){
      this.props.onChange(newState.dates.map(function(d){
        if(d.date.setHours){d.date.setHours(12)}
        d.date = d.date.toString?d.date.toString():d;
        return d
      }));
    }
    newState.selected = null;

    this.setState(newState);
    return (newState.message)?false:true;
  }

  removeDate(i){
    let {dates} = this.state;
    let newState = {...this.state}
    newState.dates = [
      ...dates.slice(0,i),
      ...dates.slice(i+1)
    ];
    newState.message = this.getMessage(newState.dates.length);
    if(this.props.onChange){
      this.props.onChange(newState.dates);
    }
    this.setState(newState)
  }


}

export default DateVisitePicker;

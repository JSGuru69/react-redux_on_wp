import React from 'react';
import T from '../traduction';
import MailInput from './MailInput';

import ZipCityPicker from './ZipCityPicker';
import QH from './QuestionHelp';
import CI from './CustomInput';


const TYPES = {
    1688 : 'normal',
    1689 : 'agence',
    8122 : 'notaire',
    1691 : 'installateur'
}
const TYPESORDER = [1688,1689,8122,1691]

class InfoCoordonnee extends React.Component{

  constructor(props){
    super(props);


    let hiddenLogin = props.user.UserID || props.popupCoordonneeIsClosed ?true:false

    this.state = { owner : {...props.owner},
                   user : {...props.user},
                   userExist : props.user && props.user.UserID,
                   newUserType : props.user.newUserType || 1688,
                   loginError : null,
                   hiddenLogin : hiddenLogin
                 };




    this.debouncedOnChangeOwner = _.debounce(this.onChangeOwner,1000);
    this.debouncedOnChangeUser = _.debounce(this.onChangeUser,1000);
    this.debouncedOnChangeAddress = _.debounce(this.onChangeAddress,1000);

  }

  render() {
    let {user} = this.props;

    let content = (user && user.UserID)?(<div>
        {this.renderExisting(user)}
        {(user.Consumer && user.Consumer.ConsumerType == "1688")?null:this.renderAskOwner()}
      </div>):this.renderAskClient();


    return (<div className="InfoCoordonnee">
      {content}
    </div>)
  }

  renderExisting(user){


    let status = user.Consumer.Status || null;
    let ctype = user.Consumer.ConsumerType || 1688
    let statusText = status == "3" ? 'status-gold':
                     status == "2" ? 'status-silver':
                     status == "1" ? 'status-bronze':
                     status == "4" ? 'status-platinum':
                     status == "11" ? 'status-bronzeNl':
                     status == "12" ? 'status-silverNl':
                     status == "13" ? 'status-goldNl':
                     null;

    let clientTypeText = ctype == 1691 ? 'client-type-installateur':
                         ctype == 1689 ? 'client-type-agence':
                         ctype == 8122 ? 'client-type-notaire':
                         'client-type-normal'

    let statusContent = (status && statusText)?(<span>{T('status-before')} <b>{T(statusText)}</b><br/></span>):null;



    let ad = {
      Street : '',
      PostalCode : '',
      VatNumber : '',
      NumBox : '',
      City : ''
    }
    if(user && user.Addresses && user.Addresses[0]){
      ad = {...user.Addresses[0]}
    }
    
    return (<div className="Card Card-shadow Info-Coordonnee-Existing">
            <h3>{T('info-coordonnee-voscoordonnee.title')}</h3>
            <div className="logout-user" onClick={this.logout.bind(this)}>{T('info-coordonnee-voscoordonnee.logout')}</div>
            <div className="Info-Coordonnee-Existing-left">
              <p>
              <b>{user.FirstName} {user.Name}</b>{T('info-coordonnee.withemail')}<b>{user.Email}</b> <br/>
              {ad.Street} {ad.NumBox}<br/>
              {ad.PostalCode} {ad.City}<br/>
              {ad.VatNumber}</p>
            </div>
            <div className="Info-Coordonnee-Existing-right">
              {statusContent}
            </div>

          </div>)
          //ajouté bouton se connecter sur espace client
  }

  logout(){
    if(window.Certinergie && window.Certinergie.logout){
      window.Certinergie.logout()
    }
  }

  onKeyUp(type){
    return function(e){
      e.preventDefault();
      let {value,name} = e.target;
      let state = {...this.state};
      if(type == 'address'){
        state.user.newaddress[name] = value;
      }else{
        state[type][name] = value;
      }
      this.setState(state);
    }
  }



  onChangeUser(e){
    //call the same logic as in onKeyUp method
    (this.onKeyUp('user')).bind(this,e)
    let type = this.state.newUserType
    let {FirstName,Name,PhoneNumber,Email} = this.state.user;
    this.props.setNewUser(type,FirstName,Name,PhoneNumber,Email)
  }

  onChangeAddress(e){
    (this.onKeyUp('address')).bind(this,e)
    let {newaddress} = this.state.user;
    this.props.setNewUserAddress(newaddress)
  }

  isEmptyFieldPutRedClass(name){
    return this.props.emptyFields && this.props.emptyFields[name]?'empty-input-field':''
  }
  //owner of the house ask if no user user or user user is notaire|agency
  renderAskOwner(){
    let {owner} = this.state;
    let {firstname,lastname,phone,email} = owner
    //{T('info-owner.firstname')}
    return (<div className="Card Card-shadow Info-Coordonnee-Owner">
      <h3>{T('info-coordonnee.titre.proprio')}</h3>
      <form>
          <CI label='info-owner.firstname'  name="firstname"
              ref="required-firstname" value={firstname} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-firstname')}
              onInput={(this.onKeyUp('owner')).bind(this)} onChange={this.debouncedOnChangeOwner.bind(this)} />

          <CI label='info-owner.lastname'  name="lastname"
              ref="required-lastname" value={lastname} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-lastname')}
              onInput={(this.onKeyUp('owner')).bind(this)} onChange={this.debouncedOnChangeOwner.bind(this)} />


          <CI label='info-owner.phone' qh="why-owner-phone" name="phone"
              ref="required-phone" value={phone} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-phone')}
              onInput={(this.onKeyUp('owner')).bind(this)} onChange={this.debouncedOnChangeOwner.bind(this)} />

          <CI isMail={true} noCheckOnline={true}
              label='info-owner.email' qh="why-owner-email"
              value={email} onChange={this.onChangeEmailOwner.bind(this)} />
      </form>
    </div>)
  }
  onChangeOwner(e){
    //call the same logic as in onKeyUp method
    (this.onKeyUp('owner')).bind(this,e)
    let {id,firstname,lastname,phone,email} = this.state.owner;
    this.props.setOwner(id,firstname,lastname,phone,email)
  }

  onChangeEmailOwner(data){
    let state = {...this.state};
    state['owner']['email'] = data.email;
    this.setState(state)
    let {id,firstname,lastname,phone,email} = this.state.owner;
    this.props.setOwner(id,firstname,lastname,phone,email)
  }
  onChangeEmailUser(data){
    let state = {...this.state};
    state['user']['Email'] = (data.isFree&&data.isValid)?data.email:'';
    this.setState(state)
    let type = this.state.newUserType
    let {FirstName,Name,PhoneNumber,Email} = this.state.user;
    this.props.setNewUser(type,FirstName,Name,PhoneNumber,Email)
  }




  //create new client|agency|notaris
  renderAskClient(){
    let {user} = this.props;
    let {newUserType} = user

    let NewUser = null /*(newUserType == 8122)?this.renderNewNotaire():
              (newUserType == 1689)?this.renderNewAgence():
              (newUserType == 9999)?this.renderNewInstallateur():
              this.renderNewNormal()*/
    let NewUserTrue = null
    let ProprioForm = null;

    if(newUserType == 8122 || newUserType == 1689 || newUserType == 9999){
      NewUser = this.renderNewUser();
      ProprioForm = this.renderAskOwner()
    }else{
      NewUser = this.renderNewUser(true);
    }



    let loginForm = (user && user.UserID && this.state.hiddenLogin)?null:this.renderLoginForm()
    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }
    return (<div>
      <div className="Card Card-shadow">
        <h2>{T('info-coordonnee-voscoordonnee.title')}</h2>
        {this.renderClientTypePicker(loginForm)}
        <div className={animationClass(this.props.error)}>{this.props.error}</div>
        {NewUser}

      </div>
      <div className={animationClass(ProprioForm)}>{ProprioForm}</div>

    </div>)

  }

  renderLoginForm(){
    let imnew = function(){
      let newState = {...this.state}
      //STOP THE POPUP LOGIN
      //this.props.setPopupCoordonneeToClose();
      newState.hiddenLogin = true;
      this.setState(newState)
    }

    let errorLogin = (this.state.loginError)?(<div className="loginError">{this.state.loginError}</div>):null;
    let classNameLogin = 'login-form'+((this.state.hiddenLogin)?'':' open');
    return (<div className={classNameLogin}>
      <form>
        <div className="login-left">
          <h4>{T('login-already-client')}</h4>
          <span><input placeholder="Email" autocomplete='off' ref="LoginMail" defaultValue="" name="LoginMail" /></span>
          <span className="LoginPass"><input placeholder={T('password')} type="search" ref="LoginPass" defaultValue="" name="LoginPass" type="password" /></span>
          <div className="LoginBtn" onClick={this.login.bind(this)}>{T('login-button')}</div>
          {errorLogin}
          <span className="password-forgotten">{T('password-forgotten')}</span>
        </div>
        <div className="login-form-newclient">
          <h4>1<sup>{T('login-continue-without.firstvisitesup')}</sup>{T('login-continue-without.firstvisite')}</h4>
          <span className="login-from-newclient-explication">{T('login-continue-without-explication')}</span>
          <div className="LoginBtn"onClick={imnew.bind(this)}>{T('login-continue-without.btn')}</div>
        </div>
      </form>
    </div>)
  }

  login(e){
    if(Certinergie && Certinergie.login){
      Certinergie.login(this.refs.LoginMail.value,this.refs.LoginPass.value).then((res)=>{
        if(ls && ls){
          ls.set('logged',true,true);
          ls.set('user',res,true);
          this.props.setUser(res)
        }
        if(window.Certinergie){
          window.Certinergie.initLoginSystem();
        }
        this.props.setPopupCoordonneeToClose();
      },(err)=>{

        let newState = {...this.state}
        newState.loginError = err;
        this.setState(newState)
      })
    }
  }
  onChangeZipCity(e){

    let state = {...this.state};
    state.user.newaddress.zip = e.zip;
    state.user.newaddress.city = e.city ;
    this.setState(state);
    this.props.setNewUserAddress(state.user.newaddress)



  }

  renderNewUser(emailRequired){
    let {FirstName,Name,PhoneNumber,Email,Address, newaddress,Consumer,newUserType}= this.state.user;
    let ADDRESS = (this.state.newUserType == 1688)? null :
      (<div>

          <CI isStreet={true} label='info-address.street'  name="street"
              value={newaddress.street}
              onInput={(this.onKeyUp('address')).bind(this)} onChange={this.debouncedOnChangeAddress.bind(this)} />

          <CI isNumbox={true} label='info-address.numbox' name="numbox"
              value={newaddress.numbox} minChar={1}
              onInput={(this.onKeyUp('address')).bind(this)} onChange={this.debouncedOnChangeAddress.bind(this)} />

          <CI isAddress={true} label='info-address.zipcity'
              zip={newaddress.zip} city={newaddress.city} onChange={this.onChangeZipCity.bind(this)} />
          <CI label='info-address.tva'  name="tva"
              value={newaddress.tva}
              onInput={(this.onKeyUp('address')).bind(this)} onChange={this.debouncedOnChangeAddress.bind(this)} />

        </div>)
    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }


    return (<div className="Info-Coordonnee-New-User">
      <form >

      <CI label='info-owner.firstname' name="FirstName"
          ref="required-firstname-user" value={FirstName} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-firstname-user')}
          onInput={(this.onKeyUp('user')).bind(this)} onChange={this.debouncedOnChangeUser.bind(this)}  />

      <CI label='info-owner.lastname' name="Name"
          ref="required-phone-user" value={Name} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-lastname-user')}
          onInput={(this.onKeyUp('user')).bind(this)} onChange={this.debouncedOnChangeUser.bind(this)}  />

      <CI label='info-owner.phone' qh="why-newuser-phone" name="PhoneNumber"
          ref="required-lastname-user" value={PhoneNumber} classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-phone-user')}
          onInput={(this.onKeyUp('user')).bind(this)} onChange={this.debouncedOnChangeUser.bind(this)}  />

      <CI isMail={true}
          label='form-info-mail' qh="why-newuser-email"
          classNameRequired={(this.isEmptyFieldPutRedClass.bind(this))('required-mail-user')} ref="required-mail-user"
          onChange={this.onChangeEmailUser.bind(this)} isRequire={emailRequired}
          value={Email} onChange={this.onChangeEmailUser.bind(this)} />


        <div className={animationClass(ADDRESS)}>{ADDRESS}</div>
      </form>
    </div>)
  }/*
  renderNewNormal(){
    return (<div>
      {this.renderNewUser(true)}
    </div>)
  }
  renderNewNotaire(){
    return (<div>
      {this.renderNewUser()}
      {this.renderAskOwner()}
    </div>)
  }
  renderNewAgence(){
    return (<div>
      {this.renderNewUser()}
      {this.renderAskOwner()}
    </div>)
  }
  renderNewInstallateur(){
    return (<div>
      {this.renderNewUser()}
      {this.renderAskOwner()}
    </div>)
  }
*/
  renderClientTypePicker(loginForm){
    let {newUserType} = this.state;

    let Items = TYPESORDER.map(type=>{
        let className = 'clientTypePicker-item'+((type == newUserType)?' active':'');

        let color = (type == newUserType)?'Blanc':'Bleu'
        let url = '/wp-content/themes/Certinergie/theme-js/commande/Icones/'+color+'/'
        let clientImg = (type == 9999)? url+'elec.png':
                        (type == 1689)? url+'agenceimmo.png':
                        (type == 8122)? url+'notaire.png':
                        url+'Proprio.png'

        let imgClass = 'clientTypePicker-img-'+type;
        return (<div className={className} key={type} onClick={this.chooseClientType.bind(this,type)}>
          <img src={clientImg} className={imgClass} />
          <span className="clientTypePicker-name"> {T('clientTypePicker-'+TYPES[type])}</span>
        </div>)
    })
    let animationClass = function(something){
      return something?'closed-animation open-animation':'closed-animation'
    }
    return (<div className='clientTypePicker-items'>
      {Items}
      <div className={animationClass(loginForm)}>{loginForm}</div>
    </div>)
  }
  chooseClientType(type){
    let newState = {...this.state}
    newState.newUserType = type;
    this.setState(newState)


    let {FirstName,Name,PhoneNumber,Email} = this.state.user;
    this.props.setNewUser(type,FirstName,Name,PhoneNumber,Email)
  }
};

export default InfoCoordonnee;

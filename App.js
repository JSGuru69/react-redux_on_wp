import React from 'react';
import { render } from 'react-dom';

// Import css
import reactdatepickercss from './css/reactdatepicker.css';
//import style from './css/style.css';
// Import Components
import App from './components/App';

import StepDevis from './components/StepDevis';
import StepCoordonnees from './components/StepCoordonnees';
import StepDocument from './components/StepDocument';
import StepValidation from './components/StepValidation';

// import react router deps
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from './store';
import {SetLangue} from './traduction';

const startPath = '/'+(window.location.pathname.split('/').reduce(function(tab,current){
    if(current != ''){tab.push(current)}
    return tab
},[]).slice(0,2).join('/'))+'/';

const router = (
  <div className="AppCommande">
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={StepDevis}></IndexRoute>
        <Route path="/rendez-vous" component={StepCoordonnees}></Route>
        <Route path="/documents" component={StepDocument}></Route>
        <Route path="/validation"  component={StepValidation}></Route>
        <Route path="*"  component={StepDevis}></Route>
      </Route>
    </Router>
  </Provider>
  </div>
)

//langue switcher
if(window.location.pathname.indexOf('/nl/')!=-1){
  SetLangue('nl')
}else if(window.location.pathname.indexOf('/en/')!=-1){
  SetLangue('en')
}

render(router, document.getElementById('App'));

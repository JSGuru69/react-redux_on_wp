function test(state = {}, action) {

  //let newState = Object.assign({},state);
  //let newState = {...state}
  let newState = (cloneObject(state));

  switch(action.type) {
    case 'ORDER_CHANGE_SENDBYMAIL' :
      newState.sendByMail = !state.sendByMail;
      break;
    case 'ORDER_CHANGE_OWNER_EDIT' :
      if(action.isOwner){
        newState.owner.firstname = action.owner.firstname;
        newState.owner.lastname = action.owner.lastname;
        newState.owner.phone = action.owner.phone;
        newState.owner.email = action.owner.email;
      }else{
        newState.user.FirstName = action.owner.firstname
        newState.user.Name = action.owner.lastname
        newState.user.PhoneNumber = action.owner.phone
        newState.user.Email = action.owner.email
      }
      console.log('action')
      console.log(action)
      console.log('new state')
      console.log(newState)

      break;
    case 'ORDER_CHANGE_ADDRESS' :
      newState.address.street = action.street;
      newState.address.numbox = action.numbox;
      newState.address.zip = action.zip;
      newState.address.city = action.city;
      break;
    case 'ORDER_CHANGE_ADDRESS_DOCUMENT_AUTRE' :
      newState.addressDocumentAutre.street = action.street;
      newState.addressDocumentAutre.numbox = action.numbox;
      newState.addressDocumentAutre.zip = action.zip;
      newState.addressDocumentAutre.city = action.city;
      break;
    case 'ORDER_CHANGE_OWNER' :
      newState.owner.id = action.id;
      newState.owner.firstname = action.firstname;
      newState.owner.lastname = action.lastname;
      newState.owner.phone = action.phone;
      newState.owner.email = action.email;
      break;

    case 'ORDER_CHANGE_CONTACT' :
      newState.contact.firstname = action.firstname;
      newState.contact.lastname = action.lastname;
      newState.contact.phone = action.phone;

      console.log('reducer, date',action.dates)
      newState.contact.dates = action.dates;
      break;

    case 'ORDER_CHANGE_BDC_SIGNED' :
      newState.bdcSigned=action.bdcSigned
      break;

    case 'ORDER_CHANGE_IS_ACTE' :
      newState.isActe = action.isActe;
      newState.acteinfo = action.acteinfo
      break;

    case 'ORDER_CHANGE_PARTIEL_NUMBER' :
      newState.partielNumber = action.num
      break;

    case 'ORDER_CHANGE_DOCUMENT' :

      newState.document.street = action.street;
      newState.document.numbox = action.numbox;
      newState.document.zip = action.zip
      newState.document.city = action.city;
      newState.document.country = action.country;
      newState.document.name = action.name;
      newState.document.company = action.company;
      newState.document.tva = action.tva;
      newState.document.phone = action.phone;
      newState.document.email = action.email;
      newState.document.is = action.is;
      break;

    case 'ORDER_CHANGE_REMARQUE' :
      newState.remarque = action.remarque;
      break;

    case 'ORDER_CHANGE_REGION' :
      newState.region = action.region;
      break;

    case 'ORDER_CHANGE_BIEN' :
      newState.bien = {
        buildingType : action.buildingType,
        count : action.count,
        size : action.size
      };
      newState.estateType = action.estateType
      break;

    case 'ORDER_CHANGE_ELEC_COUNT' :
      newState.elecCount = action.count;
      break;

    case 'ORDER_CHANGE_PLAN_COUNT' :
      newState.planCount = action.count;
      break;

    case  'ORDER_CHANGE_CITERNE_CHOICE' :
      newState.citerneChoice = action.num;
      break;

    case 'ORDER_CHANGE_EXPERTISE' :
      newState.expertises = action.expertises;
      newState.promoCodeUsed = {}
      break;

    case 'ORDER_CHANGE_MAXSTEP' :
      newState.maxStep = action.maxStep;
      break;

    case 'ORDER_CHANGE_CURRENTSTEP' :
      newState.currentStep = action.currentStep;
      break;

    case 'ORDER_ADD_DATEBUTOIR' :
      newState.contact.dateButoir = action.dateButoir;
      break;

    case 'ORDER_CHANGE_NEWUSER' :
      newState.user.FirstName = action.FirstName
      newState.user.Name = action.Name
      newState.user.PhoneNumber = action.PhoneNumber
      newState.user.Email = action.Email
      newState.user.Consumer.ConsumerType = action.newUserType
      newState.user.newUserType = action.newUserType
      break;
    case 'ORDER_CHANGE_NEWUSERADDRESS':
      newState.user.newaddress = action.address
      break;
    case 'ORDER_CHANGE_USER' :
      newState.user = action.user;
      break;
    case 'ORDER_CHANGE_PRODUCTS_TOTAL' :
      newState.expertisesPrices.products = action.products
      newState.expertisesPrices.total = action.total
      newState.commissionAmount = action.commission
      break;
    case 'ORDER_CHANGE_COMMISSION' :
      newState.commissionAmount = action.commission
      break;
    case 'ORDER_ADD_PROMO' :
      newState.promoCodeUsed[action.code]=action.promo;
      break;
    case 'ORDER_CHANGE_CONDITION' :
     newState.conditionGeneralChecked = action.checked;
     break;

    case 'ORDER_POPUP_COORDONNEE_CLOSE' :
      newState.popupCoordonneeIsClosed = true
      break;
    case 'ORDER_CHANGE_OTHER_DOCUMENT' :
      newState.otherDocument = action.other
      break;
    case 'ORDER_CHANGE_OTHER_NOTAIRE_DOCUMENT' :
      newState.otherNotaireDocument = action.otherNotaire
      break;
  }

  if(newState.isAnObject){
    newState.lastModification = new Date();
    localStorage.setItem('Certinergie-Order-State',JSON.stringify(newState));
  }
  return newState;
}

//clone object method
function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    var temp = obj.constructor();
    for (var key in obj) {
        temp[key] = cloneObject(obj[key]);
    }

    return temp;
}

export default test;

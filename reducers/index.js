import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import order from './order';

const rootReducer = combineReducers({order, routing: routerReducer });

export default rootReducer;

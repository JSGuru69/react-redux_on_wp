/**
 * CONST for EstateType
**/
const MAISON_2 = 7;
const MAISON_4 = 8;
const VILLA = 5;

const STUDIO = 4;
const PETIT_APPART = 11;
const APPART = 1;
const DUPLEX = 12;
const TRIPLEX = 13;
const APPART_5075 = 11; 
const IMMEUBLE_APPART = 2
/**
 * TABLE for binding ui estate to estate type
**/
const EstateTypes = [
  { buildingType : 'a' , size : 0 , typeId : STUDIO },   // studio <50m2
  { buildingType : 'a' , size : 1 , typeId : APPART_5075 },  // app 50-75m2
  { buildingType : 'a' , size : 2 , typeId : APPART },   // app 75-125m2
  { buildingType : 'a' , size : 3 , typeId : DUPLEX },  // app 125-200m2
  { buildingType : 'a' , size : 4 , typeId : TRIPLEX },  // app 200-300m2  duplex??
  { buildingType : 'a' , size : 5 , typeId : TRIPLEX },  // app +300m2  triplex??


  { buildingType : 'm' , size : 0 , typeId : MAISON_2 },   // maison 1 ch
  { buildingType : 'm' , size : 1 , typeId : MAISON_2 },   // maison 2 ch
  { buildingType : 'm' , size : 2 , typeId : MAISON_4 },   // maison 3 ch
  { buildingType : 'm' , size : 3 , typeId : MAISON_4 },   // maison 4 ch

  { buildingType : 'm' , size : 6 , typeId : MAISON_4 },   // maison 5 ch -350m2
  { buildingType : 'm' , size : 7 , typeId : VILLA },   // maison 5 ch +350m2
  { buildingType : 'm' , size : 8 , typeId : VILLA },   // maison + ch -350m2
  { buildingType : 'm' , size : 9 , typeId : VILLA },   // maison + ch +350m2

  { buildingType : 'i' , count : 1 , typeId : IMMEUBLE_APPART },   // immeuble
  { buildingType : 'i' , count : 2 , typeId : IMMEUBLE_APPART },   // immeuble
  { buildingType : 'i' , count : 3 , typeId : IMMEUBLE_APPART },   // immeuble
  { buildingType : 'i' , count : 4 , typeId : IMMEUBLE_APPART },   // immeuble
  { buildingType : 'i' , count : 5 , typeId : IMMEUBLE_APPART },   // immeuble
  { buildingType : 'i' , count : 6 , typeId : IMMEUBLE_APPART }    // immeuble
]

export default EstateTypes

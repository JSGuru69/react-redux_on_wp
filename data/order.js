import EstateType from './EstateTypes';

const order = {
  region : -1,
  bien : {
    buildingType : null, //a appart, m maison, i immeuble appartment
    count : -1,
    size : -1
  },
  buildings : {
    'a' : [
      {id : 0, name:'bien-appart-Studio (<50m2)'},{id : 1, name:'bien-appart-50-75m2'},
      {id : 2, name:'bien-appart-75-125m2'},{id : 3, name:'bien-appart-125-200m2'},
      {id : 4, name:'bien-appart-200-300m2'},{id : 5, name:'bien-appart->300m2'}
    ],
    'm' : [
      {id:0,name:'bien-maison-1 chambres'},{id : 1, name:'bien-maison-2 chambres'},{id :2, name:'bien-maison-3 chambres'},
      {id : 3, name:'bien-maison-4 chambres'},{id : 4, name:'bien-maison-5 chambres'},{id : 5, name:'bien-maison-6 chambres'},
      {id : 6, name:'bien-maison-Villa'},{id : 7, name:'bien-maison-GrandeVilla'},
      {id : 8, name:'bien-maison-Villa'},{id : 9, name:'bien-maison-GrandeVilla'},{id:100,name:'bien-maison-needtocall'}
    ],
    'i' : [{id : 1, name:'immeuble'},{id : 2, name:'immeuble'},{id : 3, name:'immeuble'},
           {id : 4, name:'immeuble'},{id : 5, name:'immeuble'},{id : 6, name:'immeuble'}]
  },
  services : [
    { id: 1, name: 'peb', regions: [0,1,2],biens: ['a','m','i']},
    /*{ id: 11, name: 'Peb Partiel', regions: [0], biens : ['a','i'],info:'peb-partiel-info'},*/
    { id: 4, name: 'elec', regions: [0,1,2],biens: ['a','m','i']},
    { id: 69, name: 'citerne', regions: [0], biens: ['m']},
    { id: 14, name: 'Plan', regions: [0,1,2], biens : ['a','m','i'],info:'plan-2d-info',img: 'plan.jpg'}
  ],
  expertisesPrices : {
    products : [],
    total : 0
  },
  promoCodeUsed : {},
  expertises : [],
  citerneChoice: -1,
  elecCount : 0,
  planCount : 0,
  address : {
    street : '',
    numbox : '',
    zip : '',
    city: ''
  },
  partielNumber : '',
  addressDocumentAutre : {
    name : '',
    phone : '',
    email : '',
    street : '',
    numbox : '',
    zip : '',
    city: ''
  },
  user: {
    UserID : null,
    Email : '',
    Name : '',
    FirstName : '',
    Consumer : {
      ConsumerType : '1688'
    },
    newaddress : {
      street: '',
      numbox : '',
      zip : '',
      city: ''
    }
  },
  owner : {
    firstname : '',
    lastname  : '',
    phone     : '',
    email     : '',
    id        : ''
  },
  contact : {
    firstname : '',
    lastname  : '',
    phone     : '',
    dates : [],
    dateButoir : null
  },
  products : [],
  document : {
    name   : '',
    company: '',
    tva    : '',
    street : '',
    numbox : '',
    zip: '',
    city: '',
    is : 'sth'
  },
  bdcSigned : true,
  isActe : false,
  acteinfo : {
    name:'',
    numNational :'',
    acteDate : ''
  },
  conditionGeneralChecked: false,
  remarque : '',
  maxStep : 1,
  currentStep : 1,
  commissionAmount : 0,
  isAnObject : true,
  estateType:null,
  sendByMail:false,
  popupCoordonneeIsClosed : false
}


let initFromStorage = JSON.parse(window.localStorage.getItem('Certinergie-Order-State'));

//check if the last save in storage is less than two hour old
let now = (new Date()).getTime()
let dateFromStorage = (initFromStorage && initFromStorage.lastModification)?(new Date(initFromStorage.lastModification)).getTime():-1;
let dateCheck = (now - dateFromStorage);
let isNewEnough = dateCheck < 1200000;

let orderToReturn = (initFromStorage && initFromStorage.isAnObject && isNewEnough)?initFromStorage:order

if(!(initFromStorage && initFromStorage.isAnObject && isNewEnough) && window.location.search){
  let precommande = window.location.search.split('=')[1].split('-') ;
  orderToReturn.region = precommande[0];


  let types = EstateType

  let type = types.filter(t=>t.typeId == precommande[1])[0];
  orderToReturn.estateType = precommande[1];
  orderToReturn.bien = {
      buildingType : type.buildingType, //a appart, m maison, i immeuble appartment
      count : 1,
      size : type.size
    }

    orderToReturn.expertises = [...orderToReturn.services.filter(s=>{
      return (precommande[2]==0)?(s.id == 1||s.id==4):s.id == precommande[2]
    })]

}

let loggedUserFromStorage = JSON.parse(window.localStorage.getItem('user'));
if(loggedUserFromStorage){
  orderToReturn.user = loggedUserFromStorage
}
//if more tha two hour old, we take a new one
export default orderToReturn;

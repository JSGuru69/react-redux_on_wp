import T from '../traduction';
import EstateTypes from './EstateTypes';

function transformToApi(data){
  //VatNumber in address

  let HasReducMail = data.document.is == 'mail' && (data.user && data.user.Consumer.ConsumerType == "1688")?2.5:0

  //data butoir in comment!
  let remarque = data.contact.dateButoir?("Date Butoir : "+(function(d){
    let date = new Date(d);
    return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
  })(data.contact.dateButoir)):""

  if(data.partielNumber !== ''){
    remarque = remarque + "\n"+"Numéro du partiel existant : "+data.partielNumber;
  }

  if(data.acteinfo.numNational){
    remarque = remarque+"\n"+"Numéro national en cas d'acte : "+data.acteinfo.numNational;
  }

  if(remarque !== ''){
    remarque = remarque + "\n"+'-----------------';
  }

  remarque = remarque + "\n"+data.remarque;


  let Owner = {
    "Email" : data.owner.email,
    "FirstName" : data.owner.firstname,
    "LastName" : data.owner.lastname,
    "PhoneNumber" : data.owner.phone,
    "NationalNumber" : null
  }

  if(!data.owner.email && !data.owner.firstname && !data.owner.lastname){
    Owner = null;
  }

  let _addressOrderConsumer = (data.user && data.user.newaddress && data.user.newaddress.zip)? parseInt(data.user.newaddress.zip):null;

    let url= window.location.href;

  let lang = 0;
     let userlang = "fr-BE";

  if(url.indexOf("/nl/") !== -1){
    lang = 1;
    userlang = "nl-BE";
  }
   



  let OrderConsumer = {
    "ConsumerType" : (data.user && data.user.Consumer && data.user.Consumer.ConsumerType)? data.user.Consumer.ConsumerType:1688,
    "UserID" : data.user.UserID,
    "User" : {
      "Email" : data.user.Email,
      "FirstName" : data.user.FirstName,
      "Name" : data.user.LastName ||  data.user.Name,
      "Language" : userlang,
      "PhoneNumber" : data.user.PhoneNumber,
      "TelNumber" : data.user.TelNumber,
      "Status" : data.user.Status,
      "Commission" : '0'
    },
    "AddressID" : data.user.AddressID,
    "Address" : {
      "Name" : null,
      "Company" : null,
      "Street" : (data.user && data.user.newaddress && data.user.newaddress.street)? data.user.newaddress.street:null,
      "Number" : (data.user && data.user.newaddress && data.user.newaddress.numbox)? data.user.newaddress.numbox:null,
      "VatNumber" : (data.user && data.user.newaddress && data.user.newaddress.tva)? data.user.newaddress.tva:null,
      "PostalCode" : isNaN(_addressOrderConsumer)?null:_addressOrderConsumer,
      "City" : (data.user && data.user.newaddress && data.user.newaddress.city)? data.user.newaddress.city:null,
      "Country" : 'Belgique'
    }
  }

  if((!OrderConsumer.Address.Street || OrderConsumer.Address.Street == '') && (!OrderConsumer.Address.City || OrderConsumer.Address.City == '')){
    OrderConsumer.Address = null
  }

  let AvailableDates = data.contact.dates.map(function(a){
    let date = a.date
    if(typeof date=='string'){
      date = new Date(date);
    }
    return {
      Period : parseInt(a.dayMoment),
      Date : date
    }
  })||[];

  if(data.contact.dateButoir){
    let date = data.contact.dateButoir
    if(typeof date=='string'){
      date = new Date(date);
    }
    AvailableDates.push({
      Period : 4,
      Date : date
    })
  }



  let Comment = {
    "Type" : 1,
    "Commentaire" : remarque
  }

  let Contact = {
    "Name" : data.contact.firstname,
    "Surname" : data.contact.lastname,
    "Phone" : data.contact.phone
  }


  let EstateType = EstateTypes.filter(function(et){
    return et.buildingType == data.bien.buildingType && (et.size == data.bien.size || et.count == data.bien.size);
  }).reduce(function(prev,et){
    prev = et.typeId;
    return prev;
  },-1);


  let Order = {
    "EstateType" : EstateType,
    "RegionLocation" : data.region,
    "OrderCommission" : data.commissionAmount,
    "NeedBDC" : data.bdcSigned,
    "SentByMail" : (data.document.is === 'mail')?true:false,
    "Language" : lang
  }

  let Invoice = {
    "IsDeedPayment" : data.isActe,
    "TotalPrice" : data.expertisesPrices.total-HasReducMail
  }

  let _addressBienZip = parseInt(data.address.zip)
  let AddressBien = {
    "Name" : null,
    "Company" : null,
    "Street" : data.address.street,
    "Number" : data.address.numbox,
    "VatNumber" : data.address.tva,
    "PostalCode" : isNaN(_addressBienZip)?null:_addressBienZip,
    "City" : data.address.city,
    "Country" : "Belgique"
  }

  let _addressEnvoisZip = (data.document.zip)?parseInt(data.document.zip):null
  let AddressEnvois = {
    "Name" : data.document.name,
    "Company" : data.document.company,
    "Street" : data.document.street,
    "Number" : data.document.numbox,
    "VatNumber" : data.document.tva,
    "PostalCode" : isNaN(_addressEnvoisZip)?null:_addressEnvoisZip,
    "City" : data.document.city,
    "Country" : data.document.country
  }

  let userAddres = (data.user.newaddress && data.user.newaddress.street !=='')?data.user.newaddress:
                   (data.user.Addresses && data.user.Addresses.length>0 && data.user.Addresses[0])?data.user.Addresses[0]:
                   AddressBien;
   if(userAddres.Street && userAddres.PostalCode){
     userAddres.street = userAddres.Street;
     userAddres.number = userAddres.Number;
     userAddres.zip = userAddres.zip;
     userAddres.city = userAddres.City;
   }

  if(data.user.Consumer.ConsumerType === 1689 && data.isActe){


    AddressEnvois = {
      "Name" : data.user.Name,
      "Company" : userAddres.company,//data.user.,
      "Street" : userAddres.street,
      "Number" : userAddres.numbox,
      "VatNumber" : userAddres.tva,//data.document.tva,
      "PostalCode" : isNaN(userAddres.zip)?null:userAddres.zip,
      "City" : userAddres.city,
      "Country" : data.document.country
    }
  }
  if(data.user.Consumer.ConsumerType === 8122 && data.isActe && !(data.acteinfo.numNational && data.acteinfo.numNational !== "")){

  }



  let bienPhrase =  (
    data.bien.buildingType == 'a' ? 'Appartement':
    data.bien.buildingType == 'i' ? 'Immeuble d\'appartement':
    data.bien.buildingType == 'm' ? 'Maison' : 'Maison'
  )+' ' +T(data.buildings[data.bien.buildingType][data.bien.size!=-1?data.bien.size:data.bien.count].name+'-resume');

  /*let OrderProducts = data.expertisesPrices.products.map(function(p){
    return {
      "ProductTypeId" : p.productId,
      "ProductName" : p.name,
      "Description" : bienPhrase,
      "TotalTVAC" : p.price,
      "ContactName" : p.productId == 5?null:Contact.Name,
      "ContactSurname" : p.productId == 5?null:Contact.Surname,
      "ContactPhone" : p.productId == 5?null:Contact.Phone
    }
  });*/
  let OrderProducts = data.expertisesPrices.products.reduce(function(tab,p){

    let price = parseInt(p.price);

    if(p.productId == 8){
      price = price/(parseInt(data.elecCount)-1)
    }else if(p.productId == 14){
      price = price/(parseInt(data.planCount));
    }else if(p.productId == 1){
      if(data.bien.buildingType == 'i'){
        price = price-50;
        price = price/parseInt(data.bien.count);
      }
    }

    let product = {
      "ProductTypeId" : p.productId,
      "ProductName" : p.name,
      "Description" : null,//bienPhrase,
      "TotalTVAC" : price,
      "ContactName" : p.productId == 5?null:Contact.Name,
      "ContactSurname" : p.productId == 5?null:Contact.Surname,
      "ContactPhone" : p.productId == 5?null:Contact.Phone
    }

    if(p.productId == 8){
      for(let i = 0; i<parseInt(data.elecCount)-1;i++) {
        tab.push(product)
      }
    }else if(p.productId == 14){
      for(let i = 0; i<parseInt(data.planCount);i++) {
        tab.push(product)
      }
    }else if(p.productId == 1){
      if(data.bien.buildingType == 'i'){
        for(let i = 0; i<parseInt(data.bien.count);i++) {
          let p = {...product}
          if(i==0){
            p.TotalTVAC = p.TotalTVAC+50;
          }
          tab.push(p)
        }
      }else{
        tab.push(product)
      }
    }else{
      tab.push(product)
    }

    return tab
  },[]);
  if(HasReducMail != 0){
    OrderProducts.push({
      "ProductTypeId" : 5,
      "ProductName" : 'Mail Only',
      "Description" : null,
      "TotalTVAC" : HasReducMail>0?-HasReducMail:HasReducMail,
      "ContactName" : null,
      "ContactSurname" : null,
      "ContactPhone" : null
    })
  }


  let result = {Order,Invoice,AddressBien,AddressEnvois,OrderProducts,OrderConsumer,Owner,Comment,AvailableDates}
  return result;
}

export default transformToApi

const PROMOS = {
   'notaire' : {
    region : [0,1],
    expertises : [1,4],
    errorExpertises : {
      'fr' : 'Code promo effectif en cas de combiné',
      'nl' : 'Promo code combi only',
      'en' : 'Promo code combi only'
    },
    reduc : 25
  }, 
  'combiweb' : {
    region : [0,1],
    expertises : [1,4],
    errorExpertises : {
      'fr' : 'Code promo effectif en cas de combiné',
      'nl' : 'Promo code combi only',
      'en' : 'Promo code combi only'
    },
    reduc : 25
  }, 
  'vlaanderen' : {
    region : [2],
    expertises : [1,4],
    errorExpertises : {
      'fr' : 'Code promo effectif en cas de combiné',
      'nl' : 'Promo code combi only',
      'en' : 'Promo code combi only'
    },
    reduc : 50
  },
  'elec2016' : {
    region : [0,1],
    expertises : [4],
    errorExpertises : {
      'fr' : 'Code promo effectif en cas de elec seul',
      'nl' : 'Promo code combi only',
      'en' : 'Promo code combi only'
    },
    reduc : 5
  }
}


export default PROMOS;
